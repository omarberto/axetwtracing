﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AXETWTracingCA
{
    class Program
    {
        static void Main(string[] args)
        {

            String LocalSystem = CustomActions.GetUsernameFromSID("S-1-5-18");
            String LocalService = CustomActions.GetUsernameFromSID("S-1-5-19");
            String LocalNetworkService = CustomActions.GetUsernameFromSID("S-1-5-20");

            bool i = CustomActions.IsValidPrincipal("wp", "pb00013");
            i = CustomActions.IsValidPrincipal("wp.lan", "gMSA-test-LEPE$", true);
            i = CustomActions.IsValidPrincipal("wp", "lg-systemintegration-rw");
            i = CustomActions.IsValidPrincipal("pbzdev01", "Administrator");
            i = CustomActions.IsValidPrincipal("pbzdev01.test", "Administrator");

            if (ETWHelper.existProviderName("Microsoft-Windows-Firewall"))
            {
                Console.WriteLine("Provider Found!");
            }
            else
            {
                Console.WriteLine("Provider NOT Found!");
            }
            Console.ReadLine();
        }
    }
}


