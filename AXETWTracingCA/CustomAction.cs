using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Deployment.WindowsInstaller;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Windows.Forms;
using System.Security;
using System.IO;
using System.DirectoryServices.AccountManagement;

using System.Security.AccessControl;
using System.ComponentModel;

namespace AXETWTracingCA
{
    public class CustomActions
    {
 
        const int NO_ERROR = 0;
        const int ERROR_INSUFFICIENT_BUFFER = 122;

        enum SID_NAME_USE
        {
            SidTypeUser = 1,
            SidTypeGroup,
            SidTypeDomain,
            SidTypeAlias,
            SidTypeWellKnownGroup,
            SidTypeDeletedAccount,
            SidTypeInvalid,
            SidTypeUnknown,
            SidTypeComputer,
            SidTypeLabel,
            SidTypeLogonSession
        }

        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern bool LookupAccountSid(
            string lpSystemName, [MarshalAs(UnmanagedType.LPArray)] byte[] Sid, StringBuilder lpName,
            ref uint cchName, StringBuilder ReferencedDomainName, ref uint cchReferencedDomainName, out SID_NAME_USE peUse);

        private static ContextType getPrincipalContext(string domain)
        {
            ContextType ctx;

            ctx = ContextType.Machine;
            String Computername = System.Environment.MachineName;

            if (!string.IsNullOrEmpty(domain))
            {
                if (!(domain.ToUpper().Equals(Computername.ToUpper())))
                {
                    ctx = ContextType.Domain;
                }

            }
            return ctx;
        }

        public static bool isComputerPrincipal(string domain, string principal)
        {
            bool isValid = false;
            ContextType ctx = getPrincipalContext(domain);
            try
            {
                using (PrincipalContext pc = new PrincipalContext(ctx, domain))
                {
                    Principal up = null;
                    up = ComputerPrincipal.FindByIdentity(pc, IdentityType.SamAccountName, principal);
                    isValid = (up != null);
                }
            }
            catch (Exception e)
            {
                isValid = false;
            }
            return isValid;

        }
        public static bool IsValidPrincipal(string domain, string principal, bool onlyAuthPrincipals = false)
        {
            bool isValid = false;
            ContextType ctx;

            ctx = getPrincipalContext(domain);

            try
            {
                using (PrincipalContext pc = new PrincipalContext(ctx, domain))
                {
                    Principal up = null;
                    if (onlyAuthPrincipals)
                    {
                        up = UserPrincipal.FindByIdentity(pc, IdentityType.SamAccountName, principal);
                        if (up == null)
                            up = ComputerPrincipal.FindByIdentity(pc, IdentityType.SamAccountName, principal);
                    }
                    else
                        up = Principal.FindByIdentity(pc, IdentityType.SamAccountName, principal);
                    isValid = (up != null);
                }
            }
            catch (Exception e)
            {
                isValid = false;
            }
            return isValid;
        }
        public static string GetUsernameFromSID(string strSid)
        {
            StringBuilder name = new StringBuilder();
            uint cchName = (uint)name.Capacity;
            StringBuilder referencedDomainName = new StringBuilder();
            uint cchReferencedDomainName = (uint)referencedDomainName.Capacity;
            SID_NAME_USE sidUse;

            var sid = new SecurityIdentifier(strSid);
            byte[] byteSid = new byte[sid.BinaryLength];
            sid.GetBinaryForm(byteSid, 0);

            int err = NO_ERROR;
            if (!LookupAccountSid(null, byteSid, name, ref cchName, referencedDomainName, ref cchReferencedDomainName, out sidUse))
            {
                err = System.Runtime.InteropServices.Marshal.GetLastWin32Error();
                // Error codes can be found here https://docs.microsoft.com/en-us/windows/win32/debug/system-error-codes
                if (err == ERROR_INSUFFICIENT_BUFFER)
                {
                    name.EnsureCapacity((int)cchName);
                    referencedDomainName.EnsureCapacity((int)cchReferencedDomainName);
                    err = NO_ERROR;
                    if (!LookupAccountSid(null, byteSid, name, ref cchName, referencedDomainName, ref cchReferencedDomainName, out sidUse))
                        err = System.Runtime.InteropServices.Marshal.GetLastWin32Error();
                }
            }
            if (err == 0)
                return referencedDomainName.ToString() + '\\' + name.ToString();
            else
                return null;
        }

        [CustomAction]
        public static ActionResult AXETWTrcInitEnv(Session session)
        {
            session.Log("Begin AXETWTrcInitEnv");
            // well known sid on windows https://support.microsoft.com/en-us/help/243330/well-known-security-identifiers-in-windows-operating-systems
            String accountLocalAdminGrp = new SecurityIdentifier("S-1-5-32-544").Translate(typeof(NTAccount)).ToString();
            SecurityIdentifier sid = new SecurityIdentifier("S-1-5-32-544");
            IdentityReference t = sid.Translate(typeof(NTAccount));
            string[] fullName = accountLocalAdminGrp.Split(new char[] { '\\' });

            //S-1-5-32-558  Builtin\Performance Monitor Users 
            //S-1-5-32-559  Builtin\Performance Log Users 
            //S-1-5-32-573  Builtin\Event Log Readers 
            //S-1-5-32-544  Builtin\Administrators 
            String PerfMonUsersGrp=GetUsernameFromSID("S-1-5-32-558");
            String PerfLogUsersGrp = GetUsernameFromSID("S-1-5-32-559");
            String EventLogReadersGrp = GetUsernameFromSID("S-1-5-32-573");
            String LocalAdminsGrp = GetUsernameFromSID("S-1-5-32-544");
            String LocalSystem = GetUsernameFromSID("S-1-5-18");
            String LocalService = GetUsernameFromSID("S-1-5-19");
            String LocalNetworkService = GetUsernameFromSID("S-1-5-20");

            String Computerdomain = System.DirectoryServices.ActiveDirectory.Domain.GetComputerDomain().Name;
            String Computername = System.Environment.MachineName;

            session["AXETWTRC_COMPUTERDOMAIN"] = Computerdomain;
            session["AXETWTRC_COMPUTERNAME"] = Computername;

            string[] PerfMonUsersGrpFullname = PerfMonUsersGrp.Split(new char[] { '\\' });
            session["AXETWTRC_PERFMONUSERSGRP"] = PerfMonUsersGrpFullname[1];

            string[] PerfLogUsersGrpFullname = PerfLogUsersGrp.Split(new char[] { '\\' });
            session["AXETWTRC_PERFLOGUSERSGRP"] = PerfLogUsersGrpFullname[1];

            string[] EventLogReadersGrpFullname = EventLogReadersGrp.Split(new char[] { '\\' });
            session["AXETWTRC_EVENTLOGREADERSGRP"] = EventLogReadersGrpFullname[1];

            string[] LocalAdminsGrpFullname = LocalAdminsGrp.Split(new char[] { '\\' });
            session["AXETWTRC_LOCALADMINSGRP"] = LocalAdminsGrpFullname[1];

            session["AXETWTRC_LOCALSYSTEMFULLNAME"] = LocalSystem;

            if (session["LICENSEACCEPTED"] == "1")
            {
                session["LicenseAccepted"] = session["LICENSEACCEPTED"];
            }
            else
            {
                session["LicenseAccepted"] = null;
            }

            session["AXETWTRC_INSTALLED"] = "0";

            if (ETWHelper.existProviderName("Microsoft-DynamicsAX-Tracing"))
            {
                session["AXETWTRC_INSTALLED"] = "1";
            };

            session.Log("End AXETWTrcInitEnv");
            return ActionResult.Success;
        }



        private static bool AXETWDedectConfigFile(Session session,out Record errormsg)
        {
            bool aResult = false;

            String Computerdomain = System.DirectoryServices.ActiveDirectory.Domain.GetComputerDomain().Name;
            String Computername = System.Environment.MachineName;

            string axetwdirid = session["AXETWTRACINGCONFDIR"];
            string axetwdir = session["AXETWTRACINGCONFDIR"];//session.GetTargetPath("AXETWCONFIGFOLDER"); //session["AXETWTRACINGCONFDIR"];
            string axetwconfigfilename = session["AXETWCONFIGFILENAME"];
            string msg;
            Record r = new Record(0);

            if (axetwdir != null)
            {
                axetwdir = axetwdir.Trim();
                if (!axetwdir.EndsWith("\\"))
                {
                    axetwdir = axetwdir + "\\";
                }
                string axetwcomputerdir = axetwdir + Computername;
                string axetwcomputerdomaindir = axetwdir + Computername + "." + Computerdomain;

                string _path = null;
                if (File.Exists(axetwcomputerdomaindir + "\\" + axetwconfigfilename))
                {
                    _path = axetwcomputerdomaindir + "\\" + axetwconfigfilename;
                }
                else if (File.Exists(axetwcomputerdir + "\\" + axetwconfigfilename))
                {
                    _path = axetwcomputerdir + "\\" + axetwconfigfilename;
                }
                else if (File.Exists(axetwdir + "\\" + axetwconfigfilename))
                {
                    _path = axetwdir + "\\" + axetwconfigfilename;
                }
                if (_path == null)
                { 
                    msg = "Configuration File " + axetwconfigfilename + " does not exist in directory: \n\t" + axetwcomputerdomaindir + " or \n\t" + axetwcomputerdir + " or \n\t" + axetwdir+"\nVERIFY that directory exist and User installing and running service has read permissions !!";
                    r[0] = msg;
                }
                else
                {
                    aResult = true;
                }
                session["AXETWTRC_FULLCONFIGPATH"] = _path;
            }
            else
            {
                msg = "AX ETW Config Path not set!!";
                r[0] = msg;
            }


            if (session["AXETWSERVICEACCOUNTBUILDIN"] == "1")
            {
                session["AXETWSERVICEACCOUNT"] = null;
                session["AXETWSERVICEACCOUNTPWD"] = null;
                session["AXETWTRC_SERVICEACCOUNTNAME"] = null;
                session["AXETWTRC_SERVICEACCOUNTDOMAIN"] = null;
            }

            errormsg = r;
            return aResult;
        }

        private static bool AXETWDedectAdminAccount(Session session, out Record errormsg)
        {
            bool aResult = true;
            string msg;
            Record r = new Record(0);
            
            string axetwassignadmaccount = session["AXETWASSIGNADMIN"];
            string axetwadmaccount = session["AXETWADMINACCOUNT"];
            string axetwadmaname=null;
            string axetwadmadomain=null;

            if (axetwassignadmaccount != null && axetwassignadmaccount.Equals("1"))
            {
                if (!ParseUserName(axetwadmaccount, out axetwadmaname, out axetwadmadomain) || !IsValidPrincipal(axetwadmadomain, axetwadmaname))
                {
                    msg = "Windows Account/Group " + axetwadmaccount + " for administrate AX Tracing Service is not valid !!";
                    r[0] = msg;
                    aResult = false;
                    session["AXETWTRC_ADMINACCOUNTNAME"] = null;
                    session["AXETWTRC_ADMINACCOUNTDOMAIN"] = null;
                }
                else
                {
                    session["AXETWTRC_ADMINACCOUNTNAME"] = axetwadmaname;
                    session["AXETWTRC_ADMINACCOUNTDOMAIN"] = axetwadmadomain;
                }
            }
            else
            {
                session["AXETWADMINACCOUNT"] = null;
                session["AXETWTRC_ADMINACCOUNTNAME"] = axetwadmaname;
                session["AXETWTRC_ADMINACCOUNTDOMAIN"] = axetwadmadomain;
            }
            errormsg = r;
            return aResult;
        }

        private static bool AXETWDedectServiceAccount(Session session, out Record errormsg, out Record warningmsg)
        {
            bool aResult = true;
            string msg;
            Record r = new Record();
            Record warnr = new Record();

            if (session["AXETWSERVICEACCOUNTBUILDIN"] == "1")
            {
                session["AXETWSERVICEACCOUNT"] = null;
                session["AXETWSERVICEACCOUNTPWD"] = null;
                session["AXETWTRC_SERVICEACCOUNTNAME"] = null;
                session["AXETWTRC_SERVICEACCOUNTDOMAIN"] = null;
            }
            else
            {
                string axetwsvcaccount = session["AXETWSERVICEACCOUNT"];
                string axetwsvcaname;
                string axetwsvcadomain;

                if (axetwsvcaccount == null)
                    axetwsvcaccount = "";

                if (ParseUserName(axetwsvcaccount, out axetwsvcaname, out axetwsvcadomain) && IsValidPrincipal(axetwsvcadomain, axetwsvcaname, true))
                {
                    bool pwdOK = false;
                    session["AXETWTRC_SERVICEACCOUNTNAME"] = axetwsvcaname;
                    session["AXETWTRC_SERVICEACCOUNTDOMAIN"] = axetwsvcadomain;

                    string serviceaccountpwd = session["AXETWSERVICEACCOUNTPWD"];
                    try
                    {
                        pwdOK = true;
                        if (!isComputerPrincipal(axetwsvcadomain, axetwsvcaname))//exclude gmsa and msa and computer objects
                        {
                            using (new Impersonator(axetwsvcaname, axetwsvcadomain, serviceaccountpwd))
                            {
                                pwdOK = true;
                            }
                        }
                    }

                    catch (Win32Exception e)
                    {
                        if (e.NativeErrorCode == 1326 || e.NativeErrorCode == 1331 || e.NativeErrorCode == 1909) // only error if password is wrong otherwise,blocked temporarely or disabled
                        {
                            session["AXETWTRC_SERVICEACCOUNTNAME"] = null;
                            session["AXETWTRC_SERVICEACCOUNTDOMAIN"] = null;
                            Record _rerror = new Record(0);
                            msg = "Windows Account " + axetwsvcaccount + " does not exist or Password is not valid!!!\nDetails: " + e.Message + " (" + e.NativeErrorCode + ")";
                            r[0] = msg;
                            pwdOK = false;
                        }
                        else
                        {
                            if (e.NativeErrorCode != 1385)
                            {
                                Record _rerror = new Record(0);
                                msg = "Windows Account " + axetwsvcaccount + " has some restriction. Please check message.\n " + e.Message + " (" + e.NativeErrorCode + ")";
                                warnr[0] = msg;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        session["AXETWTRC_SERVICEACCOUNTNAME"] = null;
                        session["AXETWTRC_SERVICEACCOUNTDOMAIN"] = null;
                        Record _rerror = new Record(0);
                        msg = "General Exception during Service Account Validation !!!\n" + e.ToString() ;
                        r[0] = msg;
                        pwdOK = false;
                    }
                    aResult = pwdOK;
                }
                else {
                    msg = "AX Service Account " + axetwsvcaccount + " is not valid !!";

                    r[0] = msg;
                    aResult = false;
                    session["AXETWTRC_SERVICEACCOUNTNAME"] = null;
                    session["AXETWTRC_SERVICEACCOUNTDOMAIN"] = null;
                }
            }
            errormsg = r;
            warningmsg = warnr;
            return aResult;
        }

        [CustomAction]
        public static ActionResult AXETWValidateAccountsUI(Session session)
        {
            ActionResult aresult;
            Record msg,warningmsg;

            aresult = ActionResult.Success;
            if (!AXETWDedectAdminAccount(session, out msg))
            {
                MessageBox.Show((string)msg[0], "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                session.Message(InstallMessage.Error, msg);
                aresult = ActionResult.NotExecuted;
            }

            bool isserviceaccountok = AXETWDedectServiceAccount(session, out msg, out warningmsg);
            if (!isserviceaccountok)
            {
                MessageBox.Show((string)msg[0], "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                session.Message(InstallMessage.Error, msg);
                aresult = ActionResult.NotExecuted;
            }
            else
            {
                if (!warningmsg.IsNull(0))
                {
                    MessageBox.Show((string)warningmsg[0], "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    Record tmp = new Record(1);
                    tmp[0] = "WARNING: [1]";
                    tmp[1] = warningmsg[0];
                    session.Message(InstallMessage.Warning, tmp);
                }
            }

            return aresult;
        }

        [CustomAction]
        public static ActionResult AXETWValidateAccounts(Session session)
        {
            ActionResult aresult;
            Record msg, warningmsg;

            aresult = ActionResult.Success;

            if (!AXETWDedectAdminAccount(session, out msg))
            {
                session.Message(InstallMessage.Error, msg);
                aresult = ActionResult.Failure;
            }

            bool isserviceaccountok = AXETWDedectServiceAccount(session, out msg, out warningmsg);

            if (!isserviceaccountok)
            {
                session.Message(InstallMessage.Error, msg);
                aresult = ActionResult.Failure;
            }
            else
            {
                if (!warningmsg.IsNull(0))
                {
                    Record tmp = new Record(1);
                    tmp[0] = "WARNING: [1]";
                    tmp[1] = warningmsg[0];
                    session.Message(InstallMessage.Warning, tmp);
                }
            }
            return aresult;
        }

        [CustomAction]
        public static ActionResult AXETWSETLICENSEACCEPT(Session session)
        {
            ActionResult aresult;
            session["LICENSEACCEPTED"]="1";
            aresult = ActionResult.Success;
            return aresult;
        }

        [CustomAction]
        public static ActionResult AXETWDedectConfigFileUI(Session session)
        {
            ActionResult aresult;
            Record msg;

            aresult = ActionResult.Success;
            if (!AXETWDedectConfigFile( session, out msg))
            {
                MessageBox.Show((string)msg[0],"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                session.Message(InstallMessage.Error, msg);
                aresult = ActionResult.NotExecuted;
            }
            else
            {
                if (!AXETWValidateConfigFile(session, true))
                {
                    aresult = ActionResult.NotExecuted;
                }
            }

            return aresult;
        }

        [CustomAction]
        public static ActionResult AXETWDedectConfigFile(Session session)
        {
            ActionResult aresult;
            Record msg;

            aresult = ActionResult.Success;
            if (!AXETWDedectConfigFile(session, out msg))
            {
                session.Message(InstallMessage.Error,msg);
                aresult = ActionResult.Failure;
            }
            else
            {
                if (!AXETWValidateConfigFile(session, false))
                {
                    aresult = ActionResult.Failure;
                }
            }

            if(!AXETWValidateLicenceaccept(session,false))
            {
                aresult = ActionResult.Failure;
            }
            return aresult;
        }

        private static bool AXETWValidateLicenceaccept(Session session, bool isUI)
        {
            bool result = true;
            string validLic = session["LICENSEACCEPTED"];
            
            if (validLic != "1")
            {
                result = false;
               string msg ;

                msg = "License Terms has not benn accepted!!";

                if (isUI)
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Record _r = new Record(0);
                _r[0] = msg;
                session.Message(InstallMessage.Error, _r);
            }

            return result;
        }

        private static bool AXETWValidateConfigFile(Session session,bool isUI)
        {
            List<string> warnings;
            List<string> errors;
            bool result = false;


                result = AX2012ETWtracing.Configuration.Configuration.Test(session["AXETWTRC_FULLCONFIGPATH"], out warnings, out errors);
                if (!result)
                {
                    session["AXETWTRC_FULLCONFIGPATH"] = null;
                    Record _r = new Record(0);
                    foreach (var error in errors)
                    {
                        if (isUI)
                            MessageBox.Show((string)error, "Error validating AX Tracing Config File", MessageBoxButtons.OK, MessageBoxIcon.Error);

                        _r[0] = error;
                        session.Message(InstallMessage.Error, _r);
                    }

                }

            return result;
        }

        static bool ParseUserName(string logonName, out string user, out string domain)
        {
            char cSeparator;
            string strTmplogonname;
            bool bresult = false;
            String Computername = System.Environment.MachineName;

            user = null;
            domain = null;
            cSeparator = '.';

            if (!string.IsNullOrEmpty(logonName))
            {
                strTmplogonname = logonName.Trim();

                if (logonName.Contains("\\"))
                {
                    cSeparator = '\\';
                }
                else if (logonName.Contains("@"))
                {
                    cSeparator = '@';
                }

                if (cSeparator != '.')
                {
                    string[] logonParts;
                    logonParts = logonName.Split(cSeparator);

                    if (cSeparator == '\\')
                    {
                        domain = logonParts[0].Trim();
                        user = logonParts[1].Trim();
                    }
                    else
                    {
                        user = logonParts[0].Trim();
                        domain = logonParts[1].Trim();
                    }

                    if (string.IsNullOrEmpty(domain) || domain == ".")
                    {
                        domain = Computername;
                    }
                }
                else
                {
                    user = strTmplogonname;
                    domain = Computername;
                }

                if (!string.IsNullOrEmpty(user))
                {
                    bresult = true;
                }
            }

            return bresult;
        }
    }
}
