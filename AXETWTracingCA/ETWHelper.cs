﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;

namespace AXETWTracingCA
{
    class ETWHelper
    {
        [System.Security.SecurityCritical]
        public static IEnumerable<string> GetProviderNames()
        {
            //EventLogPermissionHolder.GetEventLogPermission().Demand();

            List<string> namesList = new List<string>(100);

            using (EventLogHandle ProviderEnum = NativeWrapper.EvtOpenProviderEnum(EventLogHandle.Zero, 0))
            {
                bool finish = false;

                do
                {
                    string s = NativeWrapper.EvtNextPublisherId(ProviderEnum, ref finish);
                    if (finish == false) namesList.Add(s);
                }
                while (finish == false);

                return namesList;
            }
        }

        [System.Security.SecurityCritical]
        public static bool existProviderName(string provider)
        {
            //EventLogPermissionHolder.GetEventLogPermission().Demand();
            string upperProvider = provider.ToUpper();
            using (EventLogHandle ProviderEnum = NativeWrapper.EvtOpenProviderEnum(EventLogHandle.Zero, 0))
            {
                bool existProvider = false;
                bool finish = false;
                do
                {
                    string s = NativeWrapper.EvtNextPublisherId(ProviderEnum, ref finish);
                    if (finish == false)
                    {
                        string tmpstr = s.ToUpper();
                        if (tmpstr.Equals(upperProvider))
                        {
                            existProvider = true;
                        }
                    }
                }
                while (!finish && !existProvider);

                return existProvider;
            }
        }
    }


    [Serializable]
    public enum Consistency : int
    {
        MayCorruptProcess = 0,
        MayCorruptAppDomain = 1,
        MayCorruptInstance = 2,
        WillNotCorruptState = 3,
    }

    [Serializable]
    public enum Cer : int
    {
        None = 0,
        MayFail = 1,  // Might fail, but the method will say it failed
        Success = 2,
    }

    [AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Interface /* | AttributeTargets.Delegate*/, Inherited = false)]
    public sealed class ReliabilityContractAttribute : Attribute
    {
        private Consistency _consistency;
        private Cer _cer;

        public ReliabilityContractAttribute(Consistency consistencyGuarantee, Cer cer)
        {
            _consistency = consistencyGuarantee;
            _cer = cer;
        }

        public Consistency ConsistencyGuarantee
        {
            get { return _consistency; }
        }

        public Cer Cer
        {
            get { return _cer; }
        }
    }

    internal static class UnsafeNativeMethods
    {
        internal const String WEVTAPI = "wevtapi.dll";

        internal const int ERROR_NO_MORE_ITEMS = 0x103;  // 259
        internal const int ERROR_INSUFFICIENT_BUFFER = 0x7A;  // 122

        [DllImport(WEVTAPI)]
        [ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
        [SecurityCritical]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool EvtClose(IntPtr handle);

        [DllImport(WEVTAPI, CharSet = CharSet.Auto, SetLastError = true)]
        [SecurityCritical]
        internal static extern EventLogHandle EvtOpenPublisherEnum(
                            EventLogHandle session,
                            int flags
                                    );
        [DllImport(WEVTAPI, CharSet = CharSet.Auto, SetLastError = true)]
        [SecurityCritical]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool EvtNextPublisherId(
                            EventLogHandle publisherEnum,
                            int publisherIdBufferSize,
                            [Out, MarshalAs(UnmanagedType.LPWStr)]StringBuilder publisherIdBuffer,
                            out int publisherIdBufferUsed
                                    );
    }

    internal sealed class EventLogHandle : SafeHandle
    {
        // Called by P/Invoke when returning SafeHandles
        private EventLogHandle()
            : base(IntPtr.Zero, true)
        {
        }

        internal EventLogHandle(IntPtr handle, bool ownsHandle)
            : base(IntPtr.Zero, ownsHandle)
        {
            SetHandle(handle);
        }

        public override bool IsInvalid
        {
            get
            {
                return IsClosed || handle == IntPtr.Zero;
            }
        }

        protected override bool ReleaseHandle()
        {
            NativeWrapper.EvtClose(handle);
            handle = IntPtr.Zero;
            return true;
        }

        // DONT compare EventLogHandle with EventLogHandle.Zero
        // use IsInvalid instead. Zero is provided where a NULL handle needed   
        public static EventLogHandle Zero
        {
            get
            {
                return new EventLogHandle();
            }
        }
    }

    internal class NativeWrapper
    {
        private static bool s_platformNotSupported = (Environment.OSVersion.Version.Major < 6);

        [System.Security.SecurityCritical]
        public static void EvtClose(IntPtr handle)
        {
            //
            // purposely don't check and throw - this is 
            // always called in cleanup / finalize / etc..
            //
            UnsafeNativeMethods.EvtClose(handle);
        }

        internal static void Throw(int errorCode)
        {
            string strerrorCode = errorCode.ToString();
            switch (errorCode)
            {
                case 2:
                case 3:
                case 15007:
                case 15027:
                case 15028:
                case 15002:
                    //throw new EventLogNotFoundException(errorCode);
                    throw new Exception("EventLogNotFoundException" + ":" + strerrorCode);
                case 13:
                case 15005:
                    //throw new EventLogInvalidDataException(errorCode);
                    throw new Exception("EventLogInvalidDataException" + ":" + strerrorCode);
                case 1818: // RPC_S_CALL_CANCELED is converted to ERROR_CANCELLED
                case 1223:
                    //throw new OperationCanceledException();
                    throw new Exception("OperationCanceledException" + ":" + strerrorCode);
                case 15037:
                    //throw new EventLogProviderDisabledException(errorCode);
                    throw new Exception("EventLogProviderDisabledException" + ":" + strerrorCode);
                case 5:
                    //throw new UnauthorizedAccessException();
                    throw new Exception("UnauthorizedAccessException" + ":" + strerrorCode);
                case 15011:
                case 15012:
                    //throw new EventLogReadingException(errorCode);
                    throw new Exception("EventLogReadingException" + ":" + strerrorCode);
                default:
                    //throw new EventLogException(errorCode);
                    throw new Exception("EventLogException" + ":" + strerrorCode);
            }
        }
        [System.Security.SecurityCritical]
        public static EventLogHandle EvtOpenProviderEnum(EventLogHandle session, int flags)
        {
            if (s_platformNotSupported)
                throw new System.PlatformNotSupportedException();

            EventLogHandle pubEnum = UnsafeNativeMethods.EvtOpenPublisherEnum(session, flags);
            int win32Error = Marshal.GetLastWin32Error();
            if (pubEnum.IsInvalid)
                Throw(win32Error);
            return pubEnum;
        }

        [System.Security.SecurityCritical]
        public static string EvtNextPublisherId(EventLogHandle handle, ref bool finish)
        {
            StringBuilder sb = new StringBuilder(null);
            int ProviderIdNeeded;

            bool status = UnsafeNativeMethods.EvtNextPublisherId(handle, 0, sb, out ProviderIdNeeded);
            int win32Error = Marshal.GetLastWin32Error();
            if (!status)
            {
                if (win32Error == UnsafeNativeMethods.ERROR_NO_MORE_ITEMS)
                {
                    finish = true;
                    return null;
                }

                if (win32Error != UnsafeNativeMethods.ERROR_INSUFFICIENT_BUFFER)
                    Throw(win32Error);
            }

            sb.EnsureCapacity(ProviderIdNeeded);
            status = UnsafeNativeMethods.EvtNextPublisherId(handle, ProviderIdNeeded, sb, out ProviderIdNeeded);
            win32Error = Marshal.GetLastWin32Error();
            if (!status)
                Throw(win32Error);

            return sb.ToString();
        }
    }
}
