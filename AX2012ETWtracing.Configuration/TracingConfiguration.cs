﻿using System;

namespace AX2012ETWtracing.Configuration
{
    public class TracingConfiguration
    {
        public string provider { get; set; }
        public string keywords { get; set; } = "0x0";
        [Nett.TomlIgnore]
        public ulong keywordsValue
        {
            get
            {
                if (string.IsNullOrEmpty(keywords))
                    return 0xFFFFFFFFFFFFFFFF;
                else
                    return Convert.ToUInt64(keywords, 16);
            }
            set
            {
                keywords = string.Format("0x{0:X}", value);
            }
        }

        public TracingConfiguration(string provider)
        {
            this.provider = provider;
        }
        public TracingConfiguration(string provider, string keywords)
        {
            this.provider = provider;
            this.keywords = keywords;
        }
    }
}