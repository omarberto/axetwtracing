﻿namespace AX2012ETWtracing.Configuration
{
    public class GeneralConfiguration
    {
        public string name { set; get; } = "AXETWTracing"; //logging purpouse
        public string version { set; get; } = "AX2012ETWTracing 0.1"; //
        public int zeroEventsTimeout { set; get; } = 0; //
        public long synchroIntervalLength { set; get; } = 60; //
        public long synchroIntervalLengthNS { get { return 60000000000L * synchroIntervalLength; } }
    }
}