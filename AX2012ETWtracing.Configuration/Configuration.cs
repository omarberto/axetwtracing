﻿
using Nett;
using System;
using System.Collections.Generic;

namespace AX2012ETWtracing.Configuration
{
    //di base manteniamo tutto uguale, semmai creiamo una config alternativa 

    public class Configuration
    {
        private static ConfigurationBase _configbase;
        private static Configuration _config;
        public static Configuration config
        {
            get
            {
                return _config;
            }
        }
        public static void InitBase(string configPath)
        {
            _configbase = Toml.ReadFile<ConfigurationBase>(configPath);
        }
        //private static void Init(string configPath)
        //{
        //     _config = Toml.ReadFile<Configuration>(configPath);
        //}
        public static void Init(string configPath)
        {
            _config = Toml.ReadFile<Configuration>(configPath);


            if (config.hasAX2012)
            {
                _config.ax2012.Init();
            }
        }

        public GeneralConfiguration agent { get; set; } = new GeneralConfiguration();

        #region AX2012
        [Nett.TomlIgnore]
        public bool hasAX2012
        {
            get
            {
                //per ora ne ho uno e basta... (di base è privilegiato nats)
                return ax2012 != null;
            }
        }
        public Ax2012Configuration ax2012 { get; set; }
        #endregion

        public static bool Test(string configfile, out List<string> warnings, out List<string> errors)
        {
            warnings = new List<string>();
            errors = new List<string>();

            try
            {
                Init(configfile);
                //


                if (config.hasAX2012)
                {
                    //accettabile
                    config.ax2012.Test(ref warnings, ref errors);
                }
                else
                {
                    //non accettabile
                    errors.Add("ax2012 configuration is missing");
                }

            }
            catch
            (Exception e)
            {
                errors.Add(string.Format("Configuration --> Init {0} \n Exception Message {1}", configfile, e.Message));

                return false;
            }
            
            return errors.Count == 0;
        }
    }


}
