﻿namespace AX2012ETWtracing.Configuration
{
    public class StatisticsConfiguration
    {
        public int aggregationInterval { get; set; } = 60000;

        public StatisticsConfiguration(int aggregationInterval)
        {
            this.aggregationInterval = aggregationInterval;
        }

        //questa parte è troppo ax like, ma per ora la lasciamo cosi

        public HistogramsConfiguration histograms { get; set; } = new HistogramsConfiguration();


    }

    public class HistogramsConfiguration
    {
        public HistogramLevel[] histogramXPP { get; set; } = new HistogramLevel[] {
                new HistogramLevel() { label="50ms", threshold=50000000  },
                new HistogramLevel() { label="100ms", threshold=100000000 },
                new HistogramLevel() { label="200ms", threshold=200000000 },
                new HistogramLevel() { label="500ms", threshold=500000000 },
                new HistogramLevel() { label="1sec", threshold=1000000000 },
                new HistogramLevel() { label="2sec", threshold=2000000000 },
                new HistogramLevel() { label="over2sec" }
            };

        public HistogramLevel[] histogramRPC { get; set; } = new HistogramLevel[] {
                new HistogramLevel() { label="50ms", threshold=50000000  },
                new HistogramLevel() { label="100ms", threshold=100000000 },
                new HistogramLevel() { label="200ms", threshold=200000000 },
                new HistogramLevel() { label="500ms", threshold=500000000 },
                new HistogramLevel() { label="1sec", threshold=1000000000 },
                new HistogramLevel() { label="2sec", threshold=2000000000 },
                new HistogramLevel() { label="5sec", threshold=5000000000 },
                new HistogramLevel() { label="10sec", threshold=10000000000 },
                new HistogramLevel() { label="over10sec" }
            };

        public HistogramLevel[] histogramSQLStmt { get; set; } = new HistogramLevel[] {
                new HistogramLevel() { label="50ms", threshold=50000000  },
                new HistogramLevel() { label="100ms", threshold=100000000 },
                new HistogramLevel() { label="200ms", threshold=200000000 },
                new HistogramLevel() { label="500ms", threshold=500000000 },
                new HistogramLevel() { label="1sec", threshold=1000000000 },
                new HistogramLevel() { label="2sec", threshold=2000000000 },
                new HistogramLevel() { label="over2sec" }
            };

        public HistogramLevel[] histogramSQLModelStoredProc { get; set; } = new HistogramLevel[] {
                new HistogramLevel() { label="50ms", threshold=50000000  },
                new HistogramLevel() { label="100ms", threshold=100000000 },
                new HistogramLevel() { label="200ms", threshold=200000000 },
                new HistogramLevel() { label="500ms", threshold=500000000 },
                new HistogramLevel() { label="1sec", threshold=1000000000 },
                new HistogramLevel() { label="2sec", threshold=2000000000 },
                new HistogramLevel() { label="over2sec" }
            };

        public HistogramLevel[] histogramFetch { get; set; } = new HistogramLevel[] {
                new HistogramLevel() { label="50ms", threshold=50000000  },
                new HistogramLevel() { label="100ms", threshold=100000000 },
                new HistogramLevel() { label="200ms", threshold=200000000 },
                new HistogramLevel() { label="500ms", threshold=500000000 },
                new HistogramLevel() { label="1sec", threshold=1000000000 },
                new HistogramLevel() { label="2sec", threshold=2000000000 },
                new HistogramLevel() { label="over2sec" }
            };

        public HistogramLevel[] histogramFetchCumu { get; set; } = new HistogramLevel[] {
                new HistogramLevel() { label="50ms", threshold=50000000  },
                new HistogramLevel() { label="100ms", threshold=100000000 },
                new HistogramLevel() { label="200ms", threshold=200000000 },
                new HistogramLevel() { label="500ms", threshold=500000000 },
                new HistogramLevel() { label="1sec", threshold=1000000000 },
                new HistogramLevel() { label="2sec", threshold=2000000000 },
                new HistogramLevel() { label="over2sec" }
            };

    }

    public class HistogramLevel
    {
        public string label { get; set; }
        public long threshold { get; set; }
    }
}