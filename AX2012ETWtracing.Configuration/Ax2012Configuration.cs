﻿using System;
using System.Collections.Generic;

namespace AX2012ETWtracing.Configuration
{
    public class Ax2012Configuration
    {
        public bool axaos { get; set; } = false;
        public bool activateBindpars { get; set; } = false;
        public bool axclient { get; set; } = false;
        public bool traceAxTransitions { get; set; } = false;//undocumented

        public string[] validInstances { get; set; } = new string[0];

        public Outputs output { get; set; } = new Outputs();
        
        public void Test(ref List<string> warnings, ref List<string> errors)
        {
            warnings = new List<string>();
            errors = new List<string>();
            //accetta solo uno dei due tipi!!!
            if (axaos ^ axclient)
            {
                //accettabile
            }
            else
            {
                //non accettabile
                errors.Add("ax2012.axaos AND ax2012.axclient values are missing or in conflict");
            }

            if (output.hasOutput)
            {
                //accettabile
                output.nats.Test(ref warnings, ref errors);
            }
            else
            {
                //non accettabile
                errors.Add("ax2012.output.nats configuration is missing");
            }
            if (output.useTornado)
            {
                //accettabile
                output.tornado.Test(ref warnings, ref errors);
            }
            else
            {
                //non accettabile, andrebbe verificato tipo
                warnings.Add("ax2012.output.tornado configuration is missing");
            }

            //messaggio numero di istanze
        }

        internal void Init()
        {
            if (output.hasOutput && output.useTornado)
            {
                if (string.IsNullOrEmpty(output.tornado.address))
                    output.tornado.address = output.nats.address;
                else
                    output.tornado.openconnection = true;

                //questa parte è sostanzialmente inutile....
                if (string.IsNullOrEmpty(output.tornado.tls_ca))
                    output.tornado.tls_ca = output.nats.tls_ca;
                else
                    output.tornado.openconnection = true;

                //solo se address non è cambiato
                if (!output.tornado.openconnection)
                    //solo se secure non è impostato (possibili casini se nel tornado scrivo secure false e in nats secure true
                    //
                    if (!output.tornado.secure)
                    {
                        output.tornado.secure = output.nats.secure;
                        //copio i settings da nats
                        if (string.IsNullOrEmpty(output.tornado.tls_cert) && string.IsNullOrEmpty(output.tornado.tls_key))
                        {
                            output.tornado.tls_cert = output.nats.tls_cert;
                            output.tornado.tls_key = output.nats.tls_key;
                        }
                        else if (string.IsNullOrEmpty(output.tornado.tls_name))
                        {
                            output.tornado.tls_name = output.nats.tls_name;
                        }
                        else if (string.IsNullOrEmpty(output.tornado.tls_pfx))
                        {
                            output.tornado.tls_pfx = output.nats.tls_pfx;
                        }
                    }

            }

            var assembly = System.Reflection.Assembly.GetExecutingAssembly();
            var assemblyName = assembly.GetName().Name;

            if (output.hasOutput)
            {
                //facciamo load di valori preselvati, in ogni caso
                output.nats.LoadRetentionPolicies(assembly);
            }
        }

        [Nett.TomlIgnore]
        public TracingConfiguration tracing
        {
            get
            {


                return axaos ?
                    (
                    activateBindpars ?
                    new TracingConfiguration("Microsoft-DynamicsAX-Tracing", "0xb6") :
                    new TracingConfiguration("Microsoft-DynamicsAX-Tracing", "0x36")
                    ) :
                    new TracingConfiguration("Microsoft-DynamicsAX-Tracing", "0x2000000000000003");//0x36
            }
        }

        [Nett.TomlIgnore]
        public StatisticsConfiguration statistic
        {
            get
            {
                return new StatisticsConfiguration(60000);
            }
        }
    }
}