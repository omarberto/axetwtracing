﻿namespace AX2012ETWtracing.Configuration
{
    public class ConfigurationBase
    {
        public GeneralConfiguration general { get; set; } = new GeneralConfiguration();

        public StatisticsConfiguration tracing { get; set; }
    }
}