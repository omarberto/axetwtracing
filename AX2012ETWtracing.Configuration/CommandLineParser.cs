﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AX2012ETWtracing.Configuration
{
    public class CommandLineParser
    {
        public bool isUserlevel { private set; get; } = false;
        public bool isInstall { private set; get; } = false;
        public bool isUninstall { private set; get; } = false;

        public string ConfigPath { private set; get; }

        public string SvcName { get; set; } = "AxETWTracing";
        public string DisplayName { private set; get; } = "Ax ETW tracing";
        public string UserName { private set; get; } = "";
        public bool settedUsername { get { return !string.IsNullOrEmpty(UserName); } }
        private string _Password = "";
        public string Password { private set { _Password = value; }
            get
            {
                if (settedPassword)
                    return _Password;
                else
                    return PromptForPassword();
            }
        }
        public bool settedPassword { get { return !string.IsNullOrEmpty(_Password); } }

        //parte equivalente a config


        public bool tracing { get; private set; } = false;
        public string providerName { get; private set; } = "Microsoft-DynamicsAX-Tracing";
        public ulong keywords { get; private set; } = 0x0;
        public int BufferSizeMB { get; private set; } = 512;
        public bool consoleOut { get; private set; } = false;
        public string etlOutputPath { get; private set; } = string.Format(@".\rec_{0:yyyyMMdd_HHmmss}.etl", DateTime.Now);

        public string etlInputPath { get; private set; } = "";
        public List<string> users { get; private set; } = new List<string>();
        public List<string> instances { get; private set; } = new List<string>();
        public List<int> sessions { get; private set; } = new List<int>();
        public string logBeginMessage { get; private set; } = "";
        public string logEndMessage { get; private set; } = "";
        public int timeoutSeconds { get; private set; } = 0;
        public bool hasTimeout { get { return timeoutSeconds > 0; } }

        public bool test_tornado { get; private set; }

        internal string PromptForPassword()
        {
            Console.Write("\npassword:");
            string pass = "";
            do
            {
                ConsoleKeyInfo key = Console.ReadKey(true);
                // Backspace Should Not Work
                if (key.Key != ConsoleKey.Backspace && key.Key != ConsoleKey.Enter)
                {
                    pass += key.KeyChar;
                    Console.Write("*");
                }
                else
                {
                    if (key.Key == ConsoleKey.Backspace && pass.Length > 0)
                    {
                        pass = pass.Substring(0, (pass.Length - 1));
                        Console.Write("\b \b");
                    }
                    else if (key.Key == ConsoleKey.Enter)
                    {
                        break;
                    }
                }
            } while (true);
            return pass;
        }

        public static void ExecuteCmd(string cmdText)
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "cmd.exe";
            startInfo.Arguments = string.Format("/C {0}", cmdText);
            process.StartInfo = startInfo;
            process.Start();
        }

        public CommandLineParser(string[] args)
        {

            for (int i = 0; i < args.Length; i++)
            {
                string arg = args[i].ToLower();
                switch (arg)
                {
                    case "--userlevel":
                    case "-userlevel":
                        this.isUserlevel = true;
                        break;

                    case "--install":
                    case "-install":
                        this.isInstall = true;
                        break;
                    case "--uninstall":
                    case "-uninstall":
                        this.isUninstall = true;
                        break;
                        
                    case "--name":
                    case "-name":
                        this.SvcName = args[++i];
                        break;
                    case "--displayname":
                    case "-displayname":
                        this.DisplayName = args[++i];
                        break;
                    case "--username":
                    case "-username":
                        this.UserName = args[++i].Trim();
                        break;
                    case "--password":
                    case "-password":
                        this.Password = args[++i];
                        break;

                    case "--config":
                    case "-config":
                        this.ConfigPath = args[++i];
                        break;

                    /**************************************************************/


                    case "-trace":
                    case "--trace":
                        {
                            this.tracing = true;
                        }
                        break;

                        
                    case "--test-tornado":
                        {
                            this.test_tornado = true;
                        }
                        break;

                    case "-provider":
                    case "--provider":
                        {
                            //per ora lo forzo al default solito, però se leggo da file forse sarebbe meglio di no (anche se forse in quel caso ignoro completamente la cosa
                            this.providerName = args[++i].Trim();
                        }
                        break;
                    case "-keywords"://0x2bb
                    case "--keywords":
                        {
                            this.keywords = Convert.ToUInt64(args[++i], 16);
                        }
                        break;
                    case "-buffersize"://0x2bb
                    case "--buffersize":
                        {
                            this.BufferSizeMB = Convert.ToInt32(args[++i]);
                        }
                        break;
                    case "-outfile":
                    case "--outfile":
                        {
                            this.etlOutputPath = args[++i].Trim();
                        }
                        break;
                    case "-console":
                    case "--console":
                        {
                            this.consoleOut = true;
                        }
                        break;

                    case "-user":
                    case "--user":
                        {
                            this.users = args[++i].Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(user => user.Trim().ToLowerInvariant()).ToList();
                        }
                        break;
                    case "-instance":
                    case "--instance":
                        {
                            this.instances = args[++i].Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(instance => instance.Trim()).ToList();
                        }
                        break;
                    case "-session":
                    case "--session":
                        {
                            this.sessions = args[++i].Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(session => int.Parse(session.Trim())).ToList();
                        }
                        break;
                    case "-infile":
                    case "--infile":
                        {
                            this.etlInputPath = args[++i].Trim();
                        }
                        break;
                    case "-logbegin":
                    case "--logbegin":
                        {
                            this.logBeginMessage = args[++i].Trim();
                        }
                        break;
                    case "-logend":
                    case "--logend":
                        {
                            this.logEndMessage = args[++i].Trim();
                        }
                        break;
                    case "-sec":
                    case "--sec":
                        {
                            this.timeoutSeconds = Convert.ToInt32(args[++i]);
                        }
                        break;

                    /**************************************************************/

                    default:
                        {
                        }
                        break;
                }
            }

            //checks (forse poi si può mettere in altra classe


            if (isInstall && isUninstall)
                throw new Exception("bad command line argument");
        }
    }
}
