﻿namespace AX2012ETWtracing.Configuration
{
    public class Outputs
    {
        [Nett.TomlIgnore]
        public bool hasOutput
        {
            get
            {
                //per ora ne ho uno e basta... (di base è privilegiato nats)
                return nats != null;
            }
        }
        public Nats nats { set; get; }

        public Influxdb influxformat { set; get; } = new Influxdb();



        [Nett.TomlIgnore]
        public bool useTornado
        {
            get
            {
                return tornado != null && tornado.active;
            }
        }
        public Tornado tornado { set; get; }
    }
}