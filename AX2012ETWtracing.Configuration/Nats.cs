﻿using NATS.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace AX2012ETWtracing.Configuration
{
    public class Nats
    {
        public string address { get; set; }
        public string subject { get; set; }
        public int queueMaxSize { get; set; } = 1048576;
        public int dequeueMaxCount { get; set; } = 1000;

        public bool secure { get; set; } = false;
        public string tls_ca { get; set; } = "";
        private string _tls_cert = "";
        public string tls_cert { get { return _tls_cert; } set { _tls_cert = Path.IsPathRooted(value) ? value : Path.GetFullPath(value); } }
        private string _tls_key = "";
        public string tls_key { get { return _tls_key; } set { _tls_key = Path.IsPathRooted(value) ? value : Path.GetFullPath(value); } }
        public string tls_pfx { get; set; } = "";
        public string tls_name { get; set; } = "";

        public enum TLSmechType
        {
            none,
            type_files,//due files, linux-like
            type_pfx,//sempre file ma linux like
            type_store//
        }
        [Nett.TomlIgnore]
        public TLSmechType mechType
        {
            get
            {
                if (!secure) return TLSmechType.none;
                if (!string.IsNullOrEmpty(tls_name)) return TLSmechType.type_store;
                if (!string.IsNullOrEmpty(tls_pfx)) return TLSmechType.type_pfx;
                if (!string.IsNullOrEmpty(tls_key) && !string.IsNullOrEmpty(tls_cert)) return TLSmechType.type_files;
                return TLSmechType.none;
            }
        }
        [Nett.TomlIgnore]
        public X509Certificate2 certificate
        {
            get
            {
                switch (mechType)
                {
                    case TLSmechType.type_files:
                        {
                            string certificateText = File.ReadAllText(tls_cert);
                            string privateKeyText = File.ReadAllText(tls_key);
                            var certificate = new MyOpenSSL.CertificateFromFileProvider(certificateText, privateKeyText, false).Certificate;
                            return certificate;
                        }
                    case TLSmechType.type_pfx:
                        return new X509Certificate2(tls_pfx);
                    case TLSmechType.type_store:
                        {
                            X509Store store = new X509Store();
                            store.Open(OpenFlags.OpenExistingOnly);

                            foreach (X509Certificate2 cert in store.Certificates)
                            {
                                if (cert.GetNameInfo(X509NameType.SimpleName, false) == tls_name) return cert;
                            }
                            return new X509Certificate2();
                        }
                    default:
                        return new X509Certificate2();
                }
            }
        }

        public Nett.TomlTable retention_policy_tag { set; get; }
        [Nett.TomlIgnore]
        public string default_retention_policy_tag { set; get; }
        [Nett.TomlIgnore]
        public bool active_retention_policy_tag { set; get; }
        [Nett.TomlIgnore]
        public string retention_policy_tagname { set; get; }
        [Nett.TomlIgnore]
        public Dictionary<string, string> retention_policy_tags { set; get; } = new Dictionary<string, string>();
        public string getRetentionTag(string measurement)
        {
            if (active_retention_policy_tag)
            {
                if (retention_policy_tags.ContainsKey(measurement))
                {
                    //Console.WriteLine("{2} default_retention_policy_tag {0} {1}", measurement, default_retention_policy_tag, string.IsNullOrEmpty(default_retention_policy_tag));

                    return "," + retention_policy_tagname + "=" + retention_policy_tags[measurement];
                }
                //Console.WriteLine("{2} default_retention_policy_tag {0} {1}", measurement, default_retention_policy_tag, string.IsNullOrEmpty(default_retention_policy_tag));
                //Console.ReadKey(true);
                return "," + retention_policy_tagname + "=" + default_retention_policy_tag;
            }
            return string.Empty;
        }
        internal void LoadRetentionPolicies(System.Reflection.Assembly assembly)
        {
            using (var stream = assembly.GetManifestResourceStream(assembly.GetName().Name + ".NATS.retention_policies_tag.conf"))
            {
                var internalS = Nett.Toml.ReadStream(stream);
                var retention_policy_tag_settings = (Nett.TomlTable)internalS.Values.First();
                foreach (var retention_policy_tagEl in retention_policy_tag_settings)
                {
                    if (retention_policy_tagEl.Value.TomlType == Nett.TomlObjectType.Table)
                    {
                        var retentionPolicy = retention_policy_tagEl.Value.Get<NATSadd.RetentionPolicy>();
                        if (retentionPolicy.hasMeasurements)
                        {
                            foreach (var measurement in retentionPolicy.measurements)
                            {
                                if (retention_policy_tags.ContainsKey(measurement))
                                    retention_policy_tags[measurement] = retention_policy_tagEl.Key;
                                else
                                    retention_policy_tags.Add(measurement, retention_policy_tagEl.Key);
                            }
                        }
                    }
                    else if (retention_policy_tagEl.Key == "active" && retention_policy_tagEl.Value.TomlType == Nett.TomlObjectType.Bool)
                        active_retention_policy_tag = retention_policy_tagEl.Value.Get<bool>();
                    else if (retention_policy_tagEl.Key == "default" && retention_policy_tagEl.Value.TomlType == Nett.TomlObjectType.String)
                        default_retention_policy_tag = retention_policy_tagEl.Value.Get<string>();
                    else if (retention_policy_tagEl.Key == "tag_name" && retention_policy_tagEl.Value.TomlType == Nett.TomlObjectType.String)
                        retention_policy_tagname = retention_policy_tagEl.Value.Get<string>();
                }
            }
            //leggo da sqltrace per vedere se devo sovrascrivere
            if (retention_policy_tag != null)
            {
                foreach (var retention_policy_tagEl in retention_policy_tag)
                {
                    if (retention_policy_tagEl.Value.TomlType == Nett.TomlObjectType.Table)
                    {
                        //Console.WriteLine("B RETENTION retention_policy_tagname {0}: {1}", retention_policy_tagEl.Key, retention_policy_tagEl.Value.TomlType);
                        var retentionPolicy = retention_policy_tagEl.Value.Get<NATSadd.RetentionPolicy>();
                        if (retentionPolicy.hasMeasurements)
                        {
                            foreach (var measurement in retentionPolicy.measurements)
                            {
                                if (retention_policy_tags.ContainsKey(measurement))
                                    retention_policy_tags[measurement] = retention_policy_tagEl.Key;
                                else
                                    retention_policy_tags.Add(measurement, retention_policy_tagEl.Key);
                            }
                        }
                    }
                    else if (retention_policy_tagEl.Key == "active" && retention_policy_tagEl.Value.TomlType == Nett.TomlObjectType.Bool)
                        active_retention_policy_tag = retention_policy_tagEl.Value.Get<bool>();
                    else if (retention_policy_tagEl.Key == "default" && retention_policy_tagEl.Value.TomlType == Nett.TomlObjectType.String)
                        default_retention_policy_tag = retention_policy_tagEl.Value.Get<string>();
                    else if (retention_policy_tagEl.Key == "tag_name" && retention_policy_tagEl.Value.TomlType == Nett.TomlObjectType.String)
                        retention_policy_tagname = retention_policy_tagEl.Value.Get<string>();
                }
            }
            if (string.IsNullOrEmpty(retention_policy_tagname)) active_retention_policy_tag = false;
            if (string.IsNullOrEmpty(default_retention_policy_tag)) active_retention_policy_tag = false;

            //Console.WriteLine("RETENTION");
            //Console.WriteLine("RETENTION retention_policy_tagname {0}", retention_policy_tagname);
            //Console.WriteLine("RETENTION default_retention_policy_tag {0}", default_retention_policy_tag);
            //Console.WriteLine("RETENTION active_retention_policy_tag {0}", active_retention_policy_tag);
            //foreach (var v in retention_policy_tags)
            //    Console.WriteLine("RETENTION active_retention_policy_tag {0}:{1}", v.Key, v.Value);
            //Console.ReadKey(true);
        }

        public virtual void Test(ref List<string> warnings, ref List<string> errors)
        {
            if (string.IsNullOrEmpty(subject.Trim()))
            {
                //eccezione
                errors.Add("ax2012.output.nats subject is empty or missing");
            }

            if (string.IsNullOrEmpty(address.Trim()))
            {
                errors.Add("ax2012.output.nats address is empty or missing");
            }
            else
            {
                //test connessione
                if (secure)
                {
                    bool test = true;
                    if (!System.IO.File.Exists(tls_cert))
                    {
                        //eccezione
                        errors.Add("ax2012.output.nats tls_cert file does not exist");
                        test = false;
                    }
                    if (!System.IO.File.Exists(tls_key))
                    {
                        //eccezione
                        errors.Add("ax2012.output.nats tls_key file does not exist");
                        test = false;
                    }
                    if (test)
                    {
                        using (var sender = new InfluxTestDataSend(address, certificate))
                        {
                            sender.Connect();
                            warnings.AddRange(sender.warnings);
                            errors.AddRange(sender.errors);
                        }
                    }
                }
                else
                {
                    using (var sender = new InfluxTestDataSend(address))
                    {
                        sender.Connect();
                        warnings.AddRange(sender.warnings);
                        errors.AddRange(sender.errors);
                    }
                }
            }
        }
    }

    public class Tornado : Nats
    {
        public bool openconnection { get; set; } = false;

        [Nett.TomlIgnore]
        public bool active { get { return !string.IsNullOrEmpty(topic); } }
        public string topic { get; set; }// = "tornado_events_itoa";
        public string hostnameformat { get; set; } = "fullqualified";//hostonly
        public int keepaliveInterval { get; set; } = 5;
        public int monitordataInterval { get; set; } = 30;


        public override void Test(ref List<string> warnings, ref List<string> errors)
        {
            if (string.IsNullOrEmpty(topic.Trim()))
            {
                //eccezione
                errors.Add("ax2012.output.tornado topic is empty or missing");
            }

            if (string.IsNullOrEmpty(address.Trim()))
            {
                errors.Add("ax2012.output.tornado address is empty or missing");
            }
            else
            {
                //test connessione

                if (secure)
                {
                    bool test = true;
                    if (!System.IO.File.Exists(tls_cert))
                    {
                        //eccezione
                        errors.Add("ax2012.output.tornado tls_cert file does not exist");
                        test = false;
                    }
                    if (!System.IO.File.Exists(tls_key))
                    {
                        //eccezione
                        errors.Add("ax2012.output.tornado tls_key file does not exist");
                        test = false;
                    }
                    if (test)
                    {
                        using (var sender = new InfluxTestDataSend(address, certificate))
                        {
                            sender.Connect();
                            warnings.AddRange(sender.warnings);
                            errors.AddRange(sender.errors);
                        }
                    }
                }
                else
                {
                    using (var sender = new InfluxTestDataSend(address))
                    {
                        sender.Connect();
                        warnings.AddRange(sender.warnings);
                        errors.AddRange(sender.errors);
                    }
                }
            }
        }
    }

    public abstract class InfluxDataSend : IDisposable
    {
        protected IConnection nats_connection = null;
        private Options opts;

        private bool verifyServerCert(object sender, X509Certificate certificate, X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            if (sslPolicyErrors == System.Net.Security.SslPolicyErrors.None)
                return true;

            foreach (var chainElement in chain.ChainElements)
            {
                //Console.WriteLine(chainElement.Information);
            }

            return true;
        }
        public InfluxDataSend(string address, X509Certificate2 certificate = null)
        {
            this.opts = NATS.Client.ConnectionFactory.GetDefaultOptions();
            this.opts.Url = address;
            this.opts.AllowReconnect = true;
            this.opts.MaxReconnect = int.MaxValue;
            if (certificate != null)
            {
                this.opts.Secure = true;
                this.opts.TLSRemoteCertificationValidationCallback = verifyServerCert;
                this.opts.AddCertificate(certificate);
            }



            this.opts.AsyncErrorEventHandler += (sender, args) =>
            {
                ErrorMessage(true, "NATS.io AsyncErrorEventHandler Server: {0}\n Message: {1}\n Subject: {2}", args.Conn.ConnectedUrl, args.Error, args.Subscription.Subject);
            };

            this.opts.ReconnectedEventHandler += (sender, args) =>
            {
                ErrorMessage(false, "NATS.io ReconnectedEventHandler: {0}\n State: {1}", args.Conn.ConnectedUrl, args.Conn.State);
            };

            this.opts.ClosedEventHandler += (sender, args) =>
            {
                ErrorMessage(false, "NATS.io ClosedEventHandler: {0}\n State: {1}", args.Conn.ConnectedUrl, args.Conn.State);
            };

            this.opts.DisconnectedEventHandler += (sender, args) =>
            {
                ErrorMessage(false, "NATS.io DisconnectedEventHandler: {0}\n State: {1}", args.Conn.ConnectedUrl, args.Conn.State);
            };
        }

        public bool Connect()
        {
            //si potrebbe mettere in classe
            var cf = new NATS.Client.ConnectionFactory();

            try
            {
                nats_connection = cf.CreateConnection(this.opts);
                return true;
            }
            catch (Exception e)
            {
                ErrorMessage(true, "nats_connection CreateConnection() for {1} exception: {0}", e.Message, this.opts.Url);
                return false;
            }
        }


        public void Dispose()
        {
            try
            {
                if (!nats_connection.IsClosed())
                    nats_connection.Close();
            }
            catch (Exception e)
            {
                ErrorMessage(true, "nats_connection.Close() exception: {0}", e.Message);
            }

            try
            {
                nats_connection.Dispose();
            }
            catch (Exception e)
            {
                ErrorMessage(true, "nats_connection.Dispose() exception: {0}", e.Message);
            }
        }

        public abstract void ErrorMessage(bool error, string format, params object[] args);
    }

    internal class InfluxTestDataSend : InfluxDataSend
    {

        public List<string> warnings = new List<string>();
        public List<string> errors = new List<string>();

        public InfluxTestDataSend(string address, X509Certificate2 certificate = null) : base(address, certificate)
        {

        }

        public override void ErrorMessage(bool error, string format, params object[] args)
        {
            if (error)
                errors.Add(string.Format(format, args));
            else
                warnings.Add(string.Format(format, args));
        }
    }
}