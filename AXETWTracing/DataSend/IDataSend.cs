﻿using AxETWTracing.Statistics;

namespace AxETWTracing.DataSend
{ 
    public class Heartbeat
    {
        public int servers { set; get; }
        public long RPC { set; get; }
        public long SQL { set; get; }
        public long MDL { set; get; }
        public long FTC { set; get; }

        public int clients { set; get; }
        public int CLI { set; get; }

        public ulong Ntotal { set; get; }
        public int Nlost { set; get; }

        public string AxServer { set; get; }
    }

    public interface INATSSend
    {
        //    void Publish(AxInstanceUserSession id, IRPCStatistic statistics, SQLsessionDescr description);
        //    void Publish(AxInstanceUserSession id, ISQLStatistic statistics, SQLsessionDescr description);
        //    void Publish(AxInstanceUserSession id, IModelStoreStatistic statistics, SQLsessionDescr description);
        //    void Publish(AxInstanceUserSession id, IFETCHStatistic statistics, SQLsessionDescr description);
        //    void Publish(string measurement, AxInstanceUserSession id, SQLsessionDescr description, long WholeDuration, long nsTimeStampEpoch);

        void PublishEvent(string measurement, long nsTimeStampEpoch, string fields, string tags);
        void Publish(SendStatistics tmp);
        void PublishBatchBegin(AxServerInstance aosInstance, AxSessionId axSession, AxUserName axUserName, long nsBatchEpochStart, string batchRecId);
        void PublishBatchEnd(AxServerInstance aosInstance, AxSessionId axSession, AxUserName axUserName, long nsBatchEpochStart, long nsBatchEpochEnd, string batchRecId, string status);
        void PublishBatchRecovery(AxServerInstance aosInstance, AxSessionId axSession, AxUserName axUserName, long nsBatchEpochStart, long nsBatchEpochEnd, string batchRecId, string status);
        void PublishBeginSession(AxServerInstance aosInstance, AxSessionId axSession, AxUserName axUserName, string clientType, string computerName, long nsTimeStampEpoch);
        void PublishEndSession(AxServerInstance aosInstance, AxSessionId axSession, AxUserName axUserName, string clientType, string computerName, long nsClientTypeDuration, long nsTimeStampEpoch);

        //void PublishEvent(string retentionPolicy, string measurement, long nsTimeStampEpoch, string fields, string tags);

        void PublishHeartbeat(Heartbeat heartbeat, long nsTimeStampEpoch);
    }
}