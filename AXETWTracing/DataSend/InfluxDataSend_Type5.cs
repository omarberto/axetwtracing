﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AxETWTracing.Statistics;
using AxETWTracing.Statistics.FETCH;
using AxETWTracing.Statistics.SQL;
using System.Diagnostics;
using AxETWTracing.Statistics.RPC;
using AxETWTracing.Statistics.XPP;
using AxETWTracing.Statistics.ModelStoreProc;
using System.Security.Cryptography.X509Certificates;

namespace AxETWTracing.DataSend
{
    internal class InfluxDataSend_Type5 : InfluxDataSend, INATSSend
    {
        public InfluxDataSend_Type5(string address, string subject, X509Certificate2 certificate):base(address, subject, certificate)
        {
        }
        
        private string PrintHistogram_ByField(string histogramFieldName, string[] labels, int[] histogram)
        {
            //Logging.Log.Instance.Debug("PrintHistogram_ByField: {0} {1} {2}", histogramFieldName, labels.Length, histogram.Length);
            string influxInsert = string.Empty;

            for (int ih = 0; ih < labels.Length; ih++)
            {
                //Logging.Log.Instance.Debug("PrintHistogram_ByField: {0} {1} {2}", histogramFieldName, labels[ih], histogram[ih]);
                if (histogram[ih] == 0) continue;
                //field
                influxInsert += string.Format(",{0}_{1}={2}i", histogramFieldName, labels[ih], histogram[ih]);
            }

            return influxInsert;
        }

        public void PublishEvent(string measurement, long nsTimeStampEpoch, string fields, string tags)
        {
            string rpTag = AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.nats.getRetentionTag(measurement);
            var b = new StringBuilder(measurement + rpTag);
            if (!string.IsNullOrEmpty(tags))
                b.Append(tags);
            b.Append(" ");
            b.Append(fields);
            b.Append(" ");
            b.Append(nsTimeStampEpoch);

            string influxInsert = b.ToString();

            //Logging.Log.Instance.Debug("PublishEvent: " + influxInsert);

            Logging.Log.Instance.Debug("PublishEvent {0} ", influxInsert);


            Publish(Encoding.UTF8.GetBytes(influxInsert));
        }

        public void Publish(SendStatistics stats)
        {
            //Logging.Log.Instance.Debug("publish {0:HH:mm:ss.fff} {1:HH:mm:ss.fff} -> {2}:{3} --> {4} {5} {6}", new EpochNanoseconds(stats.nsTimeStampEpoch).dateTime, DateTime.UtcNow, stats.axUserName.Value
            //    , stats.axSession.Value, stats.SQL_stmt_count, stats.SQL_stmt_duration, stats.SQL_stmt_preparation_duration);


            string rpTag = AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.nats.getRetentionTag(AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.influxformat.measurementName);
            string influxInsert = AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.influxformat.measurementName + rpTag;
            //TAGS
            influxInsert += string.Format(",AxInstance={0},AxUser={1},AxSession={2} ", stats.aosInstance.Value, stats.axUserName.ASCII, stats.axSession.Value);
            //FIELDS
            //questa parte vedo come giocarmela, adesso in linea di principio posso evitarmi il meccanismo del sessionIndex//quindi se é null non scrivo nulla...
            //influxInsert += string.Format(" SessionType='{0}',ClientServer='{1}',StartTime='{2:dd-MM-yyyy HH:mm:ss}'", description.sessionTypeExt, description.clientServer, description.logindatetime);

            int commaIndex = influxInsert.Length;

            bool sendXPP = false;
            if (stats.XPP_nestlevel0_count > 0)
            {
                influxInsert += string.Format(",XPP_nestlevel0_count={0}i,XPP_nestlevel0_duration={1}i", stats.XPP_nestlevel0_count, stats.XPP_nestlevel0_duration / 1000000);
                sendXPP = true;
            }
            if (stats.XPP_all_count > 0)
            {
                influxInsert += string.Format(",XPP_all_count={0}i", stats.XPP_all_count);
                sendXPP = true;
            }
            if (stats.XPP_whole_duration > 0)
            {
                influxInsert += string.Format(",XPP_whole_duration={0}i", stats.XPP_whole_duration);
                sendXPP = true;
            }

            bool sendClientType = false;
            if (stats.nsClientTypeDuration > 0)
            {
                //Logging.Log.Instance.Debug(",{0}_duration={1}i", stats.clientType, stats.nsClientTypeDuration);
                //influxInsert += string.Format(",{0}_duration={1}i,computerName=\"{2}\"", stats.clientType, stats.nsClientTypeDuration, stats.computerName);
                influxInsert += string.Format(",clientType=\"{0}\",session_duration={1}i,computerName=\"{2}\"", stats.clientType, stats.nsClientTypeDuration, stats.computerName);
                sendClientType = true;
            }
            bool sendBatchType = false;
            if (stats.nsBatchDuration > 0)
            {
                influxInsert += string.Format(",clientType=\"BATCH\",batchRecId=\"{0}\",session_duration={1}i,batch_duration={1}i,batch_status=\"{2}\",computerName=\"{3}\"", stats.batchRecId, stats.nsBatchDuration, stats.batchStatus, stats.aosInstance.ServerName);
                sendBatchType = true;
            }

            bool sendRPC = false;
            if (stats.RPC_nestlevel0_count > 0)
            {
                influxInsert += string.Format(",RPC_nestlevel0_count={0}i,RPC_nestlevel0_duration={1}i", stats.RPC_nestlevel0_count, stats.RPC_nestlevel0_duration / 1000000);
                sendRPC = true;
            }
            if (stats.RPC_all_count > 0)
            {
                influxInsert += string.Format(",RPC_all_count={0}i,RPC_server_duration={1}i", stats.RPC_all_count, stats.RPC_server_duration / 1000000);
                sendRPC = true;
            }
            if (stats.RPC_client_duration > 0)
            {
                influxInsert += string.Format(",RPC_client_duration={0}i", stats.RPC_client_duration / 1000000);
                sendRPC = true;
            }
            if (stats.RPC_whole_duration > 0)
            {
                influxInsert += string.Format(",RPC_whole_duration={0}i", stats.RPC_whole_duration);
                sendRPC = true;
            }
            if (stats.RPC_bytes_recd > 0)
            {
                influxInsert += string.Format(",RPC_bytes_recd={0}i", stats.RPC_bytes_recd);
                sendRPC = true;
            }
            if (stats.RPC_bytes_sent > 0)
            {
                influxInsert += string.Format(",RPC_bytes_sent={0}i", stats.RPC_bytes_sent);
                sendRPC = true;
            }


            bool sendSQL = false;
            if (stats.SQL_stmt_count > 0)
            {
                influxInsert += string.Format(",SQL_stmt_count={0}i", stats.SQL_stmt_count);
                sendSQL = true;
            }
            if (sendSQL && stats.stmtType_call > 0)
            {
                influxInsert += string.Format(",SQL_stmtType_call={0}i", stats.stmtType_call);
            }
            if (sendSQL && stats.stmtType_delete > 0)
            {
                influxInsert += string.Format(",SQL_stmtType_delete={0}i", stats.stmtType_delete);
            }
            if (sendSQL && stats.stmtType_insert > 0)
            {
                influxInsert += string.Format(",SQL_stmtType_insert={0}i", stats.stmtType_insert);
            }
            if (sendSQL && stats.stmtType_none > 0)
            {
                influxInsert += string.Format(",SQL_stmtType_none={0}i", stats.stmtType_none);
            }
            if (sendSQL && stats.stmtType_other > 0)
            {
                influxInsert += string.Format(",SQL_stmtType_other={0}i", stats.stmtType_other);
            }
            if (sendSQL && stats.stmtType_select > 0)
            {
                influxInsert += string.Format(",SQL_stmtType_select={0}i", stats.stmtType_select);
            }
            if (sendSQL && stats.stmtType_update > 0)
            {
                influxInsert += string.Format(",SQL_stmtType_update={0}i", stats.stmtType_update);
            }

            if (stats.SQL_stmt_duration > 0)
            {
                influxInsert += string.Format(",SQL_stmt_duration={0}i", stats.SQL_stmt_duration / 1000000);
                sendSQL = true;
            }
            if (stats.SQL_stmt_preparation_duration > 0)
            {
                influxInsert += string.Format(",SQL_stmt_preparation_duration={0}i", stats.SQL_stmt_preparation_duration / 1000000);
                sendSQL = true;
            }


            bool sendMDL = false;
            if (stats.SQL_modelstoredproc_count > 0)
            {
                influxInsert += string.Format(",SQL_modelstoredproc_count={0}i", stats.SQL_modelstoredproc_count);
                sendMDL = true;
            }
            if (stats.SQL_modelstoredproc_duration > 0)
            {
                influxInsert += string.Format(",SQL_modelstoredproc_duration={0}i", stats.SQL_modelstoredproc_duration / 1000000);
                sendMDL = true;
            }
            if (stats.SQL_modelstoredproc_preparation_duration > 0)
            {
                influxInsert += string.Format(",SQL_modelstoredproc_preparation_duration={0}i", stats.SQL_modelstoredproc_preparation_duration / 1000000);
                sendMDL = true;
            }


            bool sendFTC = false;
            if (stats.FETCH_fetches_count > 0)
            {
                influxInsert += string.Format(",FETCH_fetches_count={0}i", stats.FETCH_fetches_count);
                sendFTC = true;
            }
            if (stats.FETCH_duration > 0)
            {
                influxInsert += string.Format(",FETCH_duration={0}i", stats.FETCH_duration / 1000000);
                sendFTC = true;
            }
            if (stats.FETCH_rows_count > 0)
            {
                influxInsert += string.Format(",FETCH_rows_count={0}i", stats.FETCH_rows_count);
                sendFTC = true;
            }
            if (stats.FETCH_fetchcumu_count > 0)
            {
                influxInsert += string.Format(",FETCH_fetchcumu_count={0}i", stats.FETCH_fetchcumu_count);
                sendFTC = true;
            }
            if (stats.FETCH_cumu_duration > 0)
            {
                influxInsert += string.Format(",FETCH_cumu_duration={0}i", stats.FETCH_cumu_duration / 1000000);
                sendFTC = true;
            }
            
            if (!sendXPP && !sendRPC && !sendSQL && !sendMDL && !sendFTC && !sendClientType && !sendBatchType) return;
            //quindi sono sicuro di poter eliminare la prima virgola
            influxInsert = influxInsert.Remove(commaIndex, 1);//testare questo...

            if (sendXPP)
                influxInsert += PrintHistogram_ByField("XPP", stats.XPP_histogram.labels, stats.XPP_histogram.ToArray());

            if (sendRPC)
                influxInsert += PrintHistogram_ByField("RPC", stats.RPC_histogram.labels, stats.RPC_histogram.ToArray());

            if (sendSQL)
                influxInsert += PrintHistogram_ByField("SQL_Stmt", stats.SQL_histogram.labels, stats.SQL_histogram.ToArray());
            
            if (sendMDL)
                influxInsert += PrintHistogram_ByField("SQL_ModelStored", stats.SQL_modelstoredproc_histogram.labels, stats.SQL_modelstoredproc_histogram.ToArray());
            
            if (sendFTC)
                influxInsert += PrintHistogram_ByField("FETCH", stats.FETCH_histogram.labels, stats.FETCH_histogram.ToArray());


            influxInsert += string.Format(" {0}", stats.nsTimeStampEpoch);

            Publish(Encoding.UTF8.GetBytes(influxInsert));
        }

        public void PublishBeginSession(AxServerInstance aosInstance, AxSessionId axSession, AxUserName axUserName, string clientType, string computerName, long nsTimeStampEpoch)
        {
            string rpTag = AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.nats.getRetentionTag(AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.influxformat.measurementName);
            string influxInsert = AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.influxformat.measurementName + rpTag;
            //TAGS
            influxInsert += string.Format(",AxInstance={0},AxUser={1},AxSession={2} ", aosInstance.Value, axUserName.ASCII, axSession.Value);
            
            influxInsert += string.Format(" clientType=\"{0}\",event_type=\"BEGIN\",computerName=\"{1}\"", clientType, computerName);

            influxInsert += string.Format(" {0}", nsTimeStampEpoch);

            Publish(Encoding.UTF8.GetBytes(influxInsert));
        }
        public void PublishEndSession(AxServerInstance aosInstance, AxSessionId axSession, AxUserName axUserName, string clientType, string computerName, long nsClientTypeDuration, long nsTimeStampEpoch)
        {
            string rpTag = AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.nats.getRetentionTag(AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.influxformat.measurementName);
            string influxInsert = AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.influxformat.measurementName + rpTag;
            //TAGS
            influxInsert += string.Format(",AxInstance={0},AxUser={1},AxSession={2} ", aosInstance.Value, axUserName.ASCII, axSession.Value);

            influxInsert += string.Format(" clientType=\"{0}\",session_duration={1}i,event_type=\"END\",computerName=\"{2}\"", clientType, nsClientTypeDuration, computerName);

            influxInsert += string.Format(" {0}", nsTimeStampEpoch);

            Publish(Encoding.UTF8.GetBytes(influxInsert));
        }

        public void PublishBatchRecovery(AxServerInstance aosInstance, AxSessionId axSession, AxUserName axUserName, long nsBatchEpochStart, long nsBatchEpochEnd, string batchRecId, string status)
        {
            long ns0 = nsBatchEpochStart / 60000000000L;
            //if (nsBatchEpochStart % 60000000000L > 0) --> tolgo perchè se start cade esatto al nanosecondo non voglio comunque registrare altro che start
                ns0++;
            ns0 *= 60000000000L;

            long ns1 = nsBatchEpochEnd / 60000000000L;
            if (nsBatchEpochEnd % 60000000000L > 0) ns1++;
            ns1 *= 60000000000L;

            //Logging.Log.Instance.Debug("PublishBatchRecovery A {0} {1}", nsBatchEpochStart, nsBatchEpochEnd);
            //Logging.Log.Instance.Debug("PublishBatchRecovery B {0} {1}", ns0, ns1);

            string rpTag = AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.nats.getRetentionTag(AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.influxformat.measurementName);
            string influxInsert = AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.influxformat.measurementName + rpTag;
            //TAGS
            influxInsert += string.Format(",AxInstance={0},AxUser={1},AxSession={2} ", aosInstance.Value, axUserName.ASCII, axSession.Value);

            {

                string tmp = influxInsert + string.Format("clientType=\"BATCH\",batchRecId=\"{0}\",event_type=\"BEGIN\",batch_status=\"{1}\",computerName=\"{2}\"", batchRecId, status, aosInstance.ServerName) + string.Format(" {0}", nsBatchEpochStart);

                Publish(Encoding.UTF8.GetBytes(tmp));
            }
            for (long nsBatchEpoch = ns0; nsBatchEpoch < nsBatchEpochEnd; nsBatchEpoch += 60000000000L)
            {
                string tmp = influxInsert + string.Format("clientType=\"BATCH\",batchRecId=\"{0}\",session_duration={1}i,batch_duration={1}i,batch_status=\"Executing\",computerName=\"{2}\"", batchRecId, (nsBatchEpoch - nsBatchEpochStart), aosInstance.ServerName) + string.Format(" {0}", nsBatchEpoch);

                Publish(Encoding.UTF8.GetBytes(tmp));
            }
            {

                string tmp = influxInsert + string.Format("clientType=\"BATCH\",batchRecId=\"{0}\",session_duration={1}i,batch_duration={1}i,event_type=\"END\",batch_status=\"{2}\",computerName=\"{3}\"", batchRecId, (nsBatchEpochEnd - nsBatchEpochStart), status, aosInstance.ServerName) + string.Format(" {0}", nsBatchEpochEnd);

                Publish(Encoding.UTF8.GetBytes(tmp));
            }
        }
        public void PublishBatchBegin(AxServerInstance aosInstance, AxSessionId axSession, AxUserName axUserName, long nsBatchEpochStart, string batchRecId)
        {
            string rpTag = AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.nats.getRetentionTag(AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.influxformat.measurementName);
            string influxInsert = AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.influxformat.measurementName + rpTag;
            //TAGS
            influxInsert += string.Format(",AxInstance={0},AxUser={1},AxSession={2} ", aosInstance.Value, axUserName.ASCII, axSession.Value);

            influxInsert += string.Format("clientType=\"BATCH\",batchRecId=\"{0}\",event_type=\"BEGIN\",batch_status=\"Executing\",computerName=\"{1}\"", batchRecId, aosInstance.ServerName) + string.Format(" {0}", nsBatchEpochStart);

            Publish(Encoding.UTF8.GetBytes(influxInsert));
        }
        public void PublishBatchEnd(AxServerInstance aosInstance, AxSessionId axSession, AxUserName axUserName, long nsBatchEpochStart, long nsBatchEpochEnd, string batchRecId, string status)
        {
            string rpTag = AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.nats.getRetentionTag(AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.influxformat.measurementName);
            string influxInsert = AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.influxformat.measurementName + rpTag;
            //TAGS
            influxInsert += string.Format(",AxInstance={0},AxUser={1},AxSession={2} ", aosInstance.Value, axUserName.ASCII, axSession.Value);

            influxInsert += string.Format("clientType=\"BATCH\",batchRecId=\"{0}\",session_duration={1}i,batch_duration={1}i,event_type=\"END\",batch_status=\"{2}\",computerName=\"{3}\"", batchRecId, (nsBatchEpochEnd - nsBatchEpochStart), status, aosInstance.ServerName) + string.Format(" {0}", nsBatchEpochEnd);

            Publish(Encoding.UTF8.GetBytes(influxInsert));
        }


        public void PublishHeartbeat(Heartbeat heartbeat, long nsTimeStampEpoch)
        {

            string rpTag = AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.nats.getRetentionTag("HEARTBEAT");
            string influxInsert = string.Format("HEARTBEAT" + rpTag + ",AxServer={0} servers={1}i,RPC={2}i,SQL={3}i,MDL={4}i,FTC={5}i,clients={6}i,CLI={7}i,Ntotal={8}i,Nlost={9}i {10}",
                heartbeat.AxServer,
                heartbeat.servers, heartbeat.RPC, heartbeat.SQL, heartbeat.MDL, heartbeat.FTC,
                heartbeat.clients, heartbeat.CLI,
                heartbeat.Ntotal, heartbeat.Nlost,
                nsTimeStampEpoch);

            Logging.Log.Instance.Debug("PublishEvent {0} ", influxInsert);

            Publish(Encoding.UTF8.GetBytes(influxInsert), true);
        }
    }

    internal class InfluxDataSend : AX2012ETWtracing.Configuration.InfluxDataSend
    {
        public string subject;
        public string server
        {
            get
            {
                return nats_connection.Servers[0];
            }
        }


        internal InfluxDataSend(string address, string subject, X509Certificate2 certificate = null) : base(address, certificate)
        {
            this.subject = subject;

            Connect();
        }

        public override void ErrorMessage(bool error, string format, params object[] args)
        {
            Logging.Log.Instance.Debug("ErrorMessage override: " + string.Format(format, args));

            //if (error)
            //    Logging.Log.Instance.Error("NATS.io AsyncErrorEventHandler: {0}\n Message: {1}\n Subject: {2}", args);
            //else
            //    Logging.Log.Instance.Warn("NATS.io AsyncErrorEventHandler: {0}\n Message: {1}\n Subject: {2}", args);
        }
        /****************  PARTE MESSAGGISTICA  **********/

        private Queue<byte[]> unplublishedMessages = new Queue<byte[]>();
        private int unplublishedMessagesSize = 0;
        protected void Publish(byte[] message, bool heartbeat = false)
        {
            bool connected = true;

            if (nats_connection == null || nats_connection.State == NATS.Client.ConnState.CLOSED)
                connected = heartbeat ? Connect() : false;


            try
            {
                if (connected)
                {
                    nats_connection.Publish(this.subject, message);
                    int dequeued = 0;
                    bool infoMsg = unplublishedMessages.Count > 0;
                    while (heartbeat && unplublishedMessages.Count > 0 && dequeued < AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.nats.dequeueMaxCount)
                    {
                        var old = unplublishedMessages.Dequeue();
                        unplublishedMessagesSize -= old.Length;
                        nats_connection.Publish(this.subject, old);
                        dequeued++;
                    }

                    Tornado.TornadoEnpointData.values.number_of_measurments_sent_to_nats++;

                    if (infoMsg) Logging.Log.Instance.Info("NATS {0} OLD MESSAGES DEQUEUED", dequeued);
                }
                else
                {
                    //mettiamo messaggi in coda
                    if (unplublishedMessagesSize > AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.nats.queueMaxSize)
                    {
                        var old = unplublishedMessages.Dequeue();
                        unplublishedMessagesSize -= old.Length;
                    }
                    unplublishedMessages.Enqueue(message);
                    unplublishedMessagesSize += message.Length;

                    Logging.Log.Instance.Info("NATS MESSAGE ENQUEUED SIZE {0} TOTAL {1}", message.Length, unplublishedMessagesSize);
                }
            }
            catch (Exception e)
            {
                //debugghiamo
                Logging.Log.Instance.Info(e, "Publish Exception for message {0}", Encoding.UTF8.GetString(message));
            }

        }

        
        public void PublishMessage(string subject, string message)
        {
            Publish(subject, Encoding.UTF8.GetBytes(message));
        }
        private void Publish(string subject, byte[] message)
        {
            bool connected = true;

            if (nats_connection == null || nats_connection.State == NATS.Client.ConnState.CLOSED)
                connected = Connect();
            
            try
            {
                if (connected)
                    nats_connection.Publish(subject, message);
            }
            catch (Exception e)
            {
                //debugghiamo
                Logging.Log.Instance.Error(e, "Publish Exception for message {0}", Encoding.UTF8.GetString(message));
            }
        }
    }
}
