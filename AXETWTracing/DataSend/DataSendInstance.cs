﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxETWTracing.DataSend
{
    public class DataSendInstance
    {
        private static INATSSend _instance;
        public void Init(INATSSend sender)
        {
            _instance = sender;
        }

        public INATSSend sender { get { return _instance; } }
    }
}
