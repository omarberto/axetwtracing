﻿namespace AxETWTracing
{
    internal class IndexedSessionName : SessionName
    {
        private int index;

        public IndexedSessionName(string providerName, int index = 0)
            : base(providerName)
        {
            this.index = index;
        }

        public override string Value
        {
            get
            {
                return base.Value + index;
            }
        }
    }
}