﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxETWTracing
{
    internal class EpochNanoseconds
    {
        private DateTimeKind dateTimeKind = DateTimeKind.Unspecified;

        public EpochNanoseconds(DateTime dateTime)
        {
            dateTimeKind = dateTime.Kind;
            TimeSpan t = dateTime - new DateTime(1970, 1, 1, 0, 0, 0, dateTime.Kind);
            Value = (long)(t.TotalMilliseconds * 1000000);
        }
        public EpochNanoseconds(long nsEpoch)
        {
            Value = nsEpoch;
        }

        public long Value
        {
            get;
            private set;
        }
        public DateTime dateTime
        {
            get
            {
                return new DateTime(1970, 1, 1, 0, 0, 0, dateTimeKind).Add(TimeSpan.FromMilliseconds(Value / 1000000D));
            }
        }
        public static EpochNanoseconds Now
        {
            get
            {
                return new EpochNanoseconds(DateTime.Now);
            }
        }
        public static EpochNanoseconds UtcNow
        {
            get
            {
                return new EpochNanoseconds(DateTime.UtcNow);
            }
        }
    }
    internal class EpochMilliseconds
    {
        public EpochMilliseconds(DateTime dateTime)
        {
            TimeSpan t = dateTime - new DateTime(1970, 1, 1, 0, 0, 0, dateTime.Kind);
            Value = (long)(t.TotalMilliseconds);
        }

        public long Value
        {
            get;
            private set;
        }
        public static EpochMilliseconds Now
        {
            get
            {
                return new EpochMilliseconds(DateTime.Now);
            }
        }
        public static EpochMilliseconds UtcNow
        {
            get
            {
                return new EpochMilliseconds(DateTime.UtcNow);
            }
        }
    }
}
