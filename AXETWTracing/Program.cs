﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.ServiceProcess;
using System.Threading.Tasks;

namespace AxETWTracing
{
    class Program : ServiceBase
    {
        public Program()
        {
        }
        public static string ConfigPath { get; internal set; }

        private static Service svc = null;
        protected override void OnStart(string[] args)
        {
            base.OnStart(args);
            Logging.Log.Instance.Trace("Service->OnStart");

            svc = new Service(GetServiceName());

            svc.Start(ConfigPath);
            
            var thSt = new System.Threading.ThreadStart(WorkerThreadFunc);
            this.th = new System.Threading.Thread(thSt);
            this.th.Start();
        }
        protected String GetServiceName()
        {
            // Calling System.ServiceProcess.ServiceBase::ServiceNamea allways returns
            // an empty string,
            // see https://connect.microsoft.com/VisualStudio/feedback/ViewFeedback.aspx?FeedbackID=387024

            // So we have to do some more work to find out our service name, this only works if
            // the process contains a single service, if there are more than one services hosted
            // in the process you will have to do something else

            int processId = System.Diagnostics.Process.GetCurrentProcess().Id;
            String query = "SELECT * FROM Win32_Service where ProcessId = " + processId;
            System.Management.ManagementObjectSearcher searcher = new System.Management.ManagementObjectSearcher(query);

            foreach (System.Management.ManagementObject queryObj in searcher.Get())
            {
                Logging.Log.Instance.Info("Get the ServiceName {0}", queryObj["Name"].ToString());
                return queryObj["Name"].ToString();
            }

            Logging.Log.Instance.Error("Can not get the ServiceName");
            

            return string.Format("AxETWTracing");
        }
        protected static String GetServiceNameFromExe()
        {

            string exePath = System.Reflection.Assembly.GetEntryAssembly().Location.Replace("\\","\\\\");
            String query = "SELECT * FROM Win32_Service WHERE PathName LIKE '%" + exePath + "%'";
            System.Management.ManagementObjectSearcher searcher = new System.Management.ManagementObjectSearcher(query);

            foreach (System.Management.ManagementObject queryObj in searcher.Get())
            {
                Logging.Log.Instance.Info("Get the ServiceName {0}", queryObj["Name"].ToString());
                return queryObj["Name"].ToString();
            }

            //Logging.Log.Instance.Error("Can not get the ServiceName");

            return string.Empty;
        }

        protected override void OnCustomCommand(int command)
        {
            base.OnCustomCommand(command);

            switch (command)
            {
                case 1:
                    //avvio tracing
                    break;
            }
        }
        private System.Threading.Thread th;
        private void WorkerThreadFunc()
        {
            
            svc.DoWork();
            
            //SQLsessionEXE.DoWork();
            Logging.Log.Instance.Trace("End WorkerThreadFunc");
            //Environment.Exit(117);
        }
        protected override void OnStop()
        {
            Logging.Log.Instance.Debug("Service->Stopping");

            svc.Stop();

            Logging.Log.Instance.Debug("Service->th.Joining");
            if (!th.Join(60000))
            {
                Logging.Log.Instance.Debug("Service->Aborting");
                th.Abort();
            }
            Logging.Log.Instance.Debug("Service->th.Joined");

            //System.Threading.Thread.Sleep(60000);

            base.OnStop();

            Logging.Log.Instance.Debug("Service->OnStop");
        }
        
        static unsafe void Main(string[] args)
        {
            Logging.Log.Instance.Trace("Main START");

            var cmdLine = new AX2012ETWtracing.Configuration.CommandLineParser(args);
            ConfigPath = cmdLine.ConfigPath; //vediamo se funziona.....
            
            Logging.Log.Instance.Trace("Main START {0} {1} {2}", cmdLine.DisplayName, cmdLine.tracing, cmdLine.ConfigPath);
            if (cmdLine.isInstall && cmdLine.isUninstall)
                throw new Exception("bad command line argument");

            if (System.Environment.UserInteractive || cmdLine.isUserlevel)
            {
                AppDomain.CurrentDomain.ProcessExit += CurrentDomain_ProcessExit;
                Console.CancelKeyPress += new ConsoleCancelEventHandler(ConsoleCanceled);
                if (cmdLine.tracing)
                {
                    Logging.Log.Instance.Trace("Main START TracingTool");
                    //qui parte tracing tool....
                    var eventRawExporter = new TracingToolReader(cmdLine.providerName, "TT", cmdLine.keywords, cmdLine.etlOutputPath, cmdLine);
                    //eventRawExporter.users = cmdLine.users;
                    //eventRawExporter.instances = cmdLine.instances;
                    //eventRawExporter.sessions = cmdLine.sessions;
                    //eventRawExporter.logBeginMessage = cmdLine.logBeginMessage;
                    //eventRawExporter.logEndMessage = cmdLine.logEndMessage;

                    var EtwListenerProcess = new EtwListenerProcess(eventRawExporter);//come mai ne fa partire uno solo????
                    listeners.Add(EtwListenerProcess);


                    var task = new Task(EtwListenerProcess.DoWork);
                    task.Start();
                    if (cmdLine.hasTimeout)
                    {
                        task.Wait(cmdLine.timeoutSeconds);
                    }
                    else
                    {

                        //while (true)
                        //{
                        //    var key = Console.ReadKey(true);
                        //    if (key.Key == ConsoleKey.Escape) break;
                        //}
                        task.Wait();
                    }
                    //va testato passo passo e anche il servzio
                }
                else if (cmdLine.isInstall)
                {
                    Path.GetFullPath(cmdLine.ConfigPath);
                    //in pbs problema con binpath= che vuole spazio dopo uguale
                    string cmdText = string.Format("sc create {3} binpath= \"{1} -configEXE {2}\" start= auto displayname= \"{0}\"", cmdLine.DisplayName, Assembly.GetExecutingAssembly().Location, cmdLine.ConfigPath, cmdLine.SvcName);

                    if (cmdLine.settedUsername)
                    {
                        cmdText += string.Format(" obj={0} password=\"{1}\"", cmdLine.UserName, cmdLine.Password);
                    }

                    AX2012ETWtracing.Configuration.CommandLineParser.ExecuteCmd(cmdText);
                    EventLogger.InitLog();
                }
                else if (cmdLine.isUninstall)
                {
                    string cmdText = string.Format("sc stop {0}", cmdLine.SvcName);
                    AX2012ETWtracing.Configuration.CommandLineParser.ExecuteCmd(cmdText);
                    cmdText = string.Format("sc delete {0}", cmdLine.SvcName);
                    AX2012ETWtracing.Configuration.CommandLineParser.ExecuteCmd(cmdText);
                    EventLogger.ClearLog();
                }
                else if (cmdLine.test_tornado)
                {
                    Logging.Log.Instance.Trace("Main START Console ETW - TestTornado");
                    var svcname = GetServiceNameFromExe();
                    if(!string.IsNullOrEmpty(svcname))
                    {
                        svc = new Service(svcname);
                        if(svc.StartTornado(ConfigPath))
                            Console.WriteLine("Tornado  messages sent to {1} on topic {0}", AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.tornado.topic, AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.tornado.address);
                    }
                    else
                        Console.WriteLine("Application not installed, no message sent!");
                }
                else
                {
                    Logging.Log.Instance.Trace("Main START Console ETW");
                    svc = new Service();
                    svc.Start(ConfigPath);
                    svc.DoWork();
                    Logging.Log.Instance.Trace("Main END Console ETW");
                }
            }
            else
            {
                ServiceBase.Run(new Program());
            }

            Logging.Log.Instance.Trace("Main END");
        }


        public static List<EtwListenerProcess> listeners = new List<EtwListenerProcess>();

        private static void CurrentDomain_ProcessExit(object sender, EventArgs e)
        {
            Logging.Log.Instance.Debug("CurrentDomain_ProcessExit Start");
            CleanClose();
        }
        private static void ConsoleCanceled(object sender, ConsoleCancelEventArgs e)
        {
            Logging.Log.Instance.Debug("ConsoleCanceled");
            CleanClose();
        }
        private static void CleanClose()
        {
            Logging.Log.Instance.Trace("Service->Stopping");

            //questo assicura che sessione si chiude, testare!!!
            if(svc != null) svc.Stop();
            System.Threading.Thread.Sleep(1000);

            foreach (var listener in listeners)
            {
                listener.Close();
            }
        }
    }
}
