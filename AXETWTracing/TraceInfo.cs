﻿using Microsoft.Diagnostics.Tracing;

namespace AxETWTracing
{
    internal class TraceInfo
    {
        public override string ToString()
        {
            return string.Format("{1}\\{3} -> {0} - {2}", this.ProcessName, this.ComputerName, this.AxVersion, this.UserName);
        }

        public TraceInfo(TraceEvent @event)
        {
            this.processId = @event.ProcessID;

            byte[] payload = @event.EventData();
            int offset = 0;
            //this.ProcessName = @event.PayloadByName("ProcessName") as string;
            this.ProcessName = ReadUnicodeString(payload, ref offset);
            this.ComputerName = ReadUnicodeString(payload, ref offset);
            this.AxVersion = ReadUnicodeString(payload, ref offset);
            this.UserName = ReadUnicodeString(payload, ref offset);
            //this.ComputerName = @event.PayloadByName("ComputerName") as string;
            //this.AxVersion = @event.PayloadByName("AxVersion") as string;
            //this.UserName = @event.PayloadByName("UserName") as string;

        }

        public int processId { get; private set; }
        public string ProcessName { get; private set; }
        public bool isServer
        {
            get
            {
                return ProcessName.Contains("Ax32Serv.exe");
            }
        }
        public string ComputerName { get; private set; }
        public string AxVersion { get; private set; }
        public string UserName { get; private set; }


        unsafe public static string ReadUnicodeString(byte[] buffer, ref int offset)
        {
            fixed (byte* ptr = buffer)
            {
                int bufferLength = buffer.Length;

                char* charStart = (char*)(ptr + offset);
                int maxPos = (bufferLength - offset) / sizeof(char);
                int curPos = 0;

                offset += sizeof(char);

                while (charStart[curPos] != 0)
                {
                    curPos++;
                    if (curPos == maxPos)
                        break;
                    offset += sizeof(char);
                }

                return new string(charStart, 0, curPos);
            }
        }

        unsafe public static int ReadInt32(byte[] buffer, int offset)
        {
            fixed (byte* ptr = buffer)
            {
                return *((int*)(ptr + offset));
            }
        }
    }
}