﻿using System;
using System.Collections.Generic;
using Microsoft.Diagnostics.Tracing;
using Microsoft.Diagnostics.Tracing.Session;
using AxETWTracing.Events;
using System.Threading;
using AxETWTracing.Statistics.CLIENT;

namespace AxETWTracing
{
    internal abstract class EventReader
    {
        private string providerName;
        private string taskId;
        protected TraceEventSession session;
        protected ulong keywords = 0x0;

        public EventReader(string providerName, string taskId)
        {
            this.providerName = providerName;
            this.taskId = taskId;
        }

        internal int Run()
        {
            session = new ETW.TraceEventSessionBuilder(providerName, taskId).session;
            if (session == null)
            {
                return -1;
            }
            else
            {
                Logging.Log.Instance.Trace("The session {0} is going to be started.", session.SessionName);

                //i due successivi sono alterbativi.... primo tool, secondo servizio
                SetEventManager();

                var restarted = session.EnableProvider(providerName, TraceEventLevel.Verbose, keywords);

                if (restarted) Logging.Log.Instance.Trace("The session {0} was already active, it has been restarted.", session.SessionName);
                else
                    Logging.Log.Instance.Trace("The session {0} has been started.", session.SessionName);

                return 0;
            }
        }

        internal enum EndStatus
        {
            Unknown,
            AutoKill
        }
        protected EndStatus endStatus = EndStatus.Unknown;
        internal EndStatus Process()
        {
            Logging.Log.Instance.Trace("Proccess starting");
            bool res = ProcessSession();

            Logging.Log.Instance.Trace("Proccess ending {0}", res);
            if(!res)
                System.Threading.Thread.CurrentThread.Abort();
            //System.Threading.Thread.CurrentThread.Join(10000);
            Logging.Log.Instance.Trace("Proccess ending {0}", res);
            return endStatus;
        }

        internal void Dispose()
        {
            RemoveEventManager();

            System.Threading.Thread.Sleep(1000);
            this.session.DisableProvider(this.providerName);
            //System.Threading.Thread.Sleep(1000);

            DisposeLogger();

            this.session.Stop();
            this.session.Dispose();

        }


        internal abstract void SetEventManager();
        internal abstract void RemoveEventManager();
        internal abstract bool ProcessSession();
        internal abstract void DisposeLogger();
    }

    internal class TracingToolReader
    {
        private string providerName;
        private string taskId;
        //private int serverProcessId = 0;
        public int eventsCollected = 0;
        public int eventsWritten = 0;
        internal ulong keywords;
        internal bool consoleOut = false;
        internal List<string> users;
        internal List<string> instances;
        internal List<int> sessions;
        private int serverProcessId = 0;
        private int BufferSizeMB = 512;

        //questo va tirato fuori solo se serve outfile
        private TraceEventSession session;
        private ETWReloggerTraceEventSource relogger;
        private string outputPath;
        private bool hasOutputPath { get { return !string.IsNullOrEmpty(outputPath); } }

        public TracingToolReader(string providerName, string taskId, ulong keywords, string outputPath, AX2012ETWtracing.Configuration.CommandLineParser cmdline)
        {
            this.providerName = providerName;
            this.taskId = "TT";
            this.outputPath = outputPath;
            this.keywords = keywords;
            this.BufferSizeMB = cmdline.BufferSizeMB;

            this.consoleOut = cmdline.consoleOut;

            this.instances = cmdline.instances;
            foreach (var instance in this.instances)
            {
                Logging.Log.Instance.Debug("filter instance {0}", instance);
            }

            this.users = cmdline.users;
            foreach (var user in this.users)
            {
                Logging.Log.Instance.Debug("filter user {0}", user);
            }
            
            this.sessions = cmdline.sessions;
            foreach (var session in this.sessions)
            {
                Logging.Log.Instance.Debug("filter session {0}", session);
            }



        }

        internal void SetEventManager()
        {
            Logging.Log.Instance.Debug("SetEventManager.TT {0} {1}", this.session.SessionName, this.outputPath);

            this.session.BufferSizeMB = this.BufferSizeMB;
            //this.session.Mi = 512;
            //this.session.BufferSizeMB = 512;
            relogger = new ETWReloggerTraceEventSource(this.session.SessionName, TraceEventSourceType.Session, this.outputPath);
            relogger.Dynamic.All += allEvents;
        }
        internal void RemoveEventManager()
        {
            relogger.Dynamic.All -= allEvents;
        }
        internal bool ProcessSession()
        {
            return relogger.Process();
        }
        internal void DisposeLogger()
        {

            //this.relogger.Dispose();
            if (hasOutputPath)
                this.relogger.Dispose();
        }

        private void allEvents(TraceEvent @event)
        {
            if (this.consoleOut)
                Logging.Log.Instance.Trace(@event);

            //relogger.WriteEvent(@event);
            //eventsWritten++;

            if (@event.ushort_ID == 70)
            {
                var traceInfo = new TraceInfo(@event);
                if (this.instances.Count == 0 || this.instances.Contains(traceInfo.ProcessName))
                    this.serverProcessId = traceInfo.processId;

                Logging.Log.Instance.Debug(traceInfo.ToString());

                //if (this.consoleOut) Logging.Log.Instance.Debug(@event);
                relogger.WriteEvent(@event);
                eventsWritten++;
            }
            else if (@event.ushort_ID == 95 || @event.ushort_ID == 96 || @event.ushort_ID == 97)
            {
                //var clientEvent = new ClientEvent(@event);
                //Logging.Log.Instance.Debug(clientEvent.ToString());

                if (this.consoleOut) Logging.Log.Instance.Debug(@event);
                //relogger.WriteEvent(@event);
                //eventsWritten++;
            }
            else
            {
                bool hasSession = true, hasUser = true;
                if (this.sessions.Count > 0)
                {
                    var AxSessionId = @event.PayloadByName("AxSessionId") as int?;
                    hasSession = this.sessions.Contains(AxSessionId.Value);
                }
                if (this.users.Count > 0)
                {
                    var AxUserName = @event.PayloadByName("AxUserName") as string;
                    hasUser = this.users.Contains(AxUserName.ToLowerInvariant());
                }
                
                if (hasSession && hasUser)
                {
                    if (this.consoleOut) Logging.Log.Instance.Trace(@event);
                    relogger.WriteEvent(@event);
                    eventsWritten++;
                }
            }

            if (!this.consoleOut && eventsCollected % 1000 == 0)
            {
                Logging.Log.Instance.Debug("events written {0} of {1}", eventsWritten, eventsCollected + 1);
            }
            eventsCollected++;
        }

        internal int Run()
        {
            session = new ETW.TraceEventSessionBuilder(providerName, taskId).session;
            if (session == null)
            {
                return -1;
            }
            else
            {
                session.BufferSizeMB = 512;
                Logging.Log.Instance.Trace("The session {0} is going to be started.", session.SessionName);

                //i due successivi sono alterbativi.... primo tool, secondo servizio
                //SetEventManager();
                Logging.Log.Instance.Debug("SetEventManager.TT {0} {1}", this.session.SessionName, this.outputPath);

                relogger = new ETWReloggerTraceEventSource(this.session.SessionName, TraceEventSourceType.Session, this.outputPath);
                relogger.Dynamic.All += allEvents;

                var restarted = session.EnableProvider(providerName, TraceEventLevel.Verbose, keywords);

                if (restarted) Logging.Log.Instance.Trace("The session {0} was already active, it has been restarted.", session.SessionName);
                else
                    Logging.Log.Instance.Trace("The session {0} has been started.", session.SessionName);

                return 0;
            }
        }

        internal void Process()
        {
            Logging.Log.Instance.Debug("Proccess starting");

            //bool res = ProcessSession();
            bool res = relogger.Process();


            Logging.Log.Instance.Debug("Proccess ending {0}", res);
            if(!res)
                System.Threading.Thread.CurrentThread.Abort();
        }

        internal void DisposeOLD()
        {
            Logging.Log.Instance.Debug("DISPOSE AAA 1");
            //RemoveEventManager();
            relogger.Dynamic.All -= allEvents;

            Logging.Log.Instance.Debug("DISPOSE AAA 2");
            System.Threading.Thread.Sleep(1000);
            this.session.DisableProvider(this.providerName);
            //System.Threading.Thread.Sleep(1000);

            Logging.Log.Instance.Debug("DISPOSE AAA 3");
            //DisposeLogger();
            this.relogger.Dispose();

            Logging.Log.Instance.Debug("DISPOSE AAA 4");
            //this.session.Stop(); --> commentato nell'originale
            this.session.Dispose();

            Logging.Log.Instance.Debug("DISPOSE AAA 5");
        }
        internal void Dispose()
        {
            Logging.Log.Instance.Debug("this.session.DisableProvider(this.providerName)...");
            this.session.DisableProvider(this.providerName);
            Logging.Log.Instance.Debug("DONE");
            Logging.Log.Instance.Debug("this.session.Dispose()...");
            this.session.Dispose();
            Logging.Log.Instance.Debug("DONE");

            System.Threading.Thread.Sleep(1000);

            Logging.Log.Instance.Debug("relogger.Dynamic.All -= allEvents...");
            relogger.Dynamic.All -= allEvents;
            Logging.Log.Instance.Debug("DONE");
            Logging.Log.Instance.Debug("this.session.Dispose()...");
            this.relogger.Dispose();
            Logging.Log.Instance.Debug("DONE");
        }
    }

    internal class TracingToolReaderOLD : EventReader
    {
        //questo è troppo specializzato
        internal class LogSequence
        {
            string beginMessage;
            ushort beginMessageType;
            string endMessage;
            ushort endMessageType;
        }
        private List<LogSequence> sequences = new List<LogSequence>();

        //private int serverProcessId = 0;
        public int eventsCollected = 0;
        public int eventsWritten = 0;
        //internal ulong keywords;
        internal bool consoleOut = false;

        //questo va tirato fuori solo se serve outfile
        private ETWReloggerTraceEventSource relogger;
        private string outputPath;
        private bool hasOutputPath { get { return !string.IsNullOrEmpty(outputPath); } }

        public TracingToolReaderOLD(string providerName, string taskId, ulong keywords, string outputPath, bool consoleOut) : base(providerName, "TT")
        {
            this.outputPath = outputPath;
            this.keywords = keywords;
            this.consoleOut = consoleOut;
        }

        internal override void SetEventManager()
        {
            Logging.Log.Instance.Debug("SetEventManager.TT {0} {1}", this.session.SessionName, this.outputPath);

            relogger = new ETWReloggerTraceEventSource(this.session.SessionName, TraceEventSourceType.Session, this.outputPath);
            relogger.Dynamic.All += allEvents;
        }
        internal override void RemoveEventManager()
        {
            relogger.Dynamic.All -= allEvents;
        }
        internal override bool ProcessSession()
        {
            return relogger.Process();
        }
        internal override void DisposeLogger()
        {

            //this.relogger.Dispose();
            if (hasOutputPath)
                this.relogger.Dispose();
        }

        private void allEvents(TraceEvent @event)
        {
            if (this.consoleOut)
                Logging.Log.Instance.Trace(@event);

            relogger.WriteEvent(@event);
            eventsWritten++;
            

            if (!this.consoleOut && eventsCollected % 1000 == 0)
            {
                Logging.Log.Instance.Debug("events written {0} of {1}", eventsWritten, eventsCollected + 1);
            }
            eventsCollected++;
        }
    }

    internal class Ax2012etwReader : EventReader
    {

        public Ax2012etwReader(DataSend.INATSSend dataSend, string providerName, string taskId, ulong keywords) : base(providerName, "ST")
        {
            this.dataSend = dataSend;
            this.keywords = keywords;

            AosInstances.instance.Init(AX2012ETWtracing.Configuration.Configuration.config.ax2012.validInstances);

            publishThread = new Thread(new ThreadStart(statisticPublisher));
            publishThread.Start();
        }

        internal override void SetEventManager()
        {
            Logging.Log.Instance.Debug("SetEventManager.ST {0}", this.session.SessionName);
            session.Source.Dynamic.All += allEvents;
        }
        internal override void RemoveEventManager()
        {
            autoResetEvent.Set();

            session.Source.Dynamic.All -= allEvents;
        }
        internal override bool ProcessSession()
        {
            return session.Source.Process();
        }
        internal override void DisposeLogger()
        {
        }

        /********************/
        
        private DataSend.INATSSend dataSend;
        private Thread publishThread = null;

        private void statisticPublisher()
        {
            int noEvents = 0;
            ulong checkEventCount = 0UL;
            while (!autoResetEvent.WaitOne(dueTime))
            {
                //if ((this.keywords & 0x2000000000000000UL) > 0)
                {
                    DateTime nowT = DateTime.UtcNow;
                    DateTime cycleWriteDT_EVENTS = nowT.AddMilliseconds(publishCycleTime / 2);//x evitare stranezze
                    DateTime cycleWriteDT = cycleWriteDT_EVENTS.AddMilliseconds(-publishCycleTime);

                    long cycleWriteTS_EVENTS = new EpochNanoseconds(cycleWriteDT_EVENTS).Value;
                    long cycleWriteTS = new EpochNanoseconds(cycleWriteDT).Value;//sembrerebbe che la thread calcoli piú volte questo valore 

                    Logging.Log.Instance.Debug("PUBLISHING: {0:HH:mm:ss.fffffff}||{1:HH:mm:ss.fffffff}||{2:HH:mm:ss.fffffff} - {3}||{4}", nowT, cycleWriteDT_EVENTS, cycleWriteDT, cycleWriteTS_EVENTS, cycleWriteTS);
                    Logging.Log.Instance.Debug("\n{2:HH:mm:ss.ffffff} // total: {0} losts: {1}", totalEventsCount, lostEventsCount, cycleWriteDT);

                    /**********************************************************************/
                    

                    dataSend.PublishHeartbeat(new DataSend.Heartbeat()
                    {
                        servers = AosInstances.instance.Count,

                        RPC = AosInstances.instance.RPC_stmt_count_cumu,
                        SQL = AosInstances.instance.SQL_stmt_count_cumu,
                        MDL = AosInstances.instance.MDL_stmt_count_cumu,
                        FTC = AosInstances.instance.FTC_stmt_count_cumu,

                        clients = Statistics.CLIENT.SessionsMatrix.popClientEventStatistics(cycleWriteTS),
                        CLI = Statistics.CLIENT.SessionsMatrix.CLI_events_count_cumu,

                        Ntotal = totalEventsCount,
                        Nlost = lostEventsCount,

                        AxServer = Environment.MachineName
                    }, cycleWriteTS);

                    //qui mettiamo un keep-alive
                    if (checkEventCount == totalEventsCount && AX2012ETWtracing.Configuration.Configuration.config.agent.zeroEventsTimeout > 0)
                    {
                        Logging.Log.Instance.Debug("autokill??? {0} - {1}", noEvents, AX2012ETWtracing.Configuration.Configuration.config.agent.zeroEventsTimeout);


                        //parte non testata!!!!
                        noEvents++;
                        if (noEvents > AX2012ETWtracing.Configuration.Configuration.config.agent.zeroEventsTimeout)
                        {
                            Logging.Log.Instance.Debug("autokill!!!!!!!!!!!!!!!!!!!!! {0} - {1}", noEvents, AX2012ETWtracing.Configuration.Configuration.config.agent.zeroEventsTimeout);
                            //error 5 mintu senza dati
                            //throw new Exception("No data from provider since 5 minutes");

                            autoResetEvent.Dispose();
                            autoResetEvent = null;
                            Dispose();
                            ((DataSend.InfluxDataSend)this.dataSend).Dispose();
                            endStatus = EndStatus.AutoKill;
                            break;
                            //potremmo fare dispose, giusto per provare
                        }
                    }
                    else
                    {
                        noEvents = 0;
                    }
                    checkEventCount = totalEventsCount;
                }

                AxClientStatistics.instance.checkClients();
            }
            
            Logging.Log.Instance.Debug("Ax2012etwReader->statisticPublisher exited");
        }


        private System.Threading.AutoResetEvent autoResetEvent = new AutoResetEvent(false);
        private int dueTime
        {
            get

            {
                var currentElapsed = Convert.ToInt32(DateTime.Now.TimeOfDay.TotalMilliseconds) % publishCycleTime;
                if (currentElapsed > publishCycleTime / 2)
                    return 2 * publishCycleTime - currentElapsed;
                else
                    return publishCycleTime - currentElapsed;
            }
        }
        private int publishCycleTime = AX2012ETWtracing.Configuration.Configuration.config.ax2012.statistic.aggregationInterval;

        private int lostEventsCount;
        private ulong totalEventsCount;

        private void allEventsTEST(TraceEvent @event)
        {
            //if (this.consoleOut)
            try
            {
                string s = @event.ToString();
            }
            catch (Exception e)
            {
                Logging.Log.Instance.Debug("EXCCC: " + e.Message);
            }

            if (totalEventsCount % 1000 == 0)
            {
                Logging.Log.Instance.Debug("events written {0} of {1}", 0, totalEventsCount + 1);
            }
            totalEventsCount++;
        }

        System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
        private void allEvents(TraceEvent obj)
        {
            //var bytes = obj.ToBinary(null, 0);
            //if (Convert.ToUInt16(obj.ID) == 59)
            //Logging.Log.Instance.Debug("EVENT -> " + obj.ID + " " + obj.ProcessID);

            //Console.ReadKey(true);
            //WriteBinaryAsync(bytes);

            //TraceInformation: inviato da ogni applicativo attivo
            totalEventsCount++;

            Tornado.TornadoEnpointData.values.number_of_events_read++;
            
            if (TimeSynchro.Check(obj.nsTimeStampEpoch))
            {
                var old_EventEpoch = obj.nsTimeStampEpoch;
                stopwatch.Restart();
                session.Source.SynchronizeClock();
                stopwatch.Stop();
                var new_EventEpoch = obj.nsTimeStampEpoch;
                long deltaTimeEpoch = new_EventEpoch - old_EventEpoch;
                Console.WriteLine("TIMESTAMP CHANGE {0:mm:ss.ffffff} --> {1:mm:ss.ffffff}", new EpochNanoseconds(obj.nsTimeStampEpoch).dateTime, new EpochNanoseconds(new_EventEpoch).dateTime);
                TimeSynchro.Done(deltaTimeEpoch, stopwatch.ElapsedTicks);

                if (AosInstances.instance.Count > 0)
                {
                    foreach(var server in AosInstances.instance.All)
                        server.synchroSession(new_EventEpoch, deltaTimeEpoch);
                }
            }

            //poi ci pensiamo se cancellare...
            //Statistics.SessionsMatrix.instance.ClearDefuncts();

            bool isValidEvent = false;
            switch (obj.ushort_ID)
            {

                /*ClientAccess*/
                case 95:
                case 96:
                case 97:
                    {
                        var clientEvent = new ClientEvent(obj);
                        Statistics.CLIENT.SessionsMatrix.pushClientEvent(clientEvent, obj.nsTimeStampEpoch, obj.ProcessID);
                        isValidEvent = true;

                        Tornado.TornadoEnpointData.values.number_of_events_processed++;
                    }
                    break;

                /*Caching*/
                case 12:
                    {
                        var tableLoading = new TableLoading(obj);
                        Statistics.CACHING.SessionsMatrix.pushEvent(tableLoading);
                        isValidEvent = true;

                        Tornado.TornadoEnpointData.values.number_of_events_processed++;
                    }
                    break;
                case 13:
                    {
                        var classLoading = new ClassLoading(obj);
                        Statistics.CACHING.SessionsMatrix.pushEvent(classLoading);
                        isValidEvent = true;

                        Tornado.TornadoEnpointData.values.number_of_events_processed++;
                    }
                    break;
                case 33:
                    {
                        var dataAccessCCaching = new DataAccessCCaching(obj);
                        Statistics.CACHING.SessionsMatrix.pushEvent(dataAccessCCaching);
                        isValidEvent = true;

                        Tornado.TornadoEnpointData.values.number_of_events_processed++;
                    }
                    break;
                case 34:
                    {
                        var dataAccessSCSCCaching = new DataAccessSCSCCaching(obj);
                        Statistics.CACHING.SessionsMatrix.pushEvent(dataAccessSCSCCaching);
                        isValidEvent = true;

                        Tornado.TornadoEnpointData.values.number_of_events_processed++;
                    }
                    break;
                case 98:
                    {
                        var assemblyCacheElementBegin = new AssemblyCacheElementBegin(obj);
                        Statistics.CACHING.SessionsMatrix.pushEvent(assemblyCacheElementBegin);
                        isValidEvent = true;

                        Tornado.TornadoEnpointData.values.number_of_events_processed++;
                    }
                    break;
                case 99:
                    {
                        var assemblyCacheElement = new AssemblyCacheElement(obj);
                        Statistics.CACHING.SessionsMatrix.pushEvent(assemblyCacheElement);
                        isValidEvent = true;

                        Tornado.TornadoEnpointData.values.number_of_events_processed++;
                    }
                    break;
                case 100:
                    {
                        var assemblyCacheElementEnd = new AssemblyCacheElementEnd(obj);
                        Statistics.CACHING.SessionsMatrix.pushEvent(assemblyCacheElementEnd);
                        isValidEvent = true;

                        Tornado.TornadoEnpointData.values.number_of_events_processed++;
                    }
                    break;


                //attivi solo con XppMarker 0x0000000000000001
                case 51:
                    {
                        //"marchio" la sessione con questo evento, occhio che se non me la chiudono poi io la tento aperta fino a che la stessa id non viene riusata
                        //la marchio come tag in measuremnt ad eventi e come field nella ETW cosi filtro funziona!!!

                        Logging.Log.Instance.Debug("XPPMARKER EVENT 51 for {0}", obj.ProcessID);

                        Tornado.TornadoEnpointData.values.number_of_events_processed++;
                    }
                    break;
                case 52:
                    {
                        Logging.Log.Instance.Debug("XPPMARKER EVENT 52 for {0}", obj.ProcessID);

                        Tornado.TornadoEnpointData.values.number_of_events_processed++;
                    }
                    break;


                /*Traceinfo 0x0000000000000002*/
                case 53:
                    {
                        Logging.Log.Instance.Debug("TRACEINFO EVENT 53 for {0}", obj.ProcessID);

                        Tornado.TornadoEnpointData.values.number_of_events_processed++;
                    }
                    break;
                case 68:
                    {
                        Logging.Log.Instance.Debug("TRACEINFO EVENT 68 for {0}", obj.ProcessID);

                        Tornado.TornadoEnpointData.values.number_of_events_processed++;
                    }
                    break;
            }



            if (obj.ushort_ID == 70)
            {
                var traceInformation = new Events.TraceInformation(obj);
                Logging.Log.Instance.Info("ProcessName  {0} {1}", traceInformation.ProcessName, traceInformation.ProcessId);
                if (traceInformation.isAxServer && AosInstances.instance.isValid(traceInformation.instanceNumber))
                {
                    AosInstances.instance.setInstance(traceInformation.ProcessId, traceInformation.instance);
                    Logging.Log.Instance.Info("AxServer:{0}\nHex ProcessId:{0:x}\nApplicationName:{1}\nAxUserName:{2}", traceInformation.ProcessId, traceInformation.ProcessName, traceInformation.UserName);
                }
                else /*if (traceInformation.isAxClient)*/
                {
                    Logging.Log.Instance.Info("ProcessId:{0}\nHex ProcessId:{0:x}\nApplicationName:{1}\nAxUserName:{2}", traceInformation.ProcessId, traceInformation.ProcessName, traceInformation.UserName);
                    //

                }
                //else
                //{
                //    Logging.Log.Instance.Debug("Process {1} Not Valid -> {0}", traceInformation.ProcessName, traceInformation.ProcessId);
                //}

                Tornado.TornadoEnpointData.values.number_of_events_processed++;
            }
            else if (AosInstances.instance.verifyAosInstance(obj.ProcessID))
            {
                try
                {
                    switch (obj.ushort_ID)
                    {
                        //RPC
                        case 54:
                            {
                                //CSRoundTripBegin
                                var csroundtripBegin = new CSRoundTripBegin(obj);
                                AosInstances.instance.getServer(csroundtripBegin.processId).addEvent(csroundtripBegin);
                                //AosInstances.instance.RPC_stmt_count_cumu++;
                                //Logging.Log.Instance.Debug("csroundtripBegin {0} {1}", csroundtripBegin.AxSessionId, csroundtripBegin.AxNestLevel);

                                Tornado.TornadoEnpointData.values.number_of_events_processed++;
                            }
                            break;
                        case 55:
                            {
                                //CSRoundTripEnd
                                var csroundtripEnd = new CSRoundTripEnd(obj);
                                AosInstances.instance.getServer(csroundtripEnd.processId).addEvent(csroundtripEnd);
                                AosInstances.instance.RPC_stmt_count_cumu++;
                                //Logging.Log.Instance.Debug("csroundtripEnd {0} {1} ({2}<>{3})", csroundtripEnd.AxSessionId, csroundtripEnd.AxNestLevel, csroundtripEnd.AxServerBytesRecd, csroundtripEnd.AxServerBytesSent);

                                Tornado.TornadoEnpointData.values.number_of_events_processed++;
                            }
                            break;

                        //XPP
                        case 56:
                            {
                                //XppMethodBegin
                                var xppmethodBegin = new XppMethodBegin(obj);
                                AosInstances.instance.getServer(xppmethodBegin.processId).addEvent(xppmethodBegin);

                                Tornado.TornadoEnpointData.values.number_of_events_processed++;
                            }
                            break;
                        case 57:
                            {
                                //XppMethodEnd
                                var xppmethodEnd = new XppMethodEnd(obj);
                                AosInstances.instance.getServer(xppmethodEnd.processId).addEvent(xppmethodEnd);

                                Tornado.TornadoEnpointData.values.number_of_events_processed++;
                            }
                            break;

                        //SQL
                        //ModelSqlInputBind  94  BindParameter --> non é gestito
                        case 65:
                            {
                                ////DEVEL#SQLINPUTBIND
                                ////per ora tolgo tutto, non mettere in keywords!!!!

                                ////SqlInputBind
                                var sqlInputBind = new SqlInputBind(obj);
                                Statistics.SQL.SqlParamsBinding.instance.isActive = true;
                                Statistics.SQL.SqlParamsBinding.instance.AddBindParameter(sqlInputBind);

                                Tornado.TornadoEnpointData.values.number_of_events_processed++;
                            }
                            break;
                        case 94:
                            {
                                ////DEVEL#SQLINPUTBIND
                                ////per ora tolgo tutto, non mettere in keywords!!!!

                                ////SqlInputBind
                                //var sqlInputBind = new SqlInputBind(obj);
                                //Statistics.SQL.SqlParamsBinding.instance.AddBindParameter(sqlInputBind);

                                Tornado.TornadoEnpointData.values.number_of_events_processed++;
                            }
                            break;
                        case 59:
                            {
                                //AxSqlStmtEvent
                                var axSqlStmtEvent = new AxSqlStmtEvent(obj);
                                AosInstances.instance.getServer(axSqlStmtEvent.processId).addEvent(axSqlStmtEvent);
                                AosInstances.instance.SQL_stmt_count_cumu++;

                                //DEVEL#SQLINPUTBIND
                                //per ora tolgo tutto, non mettere in keywords!!!!

                                //////tolgo questa parte perché la logica va validata, peró é chiaro come funziona!!!

                                if (Statistics.SQL.SqlParamsBinding.instance.isActive)
                                {


                                    var parser = new SqlStmtParser();
                                    var stmt = parser.ParseBatch(axSqlStmtEvent.AxSqlStmt);


                                    switch (stmt.stmtType)
                                    {
                                        case SqlStmtParser.StmtType.select:
                                            if (axSqlStmtEvent.AxSqlStmt.Contains("SELECT T1.DEBUGGERPOPUP,T1.PREFERREDTIMEZONE,T1.PREFERREDCALENDAR,T1.ID,T1.PARTITION,")
                                                && axSqlStmtEvent.AxSqlStmt.Contains(" FROM USERINFO T1 WHERE ((PARTITION=")
                                                && axSqlStmtEvent.AxSqlStmt.Contains(") AND (SID=?))"))
                                            {
                                                //considerare anche questa, forse è subito prima o viene fatta da tutte le sessioni
                                                //SELECT T1.DEBUGGERPOPUP,T1.PREFERREDTIMEZONE,T1.PREFERREDCALENDAR,T1.ID,T1.PARTITION,101090 FROM USERINFO T1 WHERE ((PARTITION=5637144576) AND (SID=?))
                                                //
                                                //SELECT T1.SESSIONID,T1.SERVERID,T1.VERSION,T1.LOGINDATETIME,T1.LOGINDATETIMETZID,T1.STATUS,T1.USERID,T1.SID,T1.USERLANGUAGE,T1.HELPLANGUAGE,T1.CLIENTTYPE,T1.SESSIONTYPE,T1.CLIENTCOMPUTER,T1.DATAPARTITION,T1.RECVERSION,T1.RECID FROM SYSCLIENTSESSIONS T1 WHERE (SESSIONID=@P1)
                                                //ricavo axsession

                                                var data = Statistics.SQL.CreateUserSessionBinding.instance.GetCreateUserSessionEvent(axSqlStmtEvent.processId);
                                                Statistics.AxSessionId axSessionId = new Statistics.AxSessionId() { Value = 1 };

                                                //qui posso cercare sessione appropriata e mettere start_time
                                                //il valore da salvare nei fields è poi calcolato come nanosecondi a partire dal start_time e poi chiudo
                                                if (!data.isBatchType)
                                                {
                                                    Logging.Log.Instance.Debug("adding SessionData {1} :: {0} // {2}", axSqlStmtEvent.AxSessionId.Value, data.axUserName.ASCII, data.nsTimeStampEpoch);
                                                    AosInstances.instance.getServer(axSqlStmtEvent.processId).addSessionData(axSqlStmtEvent.AxSessionId, data);
                                                }
                                            }
                                            if (axSqlStmtEvent.AxSessionId.Value == 2 && axSqlStmtEvent.AxSqlStmt.Contains("SELECT T1.SESSIONID,T1.SERVERID,T1.VERSION,T1.LOGINDATETIME,T1.LOGINDATETIMETZID,T1.STATUS,T1.USERID,T1.SID,T1.USERLANGUAGE,T1.HELPLANGUAGE,T1.CLIENTTYPE,T1.SESSIONTYPE,T1.CLIENTCOMPUTER,T1.DATAPARTITION,T1.RECVERSION,T1.RECID FROM SYSCLIENTSESSIONS T1 WHERE (SESSIONID=@P1)"))
                                            {
                                                //considerare anche questa, forse è subito prima o viene fatta da tutte le sessioni
                                                //SELECT T1.DEBUGGERPOPUP,T1.PREFERREDTIMEZONE,T1.PREFERREDCALENDAR,T1.ID,T1.PARTITION,101090 FROM USERINFO T1 WHERE ((PARTITION=5637144576) AND (SID=?))
                                                //
                                                //SELECT T1.SESSIONID,T1.SERVERID,T1.VERSION,T1.LOGINDATETIME,T1.LOGINDATETIMETZID,T1.STATUS,T1.USERID,T1.SID,T1.USERLANGUAGE,T1.HELPLANGUAGE,T1.CLIENTTYPE,T1.SESSIONTYPE,T1.CLIENTCOMPUTER,T1.DATAPARTITION,T1.RECVERSION,T1.RECID FROM SYSCLIENTSESSIONS T1 WHERE (SESSIONID=@P1)
                                                //ricavo axsession

                                                var data = Statistics.SQL.CreateUserSessionBinding.instance.GetCreateUserSessionEvent(axSqlStmtEvent.processId);
                                                Statistics.AxSessionId axSessionId = new Statistics.AxSessionId() { Value = 1 };

                                                Logging.Log.Instance.Debug("ADSSESSIONDATA OPTION 2");

                                                //qui posso cercare sessione appropriata e mettere start_time
                                                //il valore da salvare nei fields è poi calcolato come nanosecondi a partire dal start_time e poi chiudo
                                                //AosInstances.instance.getServer(axSqlStmtEvent.processId).addSessionData(axSessionId, data);
                                            }
                                            break;

                                        case SqlStmtParser.StmtType.call:
                                            switch (stmt.table)
                                            {
                                                case "CREATEUSERSESSIONS":
                                                    {
                                                        var sqlParameters = Statistics.SQL.SqlParamsBinding.instance.GetBindParameters(axSqlStmtEvent.processId, axSqlStmtEvent.AxSessionId, axSqlStmtEvent.AxUserName, axSqlStmtEvent.AxConDBSpid);
                                                        //qui posso estrarre 18 parametri, metto il tutto sull'istanza dei parametri e se arriva la select giusta riesco a ricavare anche la session id corretta
                                                        if (sqlParameters.Count == 18)
                                                        {
                                                            CreateUserSessionEvent data = new CreateUserSessionEvent();
                                                            //0,1,4,7,13(?),14(?)
                                                            switch (sqlParameters[0].AxBindVarValue)
                                                            {
                                                                case "0":
                                                                    data.clientType = "USER";
                                                                    break;
                                                                case "1":
                                                                    data.clientType = "BC";
                                                                    break;
                                                                case "3":
                                                                    data.clientType = "WORKER";
                                                                    break;
                                                                case "5":
                                                                    data.clientType = "WEB";
                                                                    break;
                                                                default:
                                                                    data.clientType = sqlParameters[0].AxBindVarValue;
                                                                    break;
                                                            }

                                                            data.sessionType = sqlParameters[1].AxBindVarValue;//aggiunta session type come sottotipo

                                                            data.axUserName = new Statistics.AxUserName { Value = sqlParameters[4].AxBindVarValue };
                                                            data.computerName = sqlParameters[7].AxBindVarValue; //preziosissimo...
                                                            data.nsTimeStampEpoch = axSqlStmtEvent.nsTimeStampEpoch;
                                                            Logging.Log.Instance.Debug("SetCreateUserSessionEvent {0} {1} {2} -- {3}", data.clientType, data.computerName, data.axUserName.ASCII, data.nsTimeStampEpoch);
                                                            Statistics.SQL.CreateUserSessionBinding.instance.SetCreateUserSessionEvent(data, axSqlStmtEvent.processId);
                                                        }
                                                    }
                                                    break;
                                            }
                                            break;

                                        case SqlStmtParser.StmtType.update:
                                            switch (stmt.table)
                                            {
                                                case "BATCH":
                                                    {
                                                        var sqlParameters = Statistics.SQL.SqlParamsBinding.instance.GetBindParameters(axSqlStmtEvent.processId, axSqlStmtEvent.AxSessionId, axSqlStmtEvent.AxUserName, axSqlStmtEvent.AxConDBSpid);
                                                        if (stmt.stmtType == SqlStmtParser.StmtType.update && stmt.columns.Count == sqlParameters.Count)
                                                        {
                                                            var batchEvent = new BatchEvent(stmt, sqlParameters, axSqlStmtEvent.AxSessionId, axSqlStmtEvent.AxUserName, axSqlStmtEvent.nsTimeStampEpoch);
                                                            if (batchEvent.startEvent)
                                                            {
                                                                Logging.Log.Instance.Debug("STARTING BATCH: " + axSqlStmtEvent.AxSqlStmt);
                                                            }
                                                            if (batchEvent.exitEvent)
                                                            {
                                                                Logging.Log.Instance.Debug("CLOSING BATCH: " + axSqlStmtEvent.AxSqlStmt);
                                                            }
                                                            batchEvent.GetMessage(axSqlStmtEvent.processId, axSqlStmtEvent.AxSessionId, axSqlStmtEvent.AxUserName, axSqlStmtEvent.nsTimeStampEpoch);
                                                        }
                                                    }
                                                    break;

                                                case "SYSCLIENTSESSIONS":
                                                    {
                                                        //UPDATE SYSCLIENTSESSIONS SET STATUS=@P1,RECVERSION=@P2 WHERE ((SESSIONID=@P3) AND (RECVERSION=@P4)):
                                                        //ricavo axsession e status (se è inactive 0) posso chiudere la sessione
                                                        //ovviamente posso introdurre checks aggiuntivi (tipo sessione aperta con id vecchio, ma credo che sovrascrivo in automatico)
                                                        //verificare che forse il tutto prevede una continua "chiusura sessioni" a ogni minuto 
                                                        var sqlParameters = Statistics.SQL.SqlParamsBinding.instance.GetBindParameters(axSqlStmtEvent.processId, axSqlStmtEvent.AxSessionId, axSqlStmtEvent.AxUserName, axSqlStmtEvent.AxConDBSpid);
                                                        //qui posso cercare sessione appropriata e mettere start_time
                                                        //il valore da salvare nei fields è poi calcolato come nanosecondi a partire dal start_time e poi chiudo
                                                        if (stmt.columns.Count == 4 && sqlParameters.Count == 4 && stmt.columns[0] == "STATUS" && stmt.columns[2] == "SESSIONID")
                                                        {
                                                            switch (sqlParameters[0].AxBindVarValue)
                                                            {
                                                                case "0":
                                                                    AosInstances.instance.getServer(axSqlStmtEvent.processId).endSession(new Statistics.AxSessionId { Value = int.Parse(sqlParameters[2].AxBindVarValue) }, axSqlStmtEvent.nsTimeStampEpoch);
                                                                    break;
                                                            }
                                                        }
                                                        //dovrei mettere che se ci sono UPDATE diversi debuggo
                                                    }
                                                    break;
                                            }
                                            break;
                                    }

                                    AosInstances.instance.getServer(axSqlStmtEvent.processId).setSQLstmtType(axSqlStmtEvent.AxSessionId, stmt.stmtType);

                                    Statistics.SQL.SqlParamsBinding.instance.ClearBindParameters(axSqlStmtEvent.processId, axSqlStmtEvent.AxSessionId, axSqlStmtEvent.AxUserName, axSqlStmtEvent.AxConDBSpid);
                                }

                                Tornado.TornadoEnpointData.values.number_of_events_processed++;
                            }
                            break;
                        case 74:
                            {
                                //ModelStoredProc
                                var modelStoredProc = new ModelStoredProc(obj);
                                AosInstances.instance.getServer(modelStoredProc.processId).addEvent(modelStoredProc);
                                AosInstances.instance.MDL_stmt_count_cumu++;

                                ////DEVEL#SQLINPUTBIND
                                ////per ora tolgo tutto, non mettere in keywords!!!!

                                //Statistics.SQL.SqlParamsBinding.instance.ClearBindParameters(modelStoredProc.processId, modelStoredProc.AxSessionId, modelStoredProc.AxUserName, modelStoredProc.AxConDBSpid);

                                Tornado.TornadoEnpointData.values.number_of_events_processed++;
                            }
                            break;
                        case 66:
                            {
                                //SqlRowFetch
                                var sqlRowFetch = new SqlRowFetch(obj);
                                AosInstances.instance.getServer(sqlRowFetch.processId).addEvent(sqlRowFetch);
                                AosInstances.instance.FTC_stmt_count_cumu++;

                                Tornado.TornadoEnpointData.values.number_of_events_processed++;
                            }
                            break;
                        case 67:
                            {
                                //SqlRowFetchCumu
                                var sqlRowFetchCumu = new SqlRowFetchCumu(obj);
                                AosInstances.instance.getServer(sqlRowFetchCumu.processId).addEvent(sqlRowFetchCumu);
                                //AosInstances.instance.FTC_stmt_count_cumu++;

                                Tornado.TornadoEnpointData.values.number_of_events_processed++;
                            }
                            break;

                        //attivi solo con XppMarker 0x0000000000000001
                        case 51:
                            {
                                //"marchio" la sessione con questo evento, occhio che se non me la chiudono poi io la tento aperta fino a che la stessa id non viene riusata
                                //la marchio come tag in measuremnt ad eventi e come field nella ETW cosi filtro funziona!!!

                                //dimentica sopra e per ora gestisco ad eventi....
                                var axTransactionBeginEvent = new AxTransactionBeginEvent(obj);
                                Logging.Log.Instance.Debug("AxTransactionBeginEvent {1} {0} --> {2}", axTransactionBeginEvent.AxSessionId.Value, axTransactionBeginEvent.AxUserName.ASCII, axTransactionBeginEvent.AxMarkerName);
                                AosInstances.instance.getServer(axTransactionBeginEvent.processId).pushEvent(axTransactionBeginEvent);

                                Tornado.TornadoEnpointData.values.number_of_events_processed++;
                            }
                            break;
                        case 52:
                            {
                                var axTransactionEndEvent = new AxTransactionEndEvent(obj);
                                Logging.Log.Instance.Debug("AxTransactionEndEvent {1} {0} --> {2}", axTransactionEndEvent.AxSessionId.Value, axTransactionEndEvent.AxUserName.ASCII, axTransactionEndEvent.AxMarkerName);
                                AosInstances.instance.getServer(axTransactionEndEvent.processId).pushEvent(axTransactionEndEvent);

                                Tornado.TornadoEnpointData.values.number_of_events_processed++;
                            }
                            break;


                        /*Traceinfo 0x0000000000000002*/
                        case 53:
                            {
                                var axTraceMessageEvent = new AxTraceMessageEvent(obj);
                                Logging.Log.Instance.Debug("AxTraceMessageEvent {1} {0} --> {2}", axTraceMessageEvent.AxSessionId.Value, axTraceMessageEvent.AxUserName.ASCII, axTraceMessageEvent.AxMsg);
                                AosInstances.instance.getServer(axTraceMessageEvent.processId).pushEvent(axTraceMessageEvent);

                                Tornado.TornadoEnpointData.values.number_of_events_processed++;
                            }
                            break;
                        case 68:
                            {
                                var axTraceComponentMessageEvent = new AxTraceComponentMessageEvent(obj);
                                Logging.Log.Instance.Debug("AxTraceComponentMessageEvent {1} {0} --> {2}", axTraceComponentMessageEvent.AxSessionId.Value, axTraceComponentMessageEvent.AxUserName.ASCII, axTraceComponentMessageEvent.AxMsg);
                                AosInstances.instance.getServer(axTraceComponentMessageEvent.processId).pushEvent(axTraceComponentMessageEvent);

                                Tornado.TornadoEnpointData.values.number_of_events_processed++;
                            }
                            break;

                        default:
                            //Logging.Log.Instance.Debug(obj.ID + " " + obj.ProcessID + " " + isValidProcess(obj.ProcessID));
                            break;
                    }
                }
                catch (Exception e)
                {
                    Logging.Log.Instance.Debug(e, "EXC SERVER EVENT {1} // {0} // {2} {3} {4}", EpochNanoseconds.UtcNow.Value, obj.ushort_ID, e.Message, e.Source, e.StackTrace);
                }
            }
            else if (!isValidEvent)
            {
                switch (obj.ushort_ID)
                {
                    /* clients & altro, poi vedremo se serve rimettere qualche verifica su WMI */

                    //attivi solo con XppMarker 0x0000000000000001
                    case 51:
                        {
                            //"marchio" la sessione con questo evento, occhio che se non me la chiudono poi io la tento aperta fino a che la stessa id non viene riusata
                            //la marchio come tag in measuremnt ad eventi e come field nella ETW cosi filtro funziona!!!

                            //dimentica sopra e per ora gestisco ad eventi....
                            var axTransactionBeginEvent = new AxTransactionBeginEvent(obj);
                            Logging.Log.Instance.Debug("AxTransactionBeginEvent {1} {0} --> {2}", axTransactionBeginEvent.AxSessionId.Value, axTransactionBeginEvent.AxUserName.ASCII, axTransactionBeginEvent.AxMarkerName);
                            Statistics.CLIENT.SessionsMatrix.pushEvent(axTransactionBeginEvent);

                            Tornado.TornadoEnpointData.values.number_of_events_processed++;
                        }
                        break;
                    case 52:
                        {
                            var axTransactionEndEvent = new AxTransactionEndEvent(obj);
                            Logging.Log.Instance.Debug("AxTransactionEndEvent {1} {0} --> {2}", axTransactionEndEvent.AxSessionId.Value, axTransactionEndEvent.AxUserName.ASCII, axTransactionEndEvent.AxMarkerName);
                            Statistics.CLIENT.SessionsMatrix.pushEvent(axTransactionEndEvent);

                            Tornado.TornadoEnpointData.values.number_of_events_processed++;
                        }
                        break;

                    /*Traceinfo 0x0000000000000002*/
                    case 53:
                        {
                            var axTraceMessageEvent = new AxTraceMessageEvent(obj);
                            Statistics.CLIENT.SessionsMatrix.pushEvent(axTraceMessageEvent);

                            Tornado.TornadoEnpointData.values.number_of_events_processed++;
                        }
                        break;
                    case 68:
                        {
                            var axTraceComponentMessageEvent = new AxTraceComponentMessageEvent(obj);
                            Statistics.CLIENT.SessionsMatrix.pushEvent(axTraceComponentMessageEvent);

                            Tornado.TornadoEnpointData.values.number_of_events_processed++;
                        }
                        break;



                    //LOST EVENT
                    case 0xFFFF:
                        lostEventsCount++;
                        Logging.Log.Instance.Error("EVENT_LOST {0}", obj.nsTimeStampEpoch);

                        Tornado.TornadoEnpointData.values.number_of_events_lost++;
                        break;


                    default:
                        //Logging.Log.Instance.Error("UNMANAGED EVENT ID {0}: XML\n {1}", obj.ushort_ID, obj.ToString());
                        //Console.ReadKey(true);
                        break;
                }

            }
        }
    }
}