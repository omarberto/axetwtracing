﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AxETWTracing
{
    internal class Service
    {
        private Ax2012etwReader eventRawExporter;
        private string ConfigPath;
        private bool _use_tornado;
        private string _ServiceName;

        private AX2012ETWtracing.Configuration.Nats nats;

        internal Service()
        {
            this._use_tornado = false;
        }

        internal Service(string ServiceName)
        {
            this._use_tornado = true;
            this._ServiceName = ServiceName;
        }

        private static bool TestConfig(string ConfigPath)
        {
            Logging.Log.Instance.Debug("CustomConfigurationSection.configPath: {0}", ConfigPath);

            bool result;

            List<string> warnings;
            List<string> errors;
            try
            {
                result = AX2012ETWtracing.Configuration.Configuration.Test(ConfigPath, out warnings, out errors);
            }
            catch
            (Exception e)
            {
                errors = new List<string>();
                errors.Add(string.Format("TestConfig {0} \n Exception Message {0}", e.Message));

                result = false;
            }
            foreach (var e in errors)
                Logging.Log.Instance.Error(e);
            return result;
        }

        private System.Threading.Thread thTornado = null;
        
        internal void Start(string ConfigPath)
        {
            if (!TestConfig(ConfigPath))
            {
                throw new Exception("Some errors have occurred!");
            }

            this.ConfigPath = ConfigPath;

            this.nats = AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.nats;
            //fa partire il thread DOWork
            //this.hostControl = hostControl;


            if (this._use_tornado && AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.useTornado)//qui mettere configurazione!!!!
            {
                Tornado.TornadoEndpoint endpoint = new Tornado.TornadoEndpoint(this._ServiceName);

                var thSt = new System.Threading.ThreadStart(endpoint.DoWork);
                thTornado = new System.Threading.Thread(thSt);
                thTornado.Start();
            }

            var dataSend =
                AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.nats.secure ?
                new DataSend.InfluxDataSend_Type5(nats.address, nats.subject, AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.nats.certificate)
                :
                new DataSend.InfluxDataSend_Type5(nats.address, nats.subject, null);

            //dobbiamo capire come fare per datasend multiplo, insomma, selezionabile

            new DataSend.DataSendInstance().Init(dataSend);

            //tracing ...
            this.eventRawExporter = new Ax2012etwReader(dataSend,
                AX2012ETWtracing.Configuration.Configuration.config.ax2012.tracing.provider,
                "ST",
                AX2012ETWtracing.Configuration.Configuration.config.ax2012.tracing.keywordsValue);

        }
        internal bool StartTornado(string ConfigPath)
        {
            if (!TestConfig(ConfigPath))
            {
                throw new Exception("Some errors have occurred!");
            }

            this.ConfigPath = ConfigPath;

            this.nats = AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.nats;
            //fa partire il thread DOWork
            //this.hostControl = hostControl;


            if (this._use_tornado && AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.useTornado)//qui mettere configurazione!!!!
            {
                Tornado.TornadoEndpoint endpoint = new Tornado.TornadoEndpoint(this._ServiceName);
                endpoint.Test();
                return true;
            }
            else
            {
                Console.WriteLine("Configure tornado before testing it!");
                return false;
            }
        }

        internal void DoWork()
        {
            Logging.Log.Instance.Debug("Proccess Start");
            EventReader.EndStatus endStatus = EventReader.EndStatus.Unknown;
            do
            {
                Logging.Log.Instance.Debug("Proccess Loop");
                if (eventRawExporter.Run() >= 0)
                {
                    endStatus = eventRawExporter.Process();
                }
                else
                {
                    Logging.Log.Instance.Debug("Run failed", endStatus);
                    break;
                }
                Logging.Log.Instance.Debug("Manage autokill={0}", endStatus);
                if (endStatus == EventReader.EndStatus.AutoKill) Start(ConfigPath);
            }
            while (endStatus == EventReader.EndStatus.AutoKill);
            Logging.Log.Instance.Trace("Proccess End");
        }
        
        internal void Stop()
        {
            Logging.Log.Instance.Debug("Stopping");
            if (eventRawExporter != null)
                eventRawExporter.Dispose();
            eventRawExporter = null;


            if (thTornado != null)
            {
                Tornado.TornadoEndpoint.active = false;
                thTornado.Join();
                Logging.Log.Instance.Trace("thTornado END");
            }

            Logging.Log.Instance.Debug("Stop");
        }
    }
}