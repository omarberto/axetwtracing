﻿using Microsoft.Diagnostics.Tracing;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxETWTracing
{
    public class FileConfigOptions : CustomConfigurationSection
    {
        [ConfigurationProperty("traceEventLevel", DefaultValue = "Verbose", IsRequired = false)]
        public TraceEventLevel traceEventLevel
        {
            get
            {
                return (TraceEventLevel)this["traceEventLevel"];
            }
            set
            {
                this["traceEventLevel"] = value;
            }
        }

        [ConfigurationProperty("keywords", DefaultValue = 0xFFFFFFFFFFFFFFFF, IsRequired = false)]
        public ulong keywords
        {
            get
            {
                return (ulong)this["keywords"];
            }
            set
            {
                this["keywords"] = value;
            }
        }

        [ConfigurationProperty("index", DefaultValue = 1, IsRequired = false)]
        public int index
        {
            get
            {
                return (int)this["index"];
            }
            set
            {
                this["index"] = value;
            }
        }

        [ConfigurationProperty("providerName", IsRequired = true)]
        public string providerName
        {
            get
            {
                return this["providerName"] as string;
            }
            set
            {
                this["providerName"] = value;
            }
        }

        [ConfigurationProperty("validInstances", IsRequired = false)]
        public string validInstancesList
        {
            get
            {
                return this["validInstances"] as string;
            }
            set
            {
                this["validInstances"] = value;
            }
        }
        public string[] validInstances
        {
            get
            {
                return validInstancesList.Split(new char[] { ',', ' ', ';', '|' }, StringSplitOptions.RemoveEmptyEntries);
            }
        }

        public static FileConfigOptions Load()
        {
            return Load<FileConfigOptions>();
        }
        //public static FileConfigOptions Load()
        //{
        //    FileConfigOptions settings;
        //    foreach (ConfigurationSectionGroup csg in ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).SectionGroups)
        //    {
        //        foreach (ConfigurationSection cs in csg.Sections)
        //        {
        //            if(Load(cs, out settings)) return settings;
        //        }
        //    }
        //    foreach (ConfigurationSection cs in ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).Sections)
        //    {
        //        if (Load(cs, out settings)) return settings;
        //    }
        //    throw new Exception("Configuration not found");
        //}

        //private static bool Load(ConfigurationSection cs, out FileConfigOptions settings)
        //{
        //    settings = null;
        //    if (cs.SectionInformation.Type.Contains(typeof(FileConfigOptions).AssemblyQualifiedName))
        //    {
        //        settings = (FileConfigOptions)ConfigurationManager.GetSection(cs.SectionInformation.SectionName);
        //        return true;
        //    }
        //    return false;
        //}
    }
}
