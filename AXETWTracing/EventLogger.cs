﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxETWTracing
{
    internal static class EventLogger
    {
        private const string sSource = "AxETWTracing_App";
        private const string sLog = "Application";

        public static void InitLog()
        {
            if (!EventLog.SourceExists(sSource)) EventLog.CreateEventSource(sSource, sLog);
        }

        internal static void ClearLog()
        {
            if (EventLog.SourceExists(sSource)) EventLog.DeleteEventSource(sSource);
        }
    }
}
