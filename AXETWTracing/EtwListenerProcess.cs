﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxETWTracing
{

    internal class EtwListenerProcess
    {
        internal EtwListenerProcess(TracingToolReader eventReader)
        {
            eventRawExporter = eventReader;
        }

        TracingToolReader eventRawExporter;

        private object closeClean = new object();

        public void DoWork()
        {
            Logging.Log.Instance.Debug("EtwListenerProcess.DoWork start {0}", Environment.CurrentManagedThreadId);
            int res = eventRawExporter.Run();
            if (res >= 0)
            {
                eventRawExporter.Process();
            }
            else
            {
                Logging.Log.Instance.Debug("eventRawExporter run failed {0}", res);
            }
            Logging.Log.Instance.Debug("EtwListenerProcess.DoWork end {0}", Environment.CurrentManagedThreadId);
        }

        public void Close()
        {
            Logging.Log.Instance.Debug("EtwListenerProcess.Close start");
            lock (closeClean)
            {
                if (eventRawExporter != null)
                    eventRawExporter.Dispose();
                eventRawExporter = null;
            }
            Logging.Log.Instance.Debug("EtwListenerProcess.Close end");
        }
    }
}
