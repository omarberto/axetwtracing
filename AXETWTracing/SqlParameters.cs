﻿using System;
using AxETWTracing.Events;
using System.Collections.Generic;
using System.Linq;

namespace AxETWTracing
{
    internal class SqlParameters
    {
        private List<SqlInputBind> events = new List<SqlInputBind>();

        public SqlParameters()
        {
        }

        public int Count { get { return events.Count; } }

        internal void Add(SqlInputBind sqlInputBind)
        {
            ////if (events.Count > 0 && events.Last().seqNum + 1 != sqlInputBind.seqNum)
            //if (events.Count > 0 && events.Last().AxBindColId + 1 != sqlInputBind.AxBindColId)
            //{
            //    events.Clear();//da vedere qui per errore, forse due sequenze per due comandi
            //    //ocio che forse il problema grosso é qui, cioé creo un log con AxBindColId
            //}
            events.Add(sqlInputBind);
        }

        public SqlInputBind this[int index]
        {
            get
            {
                return events.ElementAt(index);
            }
        }

        internal void Clear()
        {
            events.Clear();
        }
    }
}