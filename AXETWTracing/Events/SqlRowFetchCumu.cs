﻿using Microsoft.Diagnostics.Tracing;
using AxETWTracing.Unsafe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AxETWTracing.Statistics;

namespace AxETWTracing.Events
{
    internal unsafe class SqlRowFetchCumu
    {
        internal SqlRowFetchCumu(TraceEvent obj)
        {
            this.nsTimeStampEpoch = obj.nsTimeStampEpoch;
            this.processId = obj.ProcessID;

            /*
             <template tid="PerformanceTask67Args">
              <data name="seqNum" inType="win:Int32"/>
              <data name="AxServerName" inType="win:UnicodeString"/>
              <data name="AxUserName" inType="win:UnicodeString"/>
              <data name="AxSessionId" inType="win:Int32"/>
              <data name="AxRC" inType="win:Int32"/>
              <data name="AxConDBSpid" inType="win:Int64"/>
              <data name="AxDuration" inType="win:Int64"/>
              <data name="AxFetchCount" inType="win:Int64"/>
             </template>
            */

            byte[] payload = obj.EventData();

            int offset = 0;
            this.seqNum = RawReader.ReadInt32(payload, offset); offset += 4;
            this.AxServerName = RawReader.ReadUnicodeString(payload, ref offset);//ottimizzare con lettura???
            this.AxUserName = new AxUserName() { Value = RawReader.ReadUnicodeString(payload, ref offset) };
            this.AxSessionId = new AxSessionId() { Value = RawReader.ReadInt32(payload, offset) }; offset += 8;
            this.AxConDBSpid = new AxConDBSpid() { Value = RawReader.ReadInt64(payload, offset) }; offset += 8;
            this.AxDuration = Convert.ToInt64(RawReader.ReadInt64(payload, offset) * obj.Source.QPCConv); offset += 8;
            this.AxFetchCount = RawReader.ReadInt64(payload, offset);
        }

        public long nsTimeStampEpoch { get; private set; }

        public int seqNum { get; private set; }//da eliminare

        public int processId { get; private set; }
        public string AxServerName { get; private set; }
        public AxUserName AxUserName { get; private set; }
        public AxSessionId AxSessionId { get; private set; }
        public AxConDBSpid AxConDBSpid { get; private set; }

        public long AxDuration { get; private set; }
        public long AxFetchCount { get; private set; }

        public override string ToString()
        {
            return string.Format("{0} -> {2} {1} // {3} {4} {5}", seqNum, AxUserName, AxSessionId, AxConDBSpid, AxDuration, AxFetchCount);
        }
    }
}
