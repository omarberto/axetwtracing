﻿using AxETWTracing.Unsafe;
using Microsoft.Diagnostics.Tracing;
using System;

namespace AxETWTracing.Events
{
    internal enum ClientEventType : ushort
    {
        FormOpen = 95,
        FormClose = 96,
        ControlClicked = 97
    }

    internal class ClientEvent
    {
        public ClientEvent(TraceEvent obj)
        {
            this.nsTimeStampEpoch = obj.nsTimeStampEpoch;
            this.processId = obj.ProcessID;

            /*
             <template tid="task_0FormOpenedArgs">
              <data name="seqNum" inType="win:Int32"/>
              <data name="AxServerName" inType="win:UnicodeString"/>
              <data name="AxUserName" inType="win:UnicodeString"/>
              <data name="AxSessionId" inType="win:Int32"/>
              <data name="ControlName" inType="win:UnicodeString"/>
             </template>
            */

            eventType = (ClientEventType)obj.ushort_ID;
            AxComputerName = Environment.MachineName;

            byte[] payload = obj.EventData();

            int offset = 0;
            this.seqNum = RawReader.ReadInt32(payload, offset); offset += 4;
            this.AxServerName = RawReader.ReadUnicodeString(payload, ref offset);//ottimizzare con lettura???
            this.AxUserName = RawReader.ReadUnicodeString(payload, ref offset);
            this.AxSessionId = RawReader.ReadInt32(payload, offset); offset += 4;
            this.ControlName = RawReader.ReadUnicodeString(payload, ref offset);

        }

        public long nsTimeStampEpoch { get; private set; }

        public int seqNum { get; private set; }//da eliminare

        public ClientEventType eventType { get; private set; }
        public int processId { get; private set; }
        public string AxServerName { get; private set; }//AOS
        public string AxComputerName { get; private set; }
        public string AxUserName { get; private set; }
        public int AxSessionId { get; private set; }
    
        public string ControlName { get; private set; }

        public override string ToString()
        {
            return string.Format("{1}:{0} --> {3}:{2}", AxUserName, AxSessionId, ControlName, eventType);
        }
    }
}