﻿using Microsoft.Diagnostics.Tracing;
using AxETWTracing.Unsafe;
using AxETWTracing.Statistics;


namespace AxETWTracing.Events
{
    internal unsafe class ClassLoading //event 13 Caching,AOT only client???
    {

        internal ClassLoading(TraceEvent obj)
        {
            this.nsTimeStampEpoch = obj.nsTimeStampEpoch;
            this.processId = obj.ProcessID;

            /*
                 <template tid="AOTTask13Args">
                  <data name="seqNum" inType="win:Int32"/>
                  <data name="AxServerName" inType="win:UnicodeString"/>
                  <data name="AxUserName" inType="win:UnicodeString"/>
                  <data name="AxSessionId" inType="win:Int32"/>
                  <data name="AxClassId" inType="win:Int64"/>
                  <data name="AxClassName" inType="win:UnicodeString"/>
                  <data name="AxLoadingStrategy" inType="win:UnicodeString"/>
                  <data name="AxRequestType" inType="win:UnicodeString"/>
                  <data name="AxComponentName" inType="win:UnicodeString"/>
                  <data name="AxBufferSize" inType="win:Int64"/>
                 </template>
            */

            byte[] payload = obj.EventData();
            int offset = 0;
            this.seqNum = RawReader.ReadInt32(payload, offset); offset += 4;
            this.AxServerName = RawReader.ReadUnicodeString(payload, ref offset);//ottimizzare con lettura???
            this.AxUserName = new AxUserName() { Value = RawReader.ReadUnicodeString(payload, ref offset) };
            this.AxSessionId = new AxSessionId() { Value = RawReader.ReadInt32(payload, offset) }; offset += 4;
            this.AxClassId = RawReader.ReadInt64(payload, offset); offset += 8;
            this.AxClassName = RawReader.ReadUnicodeString(payload, ref offset);
            this.AxLoadingStrategy = RawReader.ReadUnicodeString(payload, ref offset);
            this.AxRequestType = RawReader.ReadUnicodeString(payload, ref offset);
            this.AxComponentName = RawReader.ReadUnicodeString(payload, ref offset);
            this.AxBufferSize = RawReader.ReadInt64(payload, offset);
        }

        public long nsTimeStampEpoch { get; private set; }

        public int seqNum { get; private set; }//da eliminare

        public int processId { get; private set; }
        public string AxServerName { get; private set; }
        public AxUserName AxUserName { get; private set; }
        public AxSessionId AxSessionId { get; private set; }

        public long AxClassId { get; private set; }
        public string AxClassName { get; private set; }
        public string AxLoadingStrategy { get; private set; }
        public string AxRequestType { get; private set; }
        public string AxComponentName { get; private set; }
        public long AxBufferSize { get; private set; }
    }
}
