﻿using Microsoft.Diagnostics.Tracing;
using AxETWTracing.Unsafe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AxETWTracing.WMI;

namespace AxETWTracing.Events
{
    internal class TraceInformation
    {
        private AosInstance _AosInstance;

        internal TraceInformation(TraceEvent obj)
        {
            this.ProcessId = obj.ProcessID;
            this._AosInstance = new WMI.AosInstance(obj.ProcessID);

            byte[] payload = obj.EventData();
            int offset = 0;
            this.ProcessName = RawReader.ReadUnicodeString(payload, ref offset);
            this.ComputerName = RawReader.ReadUnicodeString(payload, ref offset);
            this.AxVersion = RawReader.ReadUnicodeString(payload, ref offset);
            this.UserName = RawReader.ReadUnicodeString(payload, ref offset);
        }

        public int ProcessId { get; private set; }

        public string ProcessName { get; private set; }
        public string ComputerName { get; private set; }
        public string AxVersion { get; private set; }
        public string UserName { get; private set; }

        public bool isAxServer
        {
            get
            {
                return _AosInstance.isAx32Serv;
            }
        }
        public int instanceNumber
        {
            get
            {
                return _AosInstance.instanceNumber;
            }
        }
        public Statistics.AxServerInstance instance
        {
            get
            {
                return new Statistics.AxServerInstance() { InstanceNumber = _AosInstance.instanceNumber, ServerName = ComputerName };
            }
        }

        public override string ToString()
        {
            return ProcessId + ": " + ProcessName;
        }
    }
}
