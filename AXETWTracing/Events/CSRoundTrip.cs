﻿using Microsoft.Diagnostics.Tracing;
using AxETWTracing.Unsafe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using AxETWTracing.Statistics;

namespace AxETWTracing.Events
{
    internal unsafe class CSRoundTripBegin
    {
        internal CSRoundTripBegin(TraceEvent obj)
        {
            this.nsTimeStampEpoch = obj.nsTimeStampEpoch;

            /*
             <template tid="PerformanceTaskStart54Args">
              <data name="seqNum" inType="win:Int32"/>
              <data name="AxServerName" inType="win:UnicodeString"/>
              <data name="AxUserName" inType="win:UnicodeString"/>
              <data name="AxSessionId" inType="win:Int32"/>
              <data name="AxRC" inType="win:Int32"/>
              <data name="AxRPCName" inType="win:UnicodeString"/>
              <data name="AxNestLevel" inType="win:Int64"/>
              <data name="AxServerBytesRecd" inType="win:Int64"/>
              <data name="AxServerBytesSent" inType="win:Int64"/>
             </template>
            */
            
            byte[] payload = obj.EventData();
            this.processId = obj.ProcessID;

            int offset = 0;
            this.seqNum = RawReader.ReadInt32(payload, offset); offset += 4;
            this.AxServerName = RawReader.ReadUnicodeString(payload, ref offset);//ottimizzare con lettura???
            this.AxUserName = new AxUserName() { Value = RawReader.ReadUnicodeString(payload, ref offset) };
            this.AxSessionId = new AxSessionId() { Value = RawReader.ReadInt32(payload, offset) }; offset += 8;
            this.AxRPCName = RawReader.ReadUnicodeString(payload, ref offset);
            this.AxNestLevel = RawReader.ReadInt64(payload, offset); offset += 8;//se non si legge sopra si puó calcolare offsett all'indietro
        }

        public long nsTimeStampEpoch { get; private set; }

        public int seqNum { get; private set; }//da eliminare

        public int processId { get; private set; }
        public string AxServerName { get; private set; }
        public AxUserName AxUserName { get; private set; }
        public AxSessionId AxSessionId { get; private set; }

        public string AxRPCName { get; private set; }//da eliminare
        public long AxNestLevel { get; private set; }

        public override string ToString()
        {
            return string.Format("{6} RPC {5} -> {1}:{2}:{3} | {4}", seqNum, AxServerName, AxUserName.Value, AxSessionId.Value, AxRPCName, AxNestLevel, processId);
        }
    }
    internal unsafe class CSRoundTripEnd
    {
        internal CSRoundTripEnd(TraceEvent obj)
        {
            this.nsTimeStampEpoch = obj.nsTimeStampEpoch;
            this.processId = obj.ProcessID;

            /*
             <template tid="PerformanceTaskStart54Args">
              <data name="seqNum" inType="win:Int32"/>
              <data name="AxServerName" inType="win:UnicodeString"/>
              <data name="AxUserName" inType="win:UnicodeString"/>
              <data name="AxSessionId" inType="win:Int32"/>
              <data name="AxRC" inType="win:Int32"/>
              <data name="AxRPCName" inType="win:UnicodeString"/>
              <data name="AxNestLevel" inType="win:Int64"/>
              <data name="AxServerBytesRecd" inType="win:Int64"/>
              <data name="AxServerBytesSent" inType="win:Int64"/>
             </template>
            */

            byte[] payload = obj.EventData();
            int offset = 0;
            this.seqNum = RawReader.ReadInt32(payload, offset); offset += 4;
            this.AxServerName = RawReader.ReadUnicodeString(payload, ref offset);//ottimizzare con lettura???
            this.AxUserName = new AxUserName() { Value = RawReader.ReadUnicodeString(payload, ref offset) };
            this.AxSessionId = new AxSessionId() { Value = RawReader.ReadInt32(payload, offset) }; offset += 8;
            this.AxRPCName = RawReader.ReadUnicodeString(payload, ref offset);
            this.AxNestLevel = RawReader.ReadInt64(payload, offset); offset += 8;//se non si legge sopra si puó calcolare offsett all'indietro
            this.AxServerBytesRecd = RawReader.ReadInt64(payload, offset); offset += 8;
            this.AxServerBytesSent = RawReader.ReadInt64(payload, offset);
        }

        public long nsTimeStampEpoch { get; private set; }

        public int seqNum { get; private set; }//da eliminare

        public int processId { get; private set; }
        public string AxServerName { get; private set; }
        public AxUserName AxUserName { get; private set; }
        public AxSessionId AxSessionId { get; private set; }

        public string AxRPCName { get; private set; }//da eliminare
        public long AxNestLevel { get; private set; }
        public long AxServerBytesRecd { get; private set; }
        public long AxServerBytesSent { get; private set; }

        public override string ToString()
        {
            return string.Format("{0} -> {2} {1} // {3}", seqNum, AxUserName, AxSessionId, AxRPCName);
        }
    }
}
