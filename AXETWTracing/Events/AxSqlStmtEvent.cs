﻿using Microsoft.Diagnostics.Tracing;
using AxETWTracing.Unsafe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AxETWTracing.Statistics;

namespace AxETWTracing.Events
{
    internal unsafe class AxSqlStmtEvent
    {
        internal AxSqlStmtEvent(TraceEvent obj)
        {
            this.nsTimeStampEpoch = obj.nsTimeStampEpoch;
            this.processId = obj.ProcessID;

            /*
            <template tid="PerformanceTask59Args">
            <data name="seqNum" inType="win:Int32"/>
            <data name="AxServerName" inType="win:UnicodeString"/>
            <data name="AxUserName" inType="win:UnicodeString"/>
            <data name="AxSessionId" inType="win:Int32"/>
            <data name="AxRC" inType="win:Int32"/>
            <data name="AxConDBSpid" inType="win:Int64"/>
            <data name="AxSqlStmt" inType="win:UnicodeString"/>
            <data name="AxDuration" inType="win:Int64"/>
            <data name="AxPrepDuration" inType="win:Int64"/>
            </template>
            */
            
            byte[] payload = obj.EventData();

            int offset = 0;
            this.seqNum = RawReader.ReadInt32(payload, offset); offset += 4;
            this.AxServerName = RawReader.ReadUnicodeString(payload, ref offset);//ottimizzare con lettura???
            this.AxUserName = new AxUserName() { Value = RawReader.ReadUnicodeString(payload, ref offset) };
            this.AxSessionId = new AxSessionId() { Value = RawReader.ReadInt32(payload, offset) }; offset += 8;
            this.AxConDBSpid = new AxConDBSpid() { Value = RawReader.ReadInt64(payload, offset) }; offset += 8;
            this.AxSqlStmt = RawReader.ReadUnicodeString(payload, ref offset);
            this.AxDuration = Convert.ToInt64(RawReader.ReadInt64(payload, offset) * obj.Source.QPCConv); offset += 8;
            this.AxPrepDuration = Convert.ToInt64(RawReader.ReadInt64(payload, offset) * obj.Source.QPCConv);

            //AxInstanceSessionUserDBSpidTh.ThID = obj.ThreadID;
            //AxInstanceSessionUserDBSpidTh.DBSpid.Value = AxConDBSpid;
        }

        public long nsTimeStampEpoch { get; private set; }

        public int seqNum { get; private set; }//da eliminare

        public int processId { get; private set; }
        public string AxServerName { get; private set; }
        public AxUserName AxUserName { get; private set; }
        public AxSessionId AxSessionId { get; private set; }
        public AxConDBSpid AxConDBSpid { get; private set; }

        public string AxSqlStmt { get; private set; }
        public bool isBatchSelect {
            get
            {
                if (AxSqlStmt.IndexOf("UPDATE BATCH SET ") == 0)
                {
                    //AxSqlStmt.Split('?')
                    return true;
                }
                return false;
            }
        }

        public long AxDuration { get; private set; }
        public long AxPrepDuration { get; private set; }

        public override string ToString()
        {
            return string.Format("SQL {1}:{2}:{3} ->{4}", seqNum, AxUserName.Value, AxSessionId.Value, AxConDBSpid.Value, AxSqlStmt, AxDuration, AxPrepDuration);
        }
    }


    internal unsafe class SqlStmtParser
    {
        public enum StmtType
        {
            none,

            other,
            select,
            update,
            insert,
            delete,
            call
        }

        public Stmt ParseBatch(string stmt)
        {
            Stmt res = new Stmt();

            string result = string.Empty;

            int len = stmt.Length;

            bool phaseA = true;

            bool phaseB = false;
            bool quotedColumn = false;

            bool phaseC = false;

            fixed (char* p1 = stmt)
            {
                char* a = p1;
                ushort* b = (ushort*)p1;
                ushort* c = (ushort*)p1;

                while (len > 0)
                {
                    if (phaseA)
                    {
                        if (*a == ' ' || *a == '(')//'(' forse caso call potrebbe richiedere + specializzazione....
                        {
                            if (*(a - 1) == ' ')
                            {
                                a++;
                                len--;
                                continue;
                            }

                            switch (res.stmtType)
                            {
                                case StmtType.none:
                                    {
                                        switch (result)
                                        {
                                            case "SELECT":
                                                res.stmtType = StmtType.select;
                                                phaseA = false;
                                                break;
                                            case "INSERT":
                                                res.stmtType = StmtType.insert;
                                                break;
                                            case "UPDATE":
                                                res.stmtType = StmtType.update;
                                                break;
                                            case "DELETE":
                                                res.stmtType = StmtType.delete;
                                                break;
                                            case "{call":
                                                res.stmtType = StmtType.call;
                                                break;
                                            default:
                                                res.stmtType = StmtType.other;
                                                phaseA = false;
                                                break;
                                        }
                                    }
                                    break;
                                case StmtType.update:
                                    {
                                        if (res.table.Length > 0)
                                            if (result != "SET")
                                            {
                                                res.stmtType = StmtType.other;
                                                phaseA = false;
                                            }
                                            else
                                            {
                                                phaseA = false;
                                                switch (res.table)
                                                {
                                                    case "BATCH":
                                                    case "SYSCLIENTSESSIONS":
                                                        {
                                                            phaseB = true;
                                                            a++;
                                                            b = (ushort*)a;
                                                            len--;
                                                        }
                                                        break;
                                                }
                                            }
                                        else
                                            res.table = result;
                                    }
                                    break;
                                case StmtType.insert:
                                    {
                                        if (result != "INTO")
                                        {
                                            if (res.table.Length == 0)
                                            {
                                                phaseA = false;
                                                res.table = result;

                                                switch (res.table)
                                                {
                                                    case "BATCH":
                                                    //case "BATCHHISTORY":
                                                    case "BATCHJOB":
                                                    //case "BATCHJOBHISTORY":
                                                        {
                                                            phaseC = true;
                                                            a++;
                                                            c = (ushort*)a;
                                                            len--;
                                                        }
                                                        break;
                                                }

                                            }
                                            else
                                            {
                                                res.stmtType = StmtType.other;
                                                phaseA = false;
                                            }
                                        }
                                    }
                                    break;
                                case StmtType.delete:
                                    {
                                        if (result != "FROM")
                                        {
                                            if (res.table.Length == 0)
                                            {
                                                phaseA = false;
                                                res.table = result;
                                            }
                                            else
                                            {
                                                res.stmtType = StmtType.other;
                                                phaseA = false;
                                            }
                                        }
                                    }
                                    break;
                                case StmtType.call:
                                    {
                                        res.table = result;//dovrei cambiare variabile ma chi se ne frega
                                        phaseA = false;
                                    }
                                    break;
                                default:
                                    {
                                        //it shouldn't happen
                                        phaseA = false;
                                    }
                                    break;
                            }
                            result = string.Empty;
                        }
                        else
                        {
                            result += *a;
                        }
                    }

                    if (phaseB)
                    {
                        if (!quotedColumn)
                        {
                            if (*b == 0x27)
                                quotedColumn = true;
                            else if (*b == 0x5F || (0x40 < *b && *b < 0x5B) || (0x60 < *b && *b < 0x7B) || (0x2F < *b && *b < 0x3A))
                            {
                                result += *a;
                            }
                            else
                            {
                                if (result.Length > 0)
                                {
                                    if (*b == 0x3D && len > 0 && *(b + 1) == 0x3F)
                                    {
                                        res.columns.Add(result);
                                    }
                                    result = string.Empty;
                                }
                            }
                        }
                        else
                        {
                            if (*b == 0x27)
                                quotedColumn = false;
                            else
                                result += *a;
                        }

                        b++;
                    }

                    if (phaseC)
                    {
                        if (*a == ' ')
                        {
                            a++;
                            c++;
                            len--;
                            continue;
                        }

                        if (!quotedColumn)
                        {
                            if (*c == 0x27)
                                quotedColumn = true;
                            else if (*c == 0x5F || (0x40 < *c && *c < 0x5B) || (0x60 < *c && *c < 0x7B) || (0x2F < *c && *c < 0x3A))
                            {
                                result += *a;
                            }
                            else
                            {
                                if (result.Length > 0)
                                {
                                    if (*c == 0x29 || *c == 0x2C)
                                    {
                                        res.columns.Add(result);
                                    }
                                    result = string.Empty;
                                    if (*c == 0x29)
                                    {
                                        break;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (*c == 0x27)
                                quotedColumn = false;
                            else
                                result += *a;
                        }

                        c++;
                    }

                    a++;
                    len--;

                    if (!phaseA && !phaseB && !phaseC)
                        break;
                }
            }


            return res;
        }

        public class Stmt
        {
            public StmtType stmtType = StmtType.none;
            public string table = string.Empty;
            public List<string> columns = new List<string>();
        }
    }

    internal class BatchEvent
    {
        internal class BatchHistory
        {
            public uint recversion = 0;
            public long nsStartDateTime;
            public string status;
            private string axSessionId;
            private string axUserName;
        }

        private static Dictionary<ulong, BatchHistory> bhistory = new Dictionary<ulong, BatchHistory>();
        private static Dictionary<uint, ulong> bversions = new Dictionary<uint, ulong>();

        public bool startEvent = false;
        public bool exitEvent = false;

        private string status = string.Empty;

        private string axSessionId;
        private string axUserName;
        private string recVersion;
        private string recVersionOld;
        private string recId;

        private ulong? ul_recId;
        private uint? ui_recVersion;

        internal BatchEvent(SqlStmtParser.Stmt stmt, SqlParameters pars, AxSessionId axSessionId, AxUserName axUserName, long nsTimeStampEpoch)
        {
            // a far bene dovrei stabilire anche se i paramertri sono di set/where (o leggere solo quelli di set)
            for (int i = 0; i < stmt.columns.Count; i++)
            {
                switch (stmt.columns[i])
                {
                    case "SESSIONIDX":
                        this.axSessionId = pars[i].AxBindVarValue;
                        break;
                    case "EXECUTEDBY":
                        this.axUserName = pars[i].AxBindVarValue;
                        break;

                    case "RECVERSION":
                        if (this.recVersion == null)
                            this.recVersion = pars[i].AxBindVarValue;
                        else
                            this.recVersionOld = pars[i].AxBindVarValue;
                        break;
                    case "RECID":
                        this.recId = pars[i].AxBindVarValue;
                        break;


                    case "STATUS":
                        if (this.status.Length == 0)
                        {
                            this.status = pars[i].AxBindVarValue;
                            switch (pars[i].AxBindVarValue)
                            {
                                case "0": //Hold
                                    this.status = "Hold";
                                    break;
                                case "1": //Waiting
                                    this.status = "Waiting";
                                    break;
                                case "2": //Executing
                                    this.startEvent = true;
                                    this.status = "Executing";
                                    break;
                                case "3": //Error
                                    this.exitEvent = true;
                                    this.status = "Error";
                                    break;
                                case "4"://Finishing
                                    this.exitEvent = true;
                                    this.status = "Finished";
                                    break;
                                case "5"://Ready
                                    this.status = "Ready";
                                    break;
                                case "6"://Ready
                                    this.status = "NotRun";
                                    break;
                                case "7"://Ready
                                    this.status = "Cancelling";
                                    break;
                                case "8"://Canceled
                                    this.exitEvent = true;
                                    this.status = "Canceled";
                                    //this.validEvent = true;
                                    break;
                                default:
                                    //this.validEvent = true;
                                    this.status = pars[i].AxBindVarValue;
                                    break;
                            }
                        }
                        break;

                    //case "STARTDATETIME":

                    //    break;
                    //case "ENDDATETIME":

                    //    break;
                    //case "MODIFIEDDATETIME":

                    //    break;

                    //case "EXCEPTIONCODE":

                    //    break;
                    //case "BATCHJOBID":

                    //    break;

                    default:

                        break;
                }
            }

            ul_recId = recId == null ? new ulong?() : ulong.Parse(recId);
            uint? ui_recVersionOld = recVersionOld == null ? new uint?() : uint.Parse(recVersionOld);
            ui_recVersion = recVersion == null ? new uint?() : uint.Parse(recVersion);

            //se ho un recversion change posso eventualmente recuperare recid 
            if (ui_recVersion.HasValue && ui_recVersionOld.HasValue)
            {
                if (bversions.ContainsKey(ui_recVersionOld.Value))
                {
                    if (!ul_recId.HasValue)//potrei fare un check
                        ul_recId = bversions[ui_recVersionOld.Value];

                    bversions.Remove(ui_recVersionOld.Value);
                    bversions.Add(ui_recVersion.Value, ul_recId.Value);
                }
                else if (ul_recId.HasValue)
                    if (bversions.ContainsKey(ui_recVersion.Value))
                        bversions[ui_recVersion.Value] = ul_recId.Value;
                    else
                        bversions.Add(ui_recVersion.Value, ul_recId.Value);
                
            }


            if (!ul_recId.HasValue && ui_recVersion.HasValue)
            {
                if (bversions.ContainsKey(ui_recVersion.Value))
                    ul_recId = bversions[ui_recVersion.Value];
            }

            if (ul_recId.HasValue)
            {
                if (!bhistory.ContainsKey(ul_recId.Value))
                {
                    bhistory.Add(ul_recId.Value, new BatchHistory());
                    bhistory[ul_recId.Value].nsStartDateTime = nsTimeStampEpoch;
                }

                this.recId = ul_recId.ToString();

                bhistory[ul_recId.Value].status = this.status;
                if (ui_recVersion.HasValue)
                {
                    bhistory[ul_recId.Value].recversion = ui_recVersion.Value;
                    this.recVersion = ui_recVersion.ToString();
                }
            }
        }

        //internal BatchEvent(SqlStmtParser.Stmt stmt, SqlParameters pars, AxSessionId axSessionId, AxUserName axUserName, string AxSqlStmt)//, string axServerName, AxSessionId axSessionId, AxUserName axUserName, long nsTimeStampEpoch) 
        //{
        //    //Cerco Status -- 
        //    //Executing 2
        //    //Error 3
        //    //Finished 4
        //    //Canceled 8

        //    //i datetime li metto a now

        //    //RECVERSION .. RECID




        //    //CAPTION     nvarchar
        //    //SERVERID    nvarchar
        //    //EXECUTEDBY  nvarchar
        //    //STATUS      int
        //    //EXCEPTIONCODE int
        //    //SESSIONIDX    int
        //    //BATCHJOBID      bigint
        //    //STARTDATETIME    datetime
        //    //ENDDATETIME      datetime                
        //    //MODIFIEDDATETIME datetime
        //    //RECVERSION  int
        //    //RECID bigint
            

        //    // a far bene dovrei stabilire anche se i paramertri sono di set/where (o leggere solo quelli di set)
        //    for (int i = 0; i < stmt.columns.Count; i++)
        //    {
        //        switch (stmt.columns[i])
        //        {
        //            case "SESSIONIDX":
        //                this.axSessionId = pars[i].AxBindVarValue;
        //                break;
        //            case "EXECUTEDBY":
        //                this.axUserName = pars[i].AxBindVarValue;
        //                break;

        //            case "RECVERSION":
        //                if (this.recVersion == null)
        //                    this.recVersion = pars[i].AxBindVarValue;
        //                else
        //                    this.recVersionOld = pars[i].AxBindVarValue;
        //                break;
        //            case "RECID":
        //                this.recId = pars[i].AxBindVarValue;
        //                break;


        //            case "STATUS":
        //                if (this.status.Length == 0)
        //                {
        //                    this.status = pars[i].AxBindVarValue;
        //                    switch (pars[i].AxBindVarValue)
        //                    {
        //                        case "0": //Hold
        //                            this.status = "Hold";
        //                            break;
        //                        case "1": //Waiting
        //                            this.status = "Waiting";
        //                            break;
        //                        case "2": //Executing
        //                            this.startEvent = true;
        //                            this.status = "Executing";
        //                            break;
        //                        case "3": //Error
        //                            this.exitEvent = true;
        //                            this.status = "Error";
        //                            break;
        //                        case "4"://Finishing
        //                            this.exitEvent = true;
        //                            this.status = "Finished";
        //                            break;
        //                        case "5"://Ready
        //                            this.status = "Ready";
        //                            break; 
        //                        case "6"://Ready
        //                            this.status = "NotRun";
        //                            break; 
        //                        case "7"://Ready
        //                            this.status = "Cancelling";
        //                            break;
        //                        case "8"://Canceled
        //                            this.exitEvent = true;
        //                            this.status = "Canceled";
        //                            //this.validEvent = true;
        //                            break;
        //                        default:
        //                            //this.validEvent = true;
        //                            this.status = pars[i].AxBindVarValue;
        //                            break;
        //                    }
        //                }
        //                break;
                        
        //            //case "STARTDATETIME":

        //            //    break;
        //            //case "ENDDATETIME":

        //            //    break;
        //            //case "MODIFIEDDATETIME":

        //            //    break;

        //            //case "EXCEPTIONCODE":

        //            //    break;
        //            //case "BATCHJOBID":

        //            //    break;

        //            default:

        //                break;
        //        }
        //    }

        //    ul_recId = recId == null ? new ulong?() : ulong.Parse(recId);
        //    uint? ui_recVersionOld = recVersionOld == null ? new uint?() : uint.Parse(recVersionOld);
        //    ui_recVersion = recVersion == null ? new uint?() : uint.Parse(recVersion);

        //    //se ho un recversion change posso eventualmente recuperare recid 
        //    if (ui_recVersion.HasValue && ui_recVersionOld.HasValue)
        //    {
        //        if (bversions.ContainsKey(ui_recVersionOld.Value))
        //        {
        //            if (!ul_recId.HasValue)//potrei fare un check
        //                ul_recId = bversions[ui_recVersionOld.Value];

        //            bversions.Remove(ui_recVersionOld.Value);
        //            bversions.Add(ui_recVersion.Value, ul_recId.Value);
        //        }
        //        else if (ul_recId.HasValue)
        //            if (bversions.ContainsKey(ui_recVersion.Value))
        //                bversions[ui_recVersion.Value] = ul_recId.Value;
        //            else
        //                bversions.Add(ui_recVersion.Value, ul_recId.Value);
        
        //    }
            

        //    if (!ul_recId.HasValue && ui_recVersion.HasValue)
        //    {
        //        if (bversions.ContainsKey(ui_recVersion.Value))
        //            ul_recId = bversions[ui_recVersion.Value];
        //    }

        //    if (ul_recId.HasValue)
        //    {
        //        if (!bhistory.ContainsKey(ul_recId.Value))
        //        {
        //            bhistory.Add(ul_recId.Value, new BatchHistory());
        //            bhistory[ul_recId.Value].nsStartDateTime = EpochNanoseconds.UtcNow.Value;
        //        }

        //        this.recId = ul_recId.ToString();

        //        bhistory[ul_recId.Value].status = this.status;
        //        if (ui_recVersion.HasValue)
        //        {
        //            bhistory[ul_recId.Value].recversion = ui_recVersion.Value;
        //            this.recVersion = ui_recVersion.ToString();
        //        }


            
        //    }
        //}

        internal void GetMessage(int processId, AxSessionId axSessionId, AxUserName axUserName, long nsTimeStampEpoch)
        {
            if (axSessionId.Value > 2) this.axSessionId = axSessionId.Value.ToString();
            if (axSessionId.Value > 2) this.axUserName = axUserName.ASCII;
            
            if (this.startEvent)
            {
                    Logging.Log.Instance.Debug("{7} {6}::{5} // Begin Batch with RecId {0} RecVersion {1} : {2} // {3}-{4}", 
                        this.recId, this.recVersion, this.status, bhistory.Count, bversions.Count,
                        this.axSessionId, this.axUserName, nsTimeStampEpoch);
                

                if (ul_recId.HasValue && bhistory.ContainsKey(ul_recId.Value))
                {
                    int tmp;
                    if (!string.IsNullOrEmpty(this.axSessionId) && int.TryParse(this.axSessionId, out tmp))
                    {
                        //possiamo forzare tipo batch
                        AosInstances.instance.getServer(processId).addBatchStart(new AxSessionId { Value = tmp }, new AxUserName { Value = this.axUserName }, bhistory[ul_recId.Value].nsStartDateTime, this.status, ul_recId.Value.ToString());
                    
                    }
                }
                
            }


            if (this.exitEvent)
            {
                Logging.Log.Instance.Debug("{7} {6}::{5} // End Batch with RecId {0} RecVersion {1} : {2} // {3}-{4}", 
                    this.recId, this.recVersion, this.status, bhistory.Count, bversions.Count,
                    this.axSessionId, this.axUserName, nsTimeStampEpoch);


                if (ul_recId.HasValue && bhistory.ContainsKey(ul_recId.Value))
                {
                    int tmp;
                    if (!string.IsNullOrEmpty(this.axSessionId) && int.TryParse(this.axSessionId, out tmp))
                    {
                        //possiamo chiudere tipo batch, afar bene salviamo sessione e user anche nella histiry
                        //scrivo anche lo start, se non esisteva devo aggiornare tutto all'indietro...
                        AosInstances.instance.getServer(processId).addBatchEnd(new AxSessionId { Value = tmp }, new AxUserName { Value = this.axUserName }, bhistory[ul_recId.Value].nsStartDateTime, ul_recId.Value.ToString(), this.status, nsTimeStampEpoch);

                    }
                }

                if (ui_recVersion.HasValue && bversions.ContainsKey(ui_recVersion.Value)) bversions.Remove(ui_recVersion.Value);
                if (ul_recId.HasValue && bhistory.ContainsKey(ul_recId.Value)) bhistory.Remove(ul_recId.Value);
            }
            
        }
    }
    internal class BatchJobEvent
    {
        internal class BatchJobHistory
        {
            public uint recversion = 0;
            public DateTime lastDateTime;
            public string status;
            private string axSessionId;
            private string axUserName;
        }

        private static Dictionary<ulong, BatchJobHistory> bjhistory = new Dictionary<ulong, BatchJobHistory>();
        private static Dictionary<uint, ulong> bjversions = new Dictionary<uint, ulong>();

        private bool validEvent = false;

        private string status = string.Empty;

        private string axSessionId;
        private string axUserName;
        private string recVersion;
        private string recVersionOld;
        private string recId;

        private ulong? ul_recId;
        private uint? ui_recVersion;

        internal BatchJobEvent(SqlStmtParser.Stmt stmt, SqlParameters pars, AxSessionId axSessionId, AxUserName axUserName, string AxSqlStmt)//, string axServerName, AxSessionId axSessionId, AxUserName axUserName, long nsTimeStampEpoch) 
        {
            //Cerco Status -- 
            //Executing 2
            //Error 3
            //Finished 4
            //Canceled 8

            //i datetime li metto a now

            //RECVERSION .. RECID

            //FINISHING int
            //STATUS int
            //CAPTION nvarchar
            //COMPANY nvarchar
            //CANCELEDBY nvarchar
            //LOGLEVEL int
            //RUNTIMEJOB int
            //ORIGSTARTDATETIME datetime
            //ORIGSTARTDATETIMETZID int
            //STARTDATETIME datetime
            //ENDDATETIME datetime
            //DATAPARTITION nvarchar
            //STARTDATE datetime
            //STARTTIME int
            //CREATEDDATETIME datetime
            //CREATEDBY nvarchar

            //RECVERSION  int
            //RECID bigint


            // a far bene dovrei stabilire anche se i paramertri sono di set/where (o leggere solo quelli di set)
            for (int i = 0; i < stmt.columns.Count; i++)
            {
                switch (stmt.columns[i])
                {

                    case "RECVERSION":
                        this.recVersion = pars[i].AxBindVarValue;
                        break;
                    case "RECID":
                        this.recId = pars[i].AxBindVarValue;
                        break;


                    case "STATUS":
                        if (this.status.Length == 0)
                        {
                            this.status = pars[i].AxBindVarValue;
                            switch (pars[i].AxBindVarValue)
                            {
                                case "0": //Hold
                                    this.status = "Hold";
                                    break;
                                case "1": //Waiting
                                    this.status = "Waiting";
                                    break;
                                case "2": //Executing
                                    this.status = "Executing";
                                    this.validEvent = true;
                                    break;
                                case "3": //Error
                                    this.status = "Error";
                                    this.validEvent = true;
                                    break;
                                case "4"://Finishing
                                    this.status = "Finished";
                                    this.validEvent = true;
                                    break;
                                case "5"://Ready
                                    this.status = "Ready";
                                    break;
                                case "6"://Ready
                                    this.status = "NotRun";
                                    break;
                                case "7"://Ready
                                    this.status = "Cancelling";
                                    break;
                                case "8"://Canceled
                                    this.status = "Canceled";
                                    this.validEvent = true;
                                    break;
                                default:
                                    //this.validEvent = true;
                                    this.status = pars[i].AxBindVarValue;
                                    break;
                            }
                        }
                        break;
                        

                    default:

                        break;
                }
            }


            ul_recId = recId == null ? new ulong?() : ulong.Parse(recId);
            uint? ui_recVersionOld = recVersionOld == null ? new uint?() : uint.Parse(recVersionOld);
            ui_recVersion = recVersion == null ? new uint?() : uint.Parse(recVersion);

            //se ho un recversion change posso eventualmente recuperare recid 
            if (ui_recVersion.HasValue && ui_recVersionOld.HasValue)
            {
                if (bjversions.ContainsKey(ui_recVersionOld.Value))
                {
                    if (!ul_recId.HasValue)//potrei fare un check
                        ul_recId = bjversions[ui_recVersionOld.Value];

                    bjversions.Remove(ui_recVersionOld.Value);
                    bjversions.Add(ui_recVersion.Value, ul_recId.Value);
                }
                else if (ul_recId.HasValue)
                    if (bjversions.ContainsKey(ui_recVersion.Value))
                        bjversions[ui_recVersion.Value] = ul_recId.Value;
                    else
                        bjversions.Add(ui_recVersion.Value, ul_recId.Value);

                Logging.Log.Instance.Debug("BatchJob with RecId {2} - Change Over: from {1} to {0}", ui_recVersion.Value, ui_recVersionOld.Value, ul_recId);
            }



            if (!ul_recId.HasValue && ui_recVersion.HasValue)
            {
                if (bjversions.ContainsKey(ui_recVersion.Value))
                    ul_recId = bjversions[ui_recVersion.Value];
            }

            if (ul_recId.HasValue)
            {
                if (!bjhistory.ContainsKey(ul_recId.Value))
                    bjhistory.Add(ul_recId.Value, new BatchJobHistory());

                this.recId = ul_recId.ToString();

                bjhistory[ul_recId.Value].status = this.status;
                bjhistory[ul_recId.Value].lastDateTime = DateTime.Now;
                if (ui_recVersion.HasValue)
                {
                    bjhistory[ul_recId.Value].recversion = ui_recVersion.Value;
                    this.recVersion = ui_recVersion.ToString();
                }

                validEvent = true;

                
            }

            if (axSessionId.Value > 2) this.axSessionId = axSessionId.Value.ToString();
            if (axSessionId.Value > 2) this.axUserName = axUserName.ASCII;

            if(validEvent)
                Logging.Log.Instance.Debug("{7}::{6} // BatchJob with RecId {0} RecVersion {1} : {2} // {3}-{4}-{5}", this.recId, this.recVersion, this.status, validEvent, bjhistory.Count, bjversions.Count,
                    axSessionId.Value.ToString(), axUserName.ASCII);
        }

        internal void GetMessage(string axServerName, AxSessionId axSessionId, AxUserName axUserName, long nsTimeStampEpoch)
        {
            if (axSessionId.Value > 2) this.axSessionId = axSessionId.Value.ToString();
            if (axSessionId.Value > 2) this.axUserName = axUserName.ASCII;
            

            if (validEvent)
            {
                Logging.Log.Instance.Debug("ETW{0} {1} {2}",
                    string.Format(",AxInstance={0},AxUser={1},AxSession={2}",
                       axServerName,
                       this.axUserName,
                       this.axSessionId),
                    string.Format("recVersion=\"{0}\",recId=\"{1}\",batchStatus=\"{2}\"", this.recVersion, this.recId, this.status), nsTimeStampEpoch);

                //new DataSend.DataSendInstance().sender.PublishEvent("ETW",
                //       nsTimeStampEpoch,
                //    string.Format("recVersion=\"{0}\",recId=\"{1}\",batchStatus=\"{2}\"", this.status),
                //    string.Format(",AxInstance={0},AxUser={1},AxSession={2}",
                //       axServerName,
                //       this.axUserName,
                //       this.axSessionId)
                //       );
            }
        }
    }

    public class CreateUserSessionEvent
    {
        public long nsTimeStampEpoch { get; set; }
        public string clientType = "";
        public string sessionType = "";
        public bool isBatchType
        {
            get
            {
                return sessionType == "3" && clientType == "WORKER";
            }
        }
        public AxUserName axUserName;
        public string computerName = ""; //preziosissimo...

        //13 --> parent session id (se 0 non c'e'parent)
        //14 --> max session id (di solito ottengo questa+1) 
        public bool isValid { get { return !string.IsNullOrEmpty(clientType); } }
    }
}
