﻿using Microsoft.Diagnostics.Tracing;
using AxETWTracing.Unsafe;
using AxETWTracing.Statistics;

namespace AxETWTracing.Events
{
    internal unsafe class AssemblyCacheElementBegin //event 98 Caching
    {
        internal AssemblyCacheElementBegin(TraceEvent obj)
        {
            this.nsTimeStampEpoch = obj.nsTimeStampEpoch;
            this.processId = obj.ProcessID;

            /*
            */
        }

        public long nsTimeStampEpoch { get; private set; }

        public int processId { get; private set; }
    }

    internal unsafe class AssemblyCacheElement //event 99 Caching
    {
        internal AssemblyCacheElement(TraceEvent obj)
        {
            this.nsTimeStampEpoch = obj.nsTimeStampEpoch;
            this.processId = obj.ProcessID;

            /*
                 <template tid="PerformanceTask99Args">
                  <data name="Element" inType="win:UnicodeString"/>
                 </template>
            */

            byte[] payload = obj.EventData();
            int offset = 0;
            this.Element = RawReader.ReadUnicodeString(payload, ref offset);//ottimizzare con lettura???
        }

        public long nsTimeStampEpoch { get; private set; }

        public int processId { get; private set; }

        public string Element { get; private set; }
    }

    internal unsafe class AssemblyCacheElementEnd //event 100 Caching
    {
        internal AssemblyCacheElementEnd(TraceEvent obj)
        {
            this.nsTimeStampEpoch = obj.nsTimeStampEpoch;
            this.processId = obj.ProcessID;

            /*
            */
        }

        public long nsTimeStampEpoch { get; private set; }

        public int processId { get; private set; }
    }
}