﻿using Microsoft.Diagnostics.Tracing;
using AxETWTracing.Unsafe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AxETWTracing.Statistics;

namespace AxETWTracing.Events
{
    internal unsafe class XppMethodBegin
    {
        internal XppMethodBegin(TraceEvent obj)
        {
            this.nsTimeStampEpoch = obj.nsTimeStampEpoch;

            /*
             <template tid="PerformanceTaskStart56Args">
              <data name="seqNum" inType="win:Int32"/>
              <data name="AxServerName" inType="win:UnicodeString"/>
              <data name="AxUserName" inType="win:UnicodeString"/>
              <data name="AxSessionId" inType="win:Int32"/>
              <data name="AxRC" inType="win:Int32"/>
              <data name="AxXppClassName" inType="win:UnicodeString"/>
              <data name="AxXppMethodName" inType="win:UnicodeString"/>
              <data name="AxFormPath" inType="win:UnicodeString"/>
              <data name="AxNestLevel" inType="win:Int64"/>
              <data name="AxXppTargetType" inType="win:UnicodeString"/>
             </template>
            */

            byte[] payload = obj.EventData();
            this.processId = obj.ProcessID;

            int offset = 0;
            this.seqNum = RawReader.ReadInt32(payload, offset); offset += 4;
            this.AxServerName = RawReader.ReadUnicodeString(payload, ref offset);//ottimizzare con lettura???
            this.AxUserName = new AxUserName() { Value = RawReader.ReadUnicodeString(payload, ref offset) };
            this.AxSessionId = new AxSessionId() { Value = RawReader.ReadInt32(payload, offset) };
        }
        public long nsTimeStampEpoch { get; private set; }

        public int seqNum { get; private set; }//da eliminare

        public int processId { get; private set; }
        public string AxServerName { get; private set; }
        public AxUserName AxUserName { get; private set; }
        public AxSessionId AxSessionId { get; private set; }

        private void ReadIsBatch(byte[] payload, int offset)
        {
            int tmpOffset = offset;
            if (RawReader.TestUnicodeString(payload, ref tmpOffset, "BatchRun\0"))
            {
                if (RawReader.TestUnicodeString(payload, ref tmpOffset, "runJobStatic\0"))
                {
                    //Logging.Log.Instance.Debug("BatchRun::runJobStatic classEVO " + obj.ID + " |" + obj.EventDataLength + " |" + _AxInstanceUserSession.Session.Value);
                    this.isBatchRun_runJobStatic = true;
                }
            }
        }
        public bool isBatchRun_runJobStatic { get; private set; }

        public override string ToString()
        {
            return string.Format("{0} -> {2} {1}", seqNum, AxUserName, AxSessionId);
        }
    }
    internal unsafe class XppMethodEnd
    {
        internal XppMethodEnd(TraceEvent obj)
        {
            this.nsTimeStampEpoch = obj.nsTimeStampEpoch;

            /*
             <template tid="PerformanceTaskStart56Args">
              <data name="seqNum" inType="win:Int32"/>
              <data name="AxServerName" inType="win:UnicodeString"/>
              <data name="AxUserName" inType="win:UnicodeString"/>
              <data name="AxSessionId" inType="win:Int32"/>
              <data name="AxRC" inType="win:Int32"/>
              <data name="AxXppClassName" inType="win:UnicodeString"/>
              <data name="AxXppMethodName" inType="win:UnicodeString"/>
              <data name="AxFormPath" inType="win:UnicodeString"/>
              <data name="AxNestLevel" inType="win:Int64"/>
              <data name="AxXppTargetType" inType="win:UnicodeString"/>
             </template>
            */

            byte[] payload = obj.EventData();
            this.processId = obj.ProcessID;

            int offset = 0;
            this.seqNum = RawReader.ReadInt32(payload, offset); offset += 4;
            this.AxServerName = RawReader.ReadUnicodeString(payload, ref offset);//ottimizzare con lettura???
            this.AxUserName = new AxUserName() { Value = RawReader.ReadUnicodeString(payload, ref offset) };
            this.AxSessionId = new AxSessionId() { Value = RawReader.ReadInt32(payload, offset) };
        }
        public long nsTimeStampEpoch { get; private set; }

        public int seqNum { get; private set; }//da eliminare

        public int processId { get; private set; }
        public string AxServerName { get; private set; }
        public AxUserName AxUserName { get; private set; }
        public AxSessionId AxSessionId { get; private set; }

        private void ReadIsBatch(byte[] payload, int offset)
        {
            int tmpOffset = offset;
            if (RawReader.TestUnicodeString(payload, ref tmpOffset, "BatchRun\0"))
            {
                if (RawReader.TestUnicodeString(payload, ref tmpOffset, "runJobStatic\0"))
                {
                    //Logging.Log.Instance.Debug("BatchRun::runJobStatic classEVO " + obj.ID + " |" + obj.EventDataLength + " |" + _AxInstanceUserSession.Session.Value);
                    this.isBatchRun_runJobStatic = true;
                }
            }
        }
        public bool isBatchRun_runJobStatic { get; private set; }

        public override string ToString()
        {
            return string.Format("{0} -> {2} {1}", seqNum, AxUserName, AxSessionId);
        }
    }
}
