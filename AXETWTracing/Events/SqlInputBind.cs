﻿using Microsoft.Diagnostics.Tracing;
using AxETWTracing.Unsafe;
using AxETWTracing.Statistics;

namespace AxETWTracing.Events
{
    internal unsafe class SqlInputBind
    {
        internal SqlInputBind(TraceEvent obj)
        {
            this.nsTimeStampEpoch = obj.nsTimeStampEpoch;
            this.processId = obj.ProcessID;

            /*
            SqlInputBind    65  BindParameter
             <template tid="PerformanceTask65Args">
              <data name="seqNum" inType="win:Int32"/>
              <data name="AxServerName" inType="win:UnicodeString"/>
              <data name="AxUserName" inType="win:UnicodeString"/>
              <data name="AxSessionId" inType="win:Int32"/>
              <data name="AxRC" inType="win:Int32"/>
              <data name="AxConDBSpid" inType="win:Int64"/>
              <data name="AxBindColId" inType="win:Int16"/>
              <data name="AxBindVarLength" inType="win:Int32"/>
              <data name="AxBindVarType" inType="win:Int16"/>
              <data name="AxBindVarValue" inType="win:UnicodeString"/>
             </template>
            */

            byte[] payload = obj.EventData();

            int offset = 0;
            this.seqNum = RawReader.ReadInt32(payload, offset); offset += 4;
            this.AxServerName = RawReader.ReadUnicodeString(payload, ref offset);//ottimizzare con lettura???
            this.AxUserName = new AxUserName() { Value = RawReader.ReadUnicodeString(payload, ref offset) };
            this.AxSessionId = new AxSessionId() { Value = RawReader.ReadInt32(payload, offset) }; offset += 8;
            this.AxConDBSpid = new AxConDBSpid() { Value = RawReader.ReadInt64(payload, offset) }; offset += 8;
            this.AxBindColId = RawReader.ReadInt16(payload, offset); offset += 2;
            this.AxBindVarLength = RawReader.ReadInt32(payload, offset); offset += 4;
            this.AxBindVarType = RawReader.ReadInt16(payload, offset); offset += 2;
            this.AxBindVarValue = RawReader.ReadUnicodeString(payload, ref offset);

        }

        public long nsTimeStampEpoch { get; private set; }

        public int seqNum { get; private set; }//da eliminare

        public int processId { get; private set; }
        public string AxServerName { get; private set; }
        public AxUserName AxUserName { get; private set; }
        public AxSessionId AxSessionId { get; private set; }
        public AxConDBSpid AxConDBSpid { get; private set; }
        
        public short AxBindColId { get; private set; }
        public int AxBindVarLength { get; private set; }
        public short AxBindVarType { get; private set; }
        public string AxBindVarValue { get; private set; }

        public override string ToString()
        {
            return string.Format("{0} -> {2} {1} // {3}", seqNum, AxUserName, AxSessionId, AxConDBSpid);
        }
    }
}
