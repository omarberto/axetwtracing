﻿using Microsoft.Diagnostics.Tracing;
using AxETWTracing.Unsafe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxETWTracing.Events
{
    internal class ModelStoredProc : AxSqlStmtEvent
    {
        internal ModelStoredProc(TraceEvent obj) : base(obj)
        {

        }
    }
}
