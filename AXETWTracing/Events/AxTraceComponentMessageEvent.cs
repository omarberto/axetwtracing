﻿using Microsoft.Diagnostics.Tracing;
using AxETWTracing.Unsafe;
using AxETWTracing.Statistics;

namespace AxETWTracing.Events
{
    internal unsafe class AxTraceComponentMessageEvent //event 68 TraceInfo
    {

        internal AxTraceComponentMessageEvent(TraceEvent obj)
        {
            this.nsTimeStampEpoch = obj.nsTimeStampEpoch;
            this.processId = obj.ProcessID;

            /*
             <template tid="AxAppTask68Args">
              <data name="seqNum" inType="win:Int32"/>
              <data name="AxServerName" inType="win:UnicodeString"/>
              <data name="AxUserName" inType="win:UnicodeString"/>
              <data name="AxSessionId" inType="win:Int32"/>
              <data name="AxComponent" inType="win:UnicodeString"/>
              <data name="AxMsg" inType="win:UnicodeString"/>
             </template>
            */
            
            byte[] payload = obj.EventData();
            int offset = 0;
            this.seqNum = RawReader.ReadInt32(payload, offset); offset += 4;
            this.AxServerName = RawReader.ReadUnicodeString(payload, ref offset);//ottimizzare con lettura???
            this.AxUserName = new AxUserName() { Value = RawReader.ReadUnicodeString(payload, ref offset) };
            this.AxSessionId = new AxSessionId() { Value = RawReader.ReadInt32(payload, offset) }; offset += 4;
            this.AxComponent = RawReader.ReadUnicodeString(payload, ref offset);
            this.AxMsg = RawReader.ReadUnicodeString(payload, ref offset);
        }

        public long nsTimeStampEpoch { get; private set; }

        public int seqNum { get; private set; }//da eliminare

        public int processId { get; private set; }
        public string AxServerName { get; private set; }
        public AxUserName AxUserName { get; private set; }
        public AxSessionId AxSessionId { get; private set; }

        public string AxComponent { get; private set; }
        public string AxMsg { get; private set; }
    }
}
