﻿using Microsoft.Diagnostics.Tracing;
using AxETWTracing.Unsafe;
using AxETWTracing.Statistics;

namespace AxETWTracing.Events
{
    internal unsafe class DataAccessSCSCCaching //event 34 Caching
    {
        internal DataAccessSCSCCaching(TraceEvent obj)
        {
            this.nsTimeStampEpoch = obj.nsTimeStampEpoch;
            this.processId = obj.ProcessID;

            /*
             <template tid="DataAccessTaskArgs">
              <data name="seqNum" inType="win:Int32"/>
              <data name="AxServerName" inType="win:UnicodeString"/>
              <data name="AxUserName" inType="win:UnicodeString"/>
              <data name="AxSessionId" inType="win:Int32"/>
              <data name="EventName" inType="win:UnicodeString"/>
              <data name="RootTableId" inType="win:Int64"/>
              <data name="RootTableName" inType="win:UnicodeString"/>
              <data name="RecId" inType="win:Int64"/>
             </template>
            */


            byte[] payload = obj.EventData();
            int offset = 0;
            this.seqNum = RawReader.ReadInt32(payload, offset); offset += 4;
            this.AxServerName = RawReader.ReadUnicodeString(payload, ref offset);//ottimizzare con lettura???
            this.AxUserName = new AxUserName() { Value = RawReader.ReadUnicodeString(payload, ref offset) };
            this.AxSessionId = new AxSessionId() { Value = RawReader.ReadInt32(payload, offset) }; offset += 4;
            this.EventName = RawReader.ReadUnicodeString(payload, ref offset);
            this.RootTableId = RawReader.ReadInt64(payload, offset); offset += 8;
            this.RootTableName = RawReader.ReadUnicodeString(payload, ref offset);
            this.RecId = RawReader.ReadInt64(payload, offset);

            //capire cosa ha di diverso questo evento
        }

        public long nsTimeStampEpoch { get; private set; }

        public int seqNum { get; private set; }//da eliminare

        public int processId { get; private set; }
        public string AxServerName { get; private set; }
        public AxUserName AxUserName { get; private set; }
        public AxSessionId AxSessionId { get; private set; }

        public string EventName { get; private set; }
        public long RootTableId { get; private set; }
        public string RootTableName { get; private set; }
        public long RecId { get; private set; }
    }
}
