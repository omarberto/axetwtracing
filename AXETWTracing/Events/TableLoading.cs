﻿using Microsoft.Diagnostics.Tracing;
using AxETWTracing.Unsafe;
using AxETWTracing.Statistics;

namespace AxETWTracing.Events
{
    internal unsafe class TableLoading //event 12 Caching,AOT only client???
    {

        internal TableLoading(TraceEvent obj)
        {
            this.nsTimeStampEpoch = obj.nsTimeStampEpoch;
            this.processId = obj.ProcessID;

            /*
                <template tid="AOTTaskArgs">
                <data name="seqNum" inType="win:Int32"/>
                <data name="AxServerName" inType="win:UnicodeString"/>
                <data name="AxUserName" inType="win:UnicodeString"/>
                <data name="AxSessionId" inType="win:Int32"/>
                <data name="AxTableId" inType="win:Int64"/>
                <data name="AxTableName" inType="win:UnicodeString"/>
                <data name="AxLoadingStrategy" inType="win:UnicodeString"/>
                <data name="AxRequestType" inType="win:UnicodeString"/>
                <data name="AxUtilMode" inType="win:Int32"/>
                <data name="AxMethodList" inType="win:UnicodeString"/>
                <data name="AxUCBufferSize" inType="win:Int64"/>
                <data name="AxCBufferSize" inType="win:Int64"/>
                </template>
            */

            byte[] payload = obj.EventData();
            int offset = 0;
            this.seqNum = RawReader.ReadInt32(payload, offset); offset += 4;
            this.AxServerName = RawReader.ReadUnicodeString(payload, ref offset);//ottimizzare con lettura???
            this.AxUserName = new AxUserName() { Value = RawReader.ReadUnicodeString(payload, ref offset) };
            this.AxSessionId = new AxSessionId() { Value = RawReader.ReadInt32(payload, offset) }; offset += 4;
            this.AxTableId = RawReader.ReadInt64(payload, offset); offset += 8;
            this.AxTableName = RawReader.ReadUnicodeString(payload, ref offset);
            this.AxLoadingStrategy = RawReader.ReadUnicodeString(payload, ref offset);
            this.AxRequestType = RawReader.ReadUnicodeString(payload, ref offset);
            this.AxUtilMode = RawReader.ReadInt32(payload, offset); offset += 4;
            this.AxMethodList = RawReader.ReadUnicodeString(payload, ref offset);
            this.AxUCBufferSize = RawReader.ReadInt64(payload, offset); offset += 8;
            this.AxCBufferSize = RawReader.ReadInt64(payload, offset);
        }

        public long nsTimeStampEpoch { get; private set; }

        public int seqNum { get; private set; }//da eliminare

        public int processId { get; private set; }
        public string AxServerName { get; private set; }
        public AxUserName AxUserName { get; private set; }
        public AxSessionId AxSessionId { get; private set; }

        public long AxTableId { get; private set; }
        public string AxTableName { get; private set; }
        public string AxLoadingStrategy { get; private set; }
        public string AxRequestType { get; private set; }
        public int AxUtilMode { get; private set; }
        public string AxMethodList { get; private set; }
        public long AxUCBufferSize { get; private set; }
        public long AxCBufferSize { get; private set; }
    }
}
