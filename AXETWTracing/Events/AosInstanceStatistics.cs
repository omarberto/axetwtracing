﻿using System;
using System.Linq;
using AxETWTracing.Statistics;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;

namespace AxETWTracing.Events
{
    internal class AosInstanceStatistics
    {
        private long intervalStartTimeStamp = 0;
        private long intervalEndTimeStamp = 0;

        public AosInstanceStatistics(Statistics.AxServerInstance service)
        {
            this.service = service;
        }

        public AxServerInstance service { get; private set; }
        public int SessionsCount { get { return _sessions.Count; } }
        

        Dictionary<AxSessionId, AxSessionData> _sessions = new Dictionary<AxSessionId, AxSessionData>();
        private AxSessionData getSession(AxSessionId AxSessionId, AxUserName AxUserName, long tracingMessageTimeStamp)
        {

            if (!_sessions.ContainsKey(AxSessionId))
            {
                this.intervalStartTimeStamp = (tracingMessageTimeStamp / 60000000000L) * 60000000000L;
                this.intervalEndTimeStamp = this.intervalStartTimeStamp + 60000000000L;
                _sessions[AxSessionId] = new AxSessionData(AxUserName, tracingMessageTimeStamp);
            }
            else if(!_sessions[AxSessionId].checkUser(AxUserName))
            {
                //utente cambiato

                //invio statistiche
                Logging.Log.Instance.Debug("checkUser {0}", AxSessionId.Value);
                _sessions[AxSessionId].Close(service, AxSessionId, tracingMessageTimeStamp);

                //aggiorno sessione
                this.intervalStartTimeStamp = (tracingMessageTimeStamp / 60000000000L) * 60000000000L;
                this.intervalEndTimeStamp = this.intervalStartTimeStamp + 60000000000L;
                _sessions[AxSessionId] = new AxSessionData(AxUserName, tracingMessageTimeStamp);
            }

            //questo vale per tutti
            _sessions[AxSessionId].nsTimeStampEpochLast = intervalEndTimeStamp;
            //qui devo mandare tutto!!!!!

            if (this.intervalEndTimeStamp < tracingMessageTimeStamp)
            {
                SendStatistics(AxSessionId, tracingMessageTimeStamp);
                
                this.intervalStartTimeStamp = this.intervalStartTimeStamp + 60000000000L;
                this.intervalEndTimeStamp = this.intervalEndTimeStamp + 60000000000L;
            }

            return _sessions[AxSessionId];
        }
        private void SendStatistics(AxSessionId AxSessionId, long tracingMessageTimeStamp)
        {
            List<AxSessionId> closedSessions = new List<AxSessionId>();
            //Logging.Log.Instance.Debug("nr sessioni attive {0}", _sessions.Count);
            for (int i = 0; i < _sessions.Count; i++)
            {
                var tmp = _sessions.ElementAt(i);
                bool sessionClosed = tmp.Value.Next(service, tmp.Key, this.intervalStartTimeStamp);
                if (sessionClosed && !tmp.Key.Equals(AxSessionId)) closedSessions.Add(tmp.Key);
                //Logging.Log.Instance.Debug("cycle session {0} closed: {1}", tmp.Key.Value, sessionClosed);
            }
            foreach (AxSessionId closedSession in closedSessions)
                _sessions.Remove(closedSession);

            //new DataSend.DataSendInstance().sender.PublishEvent("HEARTBEAT", tracingMessageTimeStamp,
            //        string.Format("servers={0}i,RPC={4}i,SQL={5}i,MDL={6}i,FTC={7}i"
            //        , AosInstances.instance.Count, 
            //        0, 0, 0
            //        , AosInstances.instance.RPC_stmt_count_cumu//non distiguo per istanza
            //        , AosInstances.instance.SQL_stmt_count_cumu//non distiguo per istanza
            //        , AosInstances.instance.MDL_stmt_count_cumu//non distiguo per istanza
            //        , AosInstances.instance.FTC_stmt_count_cumu//non distiguo per istanza
            //        ),
            //        string.Format(",AxServer={0}", Environment.MachineName));
        }

        public void synchroSession(long tracingMessageTimeStamp, long deltaTimeEpoch)
        {
            //se il nuovo intervallo è nel futuro, invio tutto e correggo
            if (this.intervalEndTimeStamp < tracingMessageTimeStamp)
            {
                SendStatistics(tracingMessageTimeStamp, deltaTimeEpoch);

                this.intervalStartTimeStamp = (tracingMessageTimeStamp / 60000000000L) * 60000000000L;

                Logging.Log.Instance.Warn("synchroSession future EpochNanoseconds.UtcNow.Value: {0} intervalEndTimeStamp:  {1} -> {2} timeDelta: {3}",
                    EpochNanoseconds.UtcNow.Value, intervalEndTimeStamp, this.intervalStartTimeStamp + 60000000000L, deltaTimeEpoch);

                this.intervalEndTimeStamp = this.intervalStartTimeStamp + 60000000000L;

            }
            //se invece il nuovo intervallo è nel passato, invio tutto e correggo (a far bene, non devo inviare, tanto sovrascrive....
            else if (tracingMessageTimeStamp < this.intervalStartTimeStamp)
            {
                SendStatistics(tracingMessageTimeStamp, deltaTimeEpoch);

                this.intervalStartTimeStamp = (tracingMessageTimeStamp / 60000000000L) * 60000000000L;

                Logging.Log.Instance.Warn("synchroSession past EpochNanoseconds.UtcNow.Value: {0} intervalEndTimeStamp:  {1} -> {2} timeDelta: {3}",
                    EpochNanoseconds.UtcNow.Value, intervalEndTimeStamp, this.intervalStartTimeStamp + 60000000000L, deltaTimeEpoch);

                this.intervalEndTimeStamp = this.intervalStartTimeStamp + 60000000000L;
            }
            //se invece resto nelíntervallo attuale coreeggo ma non faccio il send
            else
            {
                CorrectStatistics(deltaTimeEpoch);

                Logging.Log.Instance.Warn("synchroSession correction EpochNanoseconds.UtcNow.Value: {0} intervalEndTimeStamp:  {1} timeDelta: 2}",
                    EpochNanoseconds.UtcNow.Value, intervalEndTimeStamp, deltaTimeEpoch);
            }

        }
        private void SendStatistics(long tracingMessageTimeStamp, long deltaTimeEpoch)
        {
            List<AxSessionId> closedSessions = new List<AxSessionId>();
            //Logging.Log.Instance.Debug("nr sessioni attive {0}", _sessions.Count);
            for (int i = 0; i < _sessions.Count; i++)
            {
                var tmp = _sessions.ElementAt(i);
                bool sessionClosed = tmp.Value.Next(service, tmp.Key, this.intervalStartTimeStamp, deltaTimeEpoch);
                if (sessionClosed) closedSessions.Add(tmp.Key);
                //Logging.Log.Instance.Debug("cycle session {0} closed: {1}", tmp.Key.Value, sessionClosed);
            }
            foreach (AxSessionId closedSession in closedSessions)
                _sessions.Remove(closedSession);
        }
        private void CorrectStatistics(long deltaTimeEpoch)
        {
            for (int i = 0; i < _sessions.Count; i++)
            {
                var tmp = _sessions.ElementAt(i);
                tmp.Value.Correct(service, tmp.Key, deltaTimeEpoch);
            }
        }



        internal void addSessionData(AxSessionId AxSessionId, CreateUserSessionEvent data)
        {
            AxSessionData sessionData = getSession(AxSessionId, data.axUserName, data.nsTimeStampEpoch);
            sessionData.clientType = data.clientType;
            sessionData.computerName = data.computerName;
            sessionData.nsTimeStampEpochStart = sessionData.nsTimeStampEpochLast = data.nsTimeStampEpoch;
            new DataSend.DataSendInstance().sender.PublishBeginSession(service, AxSessionId, _sessions[AxSessionId].User,
                _sessions[AxSessionId].clientType,
                _sessions[AxSessionId].computerName,
                data.nsTimeStampEpoch
                );
            Logging.Log.Instance.Debug("addSessionData {1} :: {0} // {2} {3} {4}", AxSessionId.Value, data.axUserName.ASCII, data.nsTimeStampEpoch, sessionData.nsTimeStampEpochStart, sessionData.nsTimeStampEpochLast);
        }
        internal void endSession(AxSessionId AxSessionId, long nsMessageTimeStamp)
        {
            Logging.Log.Instance.Debug("endSession removing :: {0}", AxSessionId.Value);
            if (_sessions.ContainsKey(AxSessionId))
            {
                string s = _sessions[AxSessionId].clientType;
                Logging.Log.Instance.Debug("addSessionData removed type {1} :: {0}", AxSessionId.Value, s);
                if (string.IsNullOrEmpty(_sessions[AxSessionId].clientType))
                {
                    new DataSend.DataSendInstance().sender.PublishEndSession(service, AxSessionId, _sessions[AxSessionId].User,
                        _sessions[AxSessionId].clientType,
                        _sessions[AxSessionId].computerName,
                        (nsMessageTimeStamp - _sessions[AxSessionId].nsTimeStampEpochStart),
                        nsMessageTimeStamp
                        );
                    _sessions[AxSessionId].clientType = "";
                    _sessions[AxSessionId].computerName = "";
                    _sessions[AxSessionId].computerName = "";
                    _sessions[AxSessionId].nsTimeStampEpochStart = _sessions[AxSessionId].nsTimeStampEpochLast = 0;
                }
                //_sessions.Remove(AxSessionId);
            }
        }

        internal void addBatchStart(AxSessionId AxSessionId, AxUserName AxUserName, long nsStartDateTime, string status, string recId)
        {
            AxSessionData sessionData = getSession(AxSessionId, AxUserName, nsStartDateTime);
            sessionData.batchRecId = recId;
            sessionData.nsTimeStampBatchStart = nsStartDateTime;
            sessionData.batchStatus = status;
            Logging.Log.Instance.Debug("addBatchStart {1} :: {0} ({2})", AxSessionId.Value, AxUserName.ASCII, recId);

            new DataSend.DataSendInstance().sender.PublishBatchBegin
                (
                service
                , AxSessionId
                , AxUserName
                , nsStartDateTime
                , recId
                );
        }

        internal void addBatchEnd(AxSessionId AxSessionId, AxUserName AxUserName, long nsStartDateTime, string recId, string status, long nsEndDateTime)
        {
            AxSessionData sessionData = getSession(AxSessionId, AxUserName, nsStartDateTime);
            if (!sessionData.nsTimeStampBatchStart.HasValue)
            {
                new DataSend.DataSendInstance().sender.PublishBatchRecovery
                    (
                    service
                    , AxSessionId
                    , AxUserName
                    , nsStartDateTime
                    , nsEndDateTime
                    , recId
                    , status
                    );
            }
            else
            {
                new DataSend.DataSendInstance().sender.PublishBatchEnd
                    (
                    service
                    , AxSessionId
                    , AxUserName
                    , sessionData.nsTimeStampBatchStart.Value
                    , nsEndDateTime
                    , recId
                    , status
                    );


                sessionData.batchRecId = "";
                sessionData.nsTimeStampBatchStart = new long?();
                sessionData.nsTimeStampBatchEnd = new long?();
                sessionData.batchStatus = "";
            }
            
            Logging.Log.Instance.Debug("addBatchEnd {1} :: {0} ({2})", AxSessionId.Value, AxUserName.ASCII, recId);
        }

        internal void addEvent(CSRoundTripBegin csroundtripBegin)
        {
            AxSessionData sessionData = getSession(csroundtripBegin.AxSessionId, csroundtripBegin.AxUserName, csroundtripBegin.nsTimeStampEpoch);
            sessionData.RPCstatistics.addBeginEvent(csroundtripBegin);
        }


        internal void addEvent(CSRoundTripEnd csroundtripEnd)
        {
            AxSessionData sessionData = getSession(csroundtripEnd.AxSessionId, csroundtripEnd.AxUserName, csroundtripEnd.nsTimeStampEpoch);
            sessionData.RPCstatistics.addEndEvent(csroundtripEnd);
        }

        internal void addEvent(XppMethodBegin xppmethodBegin)
        {
            AxSessionData sessionData = getSession(xppmethodBegin.AxSessionId, xppmethodBegin.AxUserName, xppmethodBegin.nsTimeStampEpoch);
            sessionData.XPPstatistics.addBeginEvent(xppmethodBegin);
        }
        internal void addEvent(XppMethodEnd xppmethodBegin)
        {
            AxSessionData sessionData = getSession(xppmethodBegin.AxSessionId, xppmethodBegin.AxUserName, xppmethodBegin.nsTimeStampEpoch);
            sessionData.XPPstatistics.addEndEvent(xppmethodBegin);
        }

        internal void addEvent(AxSqlStmtEvent axSqlStmtEvent)
        {
            AxSessionData sessionData = getSession(axSqlStmtEvent.AxSessionId, axSqlStmtEvent.AxUserName, axSqlStmtEvent.nsTimeStampEpoch);
            sessionData.SQLstatistics.addEvent(axSqlStmtEvent);
        }
        internal void setSQLstmtType(AxSessionId AxSessionId, SqlStmtParser.StmtType stmtType)
        {
            if (_sessions.ContainsKey(AxSessionId))
            {
                _sessions[AxSessionId].SQLstatistics.addEventType(stmtType);
            }
        }
        internal void addEvent(ModelStoredProc modelStoredProc)
        {
            AxSessionData sessionData = getSession(modelStoredProc.AxSessionId, modelStoredProc.AxUserName, modelStoredProc.nsTimeStampEpoch);
            sessionData.MODELSTOREstatistics.addEvent(modelStoredProc);
        }

        internal void addEvent(SqlRowFetch sqlRowFetch)
        {
            AxSessionData sessionData = getSession(sqlRowFetch.AxSessionId, sqlRowFetch.AxUserName, sqlRowFetch.nsTimeStampEpoch);
            sessionData.FETCHstatistics.addEvent(sqlRowFetch);
        }
        internal void addEvent(SqlRowFetchCumu sqlRowFetchCumu)
        {
            AxSessionData sessionData = getSession(sqlRowFetchCumu.AxSessionId, sqlRowFetchCumu.AxUserName, sqlRowFetchCumu.nsTimeStampEpoch);
            sessionData.FETCHstatistics.addEvent(sqlRowFetchCumu);
        }

        private Dictionary<int, Dictionary<string, long>> axtransactions = new Dictionary<int, Dictionary<string, long>>();//poi gestiremo chiusura in base a chiusura processi client...
        internal void pushEvent(AxTransactionBeginEvent axTransactionBeginEvent)
        {
            new DataSend.DataSendInstance().sender.PublishEvent("ETW",
                   axTransactionBeginEvent.nsTimeStampEpoch,
                string.Format("AxTransactionName=\"{0}\",AxTransactionType=\"Begin\"",
                   axTransactionBeginEvent.AxMarkerName),
                string.Format(",AxInstance={0},AxUser={1},AxSession={2}",
                   service.Value,
                   axTransactionBeginEvent.AxUserName.ASCII,
                   axTransactionBeginEvent.AxSessionId.Value)
                   );



            if (AX2012ETWtracing.Configuration.Configuration.config.ax2012.traceAxTransitions)
            {
                if (!axtransactions.ContainsKey(axTransactionBeginEvent.AxSessionId.Value))
                    axtransactions.Add(axTransactionBeginEvent.AxSessionId.Value, new Dictionary<string, long>());
                if (!axtransactions[axTransactionBeginEvent.AxSessionId.Value].ContainsKey(axTransactionBeginEvent.AxMarkerName))
                    axtransactions[axTransactionBeginEvent.AxSessionId.Value].Add(axTransactionBeginEvent.AxMarkerName, axTransactionBeginEvent.nsTimeStampEpoch);
                else
                    axtransactions[axTransactionBeginEvent.AxSessionId.Value][axTransactionBeginEvent.AxMarkerName] = axTransactionBeginEvent.nsTimeStampEpoch;
            }
        }

        internal void pushEvent(AxTransactionEndEvent axTransactionEndEvent)
        {
            new DataSend.DataSendInstance().sender.PublishEvent("ETW",
                   axTransactionEndEvent.nsTimeStampEpoch,
                string.Format("AxTransactionName=\"{0}\",AxTransactionType=\"End\"",
                   axTransactionEndEvent.AxMarkerName),
                string.Format(",AxInstance={0},AxUser={1},AxSession={2}",
                   service.Value,
                   axTransactionEndEvent.AxUserName.ASCII,
                   axTransactionEndEvent.AxSessionId.Value)
                   );

            if (AX2012ETWtracing.Configuration.Configuration.config.ax2012.traceAxTransitions)
            {
                if (axtransactions.ContainsKey(axTransactionEndEvent.AxSessionId.Value))
                {
                    if (axtransactions[axTransactionEndEvent.AxSessionId.Value].ContainsKey(axTransactionEndEvent.AxMarkerName))
                    {
                        long duration = axTransactionEndEvent.nsTimeStampEpoch - axtransactions[axTransactionEndEvent.AxSessionId.Value][axTransactionEndEvent.AxMarkerName];

                        new DataSend.DataSendInstance().sender.PublishEvent("AOS_AxTransactions", axTransactionEndEvent.nsTimeStampEpoch,
                            string.Format("duration={1}i",
                                duration
                                ),//se l'utente apre due clients in contemporanea potrei avere delle stranezze
                            string.Format(",AxTransactionName={0},AxUser={1},AxInstance={2},AxSession={3}",
                                axTransactionEndEvent.AxMarkerName,
                                axTransactionEndEvent.AxUserName.ASCII, service.Value, axTransactionEndEvent.AxSessionId.Value));

                        axtransactions[axTransactionEndEvent.AxSessionId.Value].Remove(axTransactionEndEvent.AxMarkerName);
                        if (axtransactions[axTransactionEndEvent.AxSessionId.Value].Count == 0)
                            axtransactions.Remove(axTransactionEndEvent.AxSessionId.Value);
                    }
                }
            }
        }

        internal void pushEvent(AxTraceMessageEvent axTraceMessageEvent)
        {
            new DataSend.DataSendInstance().sender.PublishEvent("ETW",
                   axTraceMessageEvent.nsTimeStampEpoch,
                string.Format("Message=\"{0}\"",
                   axTraceMessageEvent.AxMsg),
                string.Format(",AxInstance={0},AxUser={1},AxSession={2}",
                   service.Value,
                   axTraceMessageEvent.AxUserName.ASCII,
                   axTraceMessageEvent.AxSessionId.Value)
                   );
        }

        internal void pushEvent(AxTraceComponentMessageEvent axTraceComponentMessageEvent)
        {
            new DataSend.DataSendInstance().sender.PublishEvent("ETW",
                   axTraceComponentMessageEvent.nsTimeStampEpoch,
                string.Format("Message=\"{0}::{1}\"",
                   axTraceComponentMessageEvent.AxComponent, axTraceComponentMessageEvent.AxMsg),
                string.Format(",AxInstance={0},AxUser={1},AxSession={2}",
                   service.Value,
                   axTraceComponentMessageEvent.AxUserName.ASCII,
                   axTraceComponentMessageEvent.AxSessionId.Value)
                   );
        }
    }
}