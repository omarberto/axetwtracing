﻿/*
SecurityWebEntryPointAuthorization
44
EP, AOT, Security
SecurityWebUserRoles
45
EP, AOT, Security
WebFrameworkException
46
EP

     <template tid="SecurityTask44Args">
      <data name="AxServerName" inType="win:UnicodeString"/>
      <data name="AxWebServerName" inType="win:UnicodeString"/>
      <data name="AxUserName" inType="win:UnicodeString"/>
      <data name="HttpSessionId" inType="win:UnicodeString"/>
      <data name="AxSessionId" inType="win:Int32"/>
      <data name="MenuItemName" inType="win:UnicodeString"/>
      <data name="ModuleName" inType="win:UnicodeString"/>
      <data name="PageName" inType="win:UnicodeString"/>
      <data name="EntryPointType" inType="win:UnicodeString"/>
      <data name="EntryPointName" inType="win:UnicodeString"/>
      <data name="IsPostback" inType="win:Boolean"/>
      <data name="AccessGranted" inType="win:Boolean"/>
     </template>
     <template tid="SecurityTask45Args">
      <data name="AxServerName" inType="win:UnicodeString"/>
      <data name="AxWebServerName" inType="win:UnicodeString"/>
      <data name="AxUserName" inType="win:UnicodeString"/>
      <data name="HttpSessionId" inType="win:UnicodeString"/>
      <data name="AxSessionId" inType="win:Int32"/>
      <data name="MenuItemName" inType="win:UnicodeString"/>
      <data name="ModuleName" inType="win:UnicodeString"/>
      <data name="PageName" inType="win:UnicodeString"/>
      <data name="Roles" inType="win:UnicodeString"/>
      <data name="IsPostback" inType="win:Boolean"/>
     </template>
     <template tid="EventsrelatedtotheEnterprisePortalframework.Args">
      <data name="AxWebServerName" inType="win:UnicodeString"/>
      <data name="AxUserName" inType="win:UnicodeString"/>
      <data name="HttpSessionId" inType="win:UnicodeString"/>
      <data name="AxSessionId" inType="win:Int32"/>
      <data name="MenuItemName" inType="win:UnicodeString"/>
      <data name="ModuleName" inType="win:UnicodeString"/>
      <data name="PageName" inType="win:UnicodeString"/>
      <data name="Message" inType="win:UnicodeString"/>
      <data name="Exception" inType="win:UnicodeString"/>
      <data name="IsPostback" inType="win:Boolean"/>
     </template>
*/