﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxETWTracing.Events
{
    internal class TimeSynchro
    {
        private static long intervalEndTimeStamp = 0;
        private static long timeDelta = 0;

        private static long intervalLength = AX2012ETWtracing.Configuration.Configuration.config.agent.synchroIntervalLengthNS;
        //private static long marginIntervalLength = 6000000000L;

        internal static bool Check(long tracingMessageTimeStamp)
        {
            if (TimeSynchro.intervalEndTimeStamp == 0)
                TimeSynchro.intervalEndTimeStamp = ((tracingMessageTimeStamp / intervalLength) * intervalLength) + intervalLength;
            else if (TimeSynchro.intervalEndTimeStamp < tracingMessageTimeStamp)
            {
                TimeSynchro.intervalEndTimeStamp = ((tracingMessageTimeStamp / intervalLength) * intervalLength) + intervalLength;
                return true;
            }
            
            return false;
        }

        internal static bool Done(long timeDelta, long ElapsedTicks)
        {
            TimeSynchro.timeDelta = timeDelta;

            Logging.Log.Instance.Warn("TimeSynchro EpochNanoseconds.UtcNow.Value: {0} intervalEndTimeStamp:  {1} timeDelta: {2} ElapsedTicks: {4}",
                EpochNanoseconds.UtcNow.Value, intervalEndTimeStamp, timeDelta, true, 1000D * ElapsedTicks / System.Diagnostics.Stopwatch.Frequency);

            return true;
        }
    }
}
