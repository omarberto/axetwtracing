﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AxETWTracing.Statistics;

namespace AxETWTracing.Events
{
    internal sealed class AosInstances
    {
        private static readonly Lazy<AosInstances> lazy = new Lazy<AosInstances>(() => new AosInstances());
        public static AosInstances instance { get { return lazy.Value; } }

        private AosInstances()
        {
        }

        private Dictionary<int, AosInstanceStatistics> _aosServices = new Dictionary<int, AosInstanceStatistics>();
        public int Count
        {
            get { return _aosServices.Values.Sum(inst=>inst.SessionsCount); }
        }
        public IEnumerable<AosInstanceStatistics> All
        {
            get { return _aosServices.Values; }
        }

        public long RPC_stmt_count_cumu { get; internal set; }
        public long SQL_stmt_count_cumu { get; internal set; }
        public long MDL_stmt_count_cumu { get; internal set; }
        public long FTC_stmt_count_cumu { get; internal set; }

        private List<int> validInstances;
        
        internal void setInstance(int processID, AxServerInstance instance)
        {
            if (isValid(instance.InstanceNumber))
            {
                if (_aosServices.ContainsKey(processID))
                    _aosServices.Remove(processID);
                else if (_aosServices.Values.Select(svc=>svc.service).ToList().Contains(instance))
                {
                    int oldProcessId = _aosServices.First(el => el.Value.service.Equals(instance)).Key;
                    _aosServices.Remove(oldProcessId);
                }
                _aosServices.Add(processID, new AosInstanceStatistics(instance));
            }
        }
        internal bool verifyAosInstance(int processID)
        {
            //Logging.Log.Instance.Debug("verifyAosInstance:  {0} --> {1}", processID, _aosServices.Keys.FirstOrDefault());
            return _aosServices.ContainsKey(processID);
        }

        internal void Init(string[] validInstances)
        {
            this.validInstances = validInstances.Select(item=>int.Parse(item)).ToList();
        }

        internal bool isValid(int instanceNumber)
        {
            if (validInstances.Count == 0) return true;
            return this.validInstances.Contains(instanceNumber);
        }

        internal AosInstanceStatistics getServer(int processId)
        {
            return _aosServices[processId];
        }
    }
}
