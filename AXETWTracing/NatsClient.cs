﻿using AxETWTracing.DataSend;
using System;
using System.Collections.Generic;

namespace AxETWTracing
{
    internal sealed class NatsClient : IDisposable
    {
        private static readonly Lazy<NatsClient> lazy = new Lazy<NatsClient>(() => new NatsClient());

        internal static NatsClient Instance { get { return lazy.Value; } }

        Dictionary<string, InfluxDataSend> _senders = new Dictionary<string, InfluxDataSend>();
        private NatsClient()
        {
            //qui fare init di tutti i publishers
            if (AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.hasOutput)
            {
                InfluxDataSend sender = AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.nats.secure ?
                    new InfluxDataSend(AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.nats.address, AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.nats.subject, AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.nats.certificate)
                    :
                    new InfluxDataSend(AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.nats.address, AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.nats.subject);

                _senders.Add("DATA",
                    sender
                );
                if (AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.useTornado && !AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.tornado.openconnection)
                {
                    _senders.Add("TORNADO",
                        sender
                    );
                }
            }
            if (AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.useTornado && AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.tornado.openconnection)
            {
                InfluxDataSend sender = AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.tornado.secure ?
                    new InfluxDataSend(AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.tornado.address, AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.tornado.topic, AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.tornado.certificate)
                    :
                    new InfluxDataSend(AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.tornado.address, AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.tornado.topic);
                _senders.Add("TORNADO",
                    sender
                );
            }
        }

        internal InfluxDataSend this[string name]
        {
            get
            {
                return _senders[name];
            }
        }

        public void Dispose()
        {
            foreach (var sender in _senders.Keys)
                _senders[sender].Dispose();
        }

        internal bool hasSender(string name)
        {
            return _senders.ContainsKey(name);
        }
    }
}
