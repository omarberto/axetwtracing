﻿using Microsoft.Diagnostics.Tracing;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxETWTracing
{
    public class CustomConfigurationSection : ConfigurationSection
    {
        public static T Load<T>(string name = null)
        {
            Logging.Log.Instance.Debug("{0:HH:mm:ss.ffffff} CustomConfigurationSection -> ${1}$${2}$", DateTime.Now, name, configPath);
            Configuration configuration;
            if (string.IsNullOrEmpty(configPath))
            {
                configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            }
            else
            {
                Logging.Log.Instance.Debug(configPath);
                ExeConfigurationFileMap configMap = new ExeConfigurationFileMap();
                configMap.ExeConfigFilename = configPath;
                configuration = ConfigurationManager.OpenMappedExeConfiguration(configMap, ConfigurationUserLevel.None);
            }

            foreach (ConfigurationSectionGroup csg in configuration.SectionGroups)
            {
                foreach (ConfigurationSection cs in csg.Sections)
                {
                    if(Verify<T>(cs, name)) return (T)(object)cs;
                }
            }
            foreach (ConfigurationSection cs in configuration.Sections)
            {
                if (Verify<T>(cs, name)) return (T)(object)cs;
            }
            throw new Exception("Configuration not found");
        }
        
        public static string configPath { set; get; } = null;

        private static bool Verify<T>(ConfigurationSection cs, string name = null)
        {
            if (string.IsNullOrEmpty(name))
                return cs.SectionInformation.Type.Contains(typeof(T).AssemblyQualifiedName);
            else
                return cs.SectionInformation.Type.Contains(typeof(Hstatistics).AssemblyQualifiedName) && cs.SectionInformation.Name == name;
        }
    }
}
