﻿using Microsoft.Diagnostics.Tracing.Session;
using System;
using System.Linq;

namespace AxETWTracingOLD.ETW
{
    //questa parte andrebbe possibilimente inserita in Lib TraceEvent

    //internal class TraceEventSessionBuilder
    //{
    //    private string providerName;

    //    public TraceEventSessionBuilder(string providerName, string taskId)
    //    {
    //        this.providerName = providerName;

    //        //questa parte qui sarebbe bello correggerla!!!

    //        // Today you have to be Admin to turn on ETW events (anyone can write ETW events).   
    //        //if (!(TraceEventSession.IsElevated() ?? false))
    //        //{
    //        //    //Debugger.Break();
    //        //    session = null;
    //        //}
    //        //else
    //        {
    //            var sessionName = new SessionName(providerName, taskId).Value;
    //            Logging.Log.Instance.Trace("sessionName {0}", sessionName);
    //            var session = TraceEventSession.GetActiveSession(sessionName);
    //            Logging.Log.Instance.Trace("session {0}", session == null);
    //            if (session != null)
    //            {
    //                //attach old
    //                session.Dispose();
    //            }
    //            this.session = new TraceEventSession(sessionName);
    //        }
    //    }

    //    public TraceEventSession session { private set; get; }
    //}
}

namespace AxETWTracing.ETW
{
    internal class TraceEventSessionBuilder
    {
        public TraceEventSessionBuilder(string providerName)
        {
            var sessionClassName = new SessionName(providerName, "ST").Value;

            Logging.Log.Instance.Debug("active sessions QUERY!!!}");

            foreach (var activeSessionName in TraceEventSession.GetActiveSessionNames())
            {
                Logging.Log.Instance.Debug("active sessions {0} {1}", sessionClassName, activeSessionName);
            }
            {
                var classSessionName = new SessionName(providerName, "ST").Value;
                Logging.Log.Instance.Debug("classSessionName {0}", classSessionName);
                var maxUsedIndex = 0;
                foreach (var sessionName in TraceEventSession.GetActiveSessionNames().Where(name => name.Contains(classSessionName)))
                {
                    int tmp;
                    Logging.Log.Instance.Debug("sessionName {0}", sessionName);
                    if (int.TryParse(sessionName.Replace(classSessionName, ""), out tmp))
                    {
                        Logging.Log.Instance.Debug("tmp {0}", tmp);
                        if (maxUsedIndex == tmp)
                            maxUsedIndex++;
                        else
                            break;
                    }
                }
                var indexedSessionName = new IndexedSessionName(providerName, maxUsedIndex, "ST").Value;
                {
                    //new session
                    session = new TraceEventSession(indexedSessionName);
                }
            }

        }

        public TraceEventSessionBuilder(string providerName, string taskId)
        {
            foreach (var activeSessionName in TraceEventSession.GetActiveSessionNames())
            {
                Logging.Log.Instance.Debug("active sessions {0}", activeSessionName);
            }
            {
                var classSessionName = new SessionName(providerName, taskId).Value;
                Logging.Log.Instance.Debug("classSessionName {0}", classSessionName);
                var maxUsedIndex = 0;
                foreach (var sessionName in TraceEventSession.GetActiveSessionNames().Where(name => name.Contains(classSessionName)))
                {
                    int tmp;
                    Logging.Log.Instance.Debug("sessionName {0}", sessionName);
                    if (int.TryParse(sessionName.Replace(classSessionName, ""), out tmp))
                    {
                        Logging.Log.Instance.Debug("tmp {0}", tmp);
                        if (maxUsedIndex == tmp)
                            maxUsedIndex++;
                        else
                            break;
                    }
                }
                var indexedSessionName = new IndexedSessionName(providerName, maxUsedIndex, taskId).Value;
                {
                    //new session
                    session = new TraceEventSession(indexedSessionName);
                }
            }
        }

        public TraceEventSession session { private set; get; }
    }
}