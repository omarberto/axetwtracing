﻿namespace AxETWTracingOLD.ETW
{
    internal class SessionName
    {
        internal SessionName(string providerName, string taskId)
        {
            this.Value = "WPST_" + System.Diagnostics.Process.GetCurrentProcess().Id + "_" + providerName + "_" + taskId;
        }

        internal string Value { private set; get; }
    }
}


namespace AxETWTracing.ETW
{
    internal class SessionName
    {
        private string providername;
        private string taskId = "";

        public SessionName(string providername, string taskId)
        {
            this.taskId = taskId;
            this.providername = providername;
        }

        public virtual string Value
        {
            get
            {
                return "WPST_" + System.Diagnostics.Process.GetCurrentProcess().Id + "_" + providername + "_" + taskId;
                return "WPST_" + providername + "_";
            }
        }
    }

    internal class IndexedSessionName : SessionName
    {
        private int index;

        public IndexedSessionName(string providername, int index, string taskId)
            : base(providername, taskId)
        {
            this.index = index;
        }

        public override string Value
        {
            get
            {
                return base.Value + index;
            }
        }
    }
}