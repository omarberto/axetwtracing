﻿using Microsoft.Diagnostics.Tracing;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxETWTracing
{
    public class Hstatistics : CustomConfigurationSection
    {
        [ConfigurationProperty("histograms", IsRequired = false)]
        public histograms histograms
        {
            get
            {
                return (histograms)this["histograms"];
            }
        }

        //private static Hstatistics Load(string name)
        //{
        //    Hstatistics settings;
        //    foreach (ConfigurationSectionGroup csg in ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).SectionGroups)
        //    {
        //        foreach (ConfigurationSection cs in csg.Sections)
        //        {
        //            if(Load(name, cs, out settings)) return settings;
        //        }
        //    }
        //    foreach (ConfigurationSection cs in ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).Sections)
        //    {
        //        if (Load(name, cs, out settings)) return settings;
        //    }
        //    throw new Exception("Configuration not found");
        //}
        public static histogram Load(string name, string histogramName)
        {
            return Load<Hstatistics>(name).histograms[histogramName];
            //Hstatistics settings = Load(name);
            //return settings.histograms[histogramName];
        }

        //private static bool Load(string name, ConfigurationSection cs, out Hstatistics settings)
        //{
        //    settings = null;
        //    if (cs.SectionInformation.Type.Contains(typeof(Hstatistics).AssemblyQualifiedName) && cs.SectionInformation.Name == name)
        //    {
        //        settings = (Hstatistics)ConfigurationManager.GetSection(cs.SectionInformation.SectionName);
        //        return true;
        //    }
        //    return false;
        //}
    }

    [ConfigurationCollection(typeof(histogram))]
    public class histograms : ConfigurationElementCollection
    {
        public new histogram this[string name]
        {
            get
            {
                if (IndexOf(name) < 0) return null;
                return (histogram)BaseGet(name);
            }
        }

        public histogram this[int index]
        {
            get { return (histogram)BaseGet(index); }
        }

        public int IndexOf(string name)
        {
            name = name.ToLower();

            for (int idx = 0; idx < base.Count; idx++)
            {
                if (this[idx].name.ToLower() == name)
                    return idx;
            }
            return -1;
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get { return ConfigurationElementCollectionType.BasicMap; }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new histogram();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((histogram)element).name;
        }

        protected override string ElementName
        {
            get { return "histogram"; }
        }
    }
    public class histogram : ConfigurationElement
    {
        [ConfigurationProperty("name")]
        public string name
        {
            get { return (string)base["name"]; }
            set { base["name"] = value; }
        }

        [ConfigurationProperty("levels", IsRequired = false)]
        public levels levels
        {
            get
            {
                return (levels)this["levels"];
            }
        }
    }

    [ConfigurationCollection(typeof(level))]
    public class levels : ConfigurationElementCollection
    {
        public new level this[string label]
        {
            get
            {
                if (IndexOf(label) < 0) return null;
                return (level)BaseGet(label);
            }
        }

        public level this[int index]
        {
            get { return (level)BaseGet(index); }
        }

        public int IndexOf(string label)
        {
            label = label.ToLower();

            for (int idx = 0; idx < base.Count; idx++)
            {
                if (this[idx].label.ToLower() == label)
                    return idx;
            }
            return -1;
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get { return ConfigurationElementCollectionType.BasicMap; }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new level();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((level)element).label;
        }

        protected override string ElementName
        {
            get { return "level"; }
        }
    }
    public class level : ConfigurationElement
    {
        [ConfigurationProperty("label")]
        public string label
        {
            get { return (string)base["label"]; }
            set { base["label"] = value; }
        }

        [ConfigurationProperty("threshold")]
        public long threshold
        {
            get { return (long)base["threshold"]; }
            set { base["threshold"] = value; }
        }
    }
}
