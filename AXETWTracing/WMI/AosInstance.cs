﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxETWTracing.WMI
{
    internal class AosInstance
    {
        private ushort _instanceNumber;
        public ushort instanceNumber
        {
            get { return _instanceNumber; }
        }
        public bool isAx32Serv
        {
            get { return _instanceNumber > 0; }
        }

        internal AosInstance(int processId)
        {
            string tmp;
            if (GetServiceName(processId, out tmp) && tmp.IndexOf("AOS60$") == 0)
            {
                ushort.TryParse(tmp.Substring(6), out _instanceNumber);
            }
        }

        protected bool GetServiceName(int processId, out string name)
        {
            string query = "SELECT * FROM Win32_Service where ProcessId = " + processId;
            System.Management.ManagementObjectSearcher searcher = new System.Management.ManagementObjectSearcher(query);

            foreach (System.Management.ManagementObject queryObj in searcher.Get())
            {
                name = queryObj["Name"].ToString();
                return true;
            }
            name = string.Empty;
            return false;
        }
    }
}
