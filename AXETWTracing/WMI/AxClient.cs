﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxETWTracing.WMI
{
    internal class AxClient
    {
        public int processId { get; private set; }
        public DateTime creationDate { get; private set; }

        internal AxClient(int processId)
        {
            GetProcess(processId);
        }

        public AxClient(int processId, DateTime dateTime) : this(processId)
        {
            this.processId = processId;
            //non é esatto ma alla fine mi accontento!!!
            this.creationDate = dateTime;
        }

        protected void GetProcess(int processId)
        {
            string query = "SELECT * FROM Win32_Process where ProcessId = " + processId;
            System.Management.ManagementObjectSearcher searcher = new System.Management.ManagementObjectSearcher(query);

            foreach (System.Management.ManagementObject queryObj in searcher.Get())
            {
                try
                {
                    var timeText = queryObj["CreationDate"].ToString().Split(new char[] { '+' }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault();
                    this.creationDate = DateTime.ParseExact(timeText, "yyyyMMddHHmmss.ffffff", System.Globalization.CultureInfo.InvariantCulture);
                }
                catch { }
            }
        }

        public static IEnumerable<int> CheckProcesses()
        {
            string query = "SELECT ProcessId FROM Win32_Process where Name = 'Ax32.exe'";
            System.Management.ManagementObjectSearcher searcher = new System.Management.ManagementObjectSearcher(query);

            foreach (System.Management.ManagementObject queryObj in searcher.Get())
            {
                yield return int.Parse(queryObj["ProcessId"].ToString());
            }
        }
    }
}
