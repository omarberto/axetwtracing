﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxETWTracing.Monitor.Servers
{
    internal class Server
    {
        private string instanceName;

        public Server(string instanceName)
        {
            this.instanceName = instanceName;
        }

        public bool verifyInstance(string instanceName)
        {
            return this.instanceName == instanceName;
        }
    }
}
