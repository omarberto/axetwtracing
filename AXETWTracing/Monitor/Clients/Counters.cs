﻿using AxETWTracing.Events;

namespace AxETWTracing.Monitor.Clients
{
    internal class Counters
    {
        public int FormOpen { get; set; }
        public int FormClose { get; set; }
        public int ControlClicked { get; set; }
    }
}