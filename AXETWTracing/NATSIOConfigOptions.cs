﻿using Microsoft.Diagnostics.Tracing;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxETWTracing
{
    public class NATSIOConfigOptions : CustomConfigurationSection
    {
        [ConfigurationProperty("msSendCycle", DefaultValue = 60000, IsRequired = false)]
        public int msSendCycle
        {
            get
            {
                return (int)this["msSendCycle"];
            }
            set
            {
                this["msSendCycle"] = value;
            }
        }

        [ConfigurationProperty("address", IsRequired = true)]
        public string NATSaddress
        {
            get
            {
                return this["address"] as string;
            }
            set
            {
                this["address"] = value;
            }
        }

        [ConfigurationProperty("subject", IsRequired = true)]
        public string NATSsubject
        {
            get
            {
                return this["subject"] as string;
            }
            set
            {
                this["subject"] = value;
            }
        }

        public static NATSIOConfigOptions Load()
        {
            return Load<NATSIOConfigOptions>();
        }
    }
}
