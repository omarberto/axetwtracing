﻿using Microsoft.Diagnostics.Tracing;
using System;

namespace AxETWTracing
{
    internal class SimpleFileReloggerTest
    {
        internal static void Run()
        {
            var inputFileName = "ReloggerFileInput.etl";
            var outFileName = "ReloggerFileOutput.etl";

            // Create some data by listening for 10 seconds
            //DataCollection(inputFileName);

            inputFileName = @"C:\Users\pb00238\Progetti\demo\DataCollector01.etl";
            //inputFileName = @"C:\Users\pb00238\Progetti\demo\piccolo.etl";

            FilterData(inputFileName, outFileName);

            //DataProcessing(outFileName);
        }

        private static void DataProcessing(string outFileName)
        {
            throw new NotImplementedException();
        }

        private static bool isFirst = true;
        private static void FilterData(string inputFileName, string outFileName)
        {
            using (var relogger = new ETWReloggerTraceEventSource(inputFileName, outFileName))
            {
                relogger.AllEvents += delegate (TraceEvent data)
                {
                    if (isFirst)
                    {
                        relogger.WriteEvent(data);
                        isFirst = false;
                    }
                };

                relogger.Process();
            }
        }
    }
}