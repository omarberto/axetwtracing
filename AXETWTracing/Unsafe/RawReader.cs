﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxETWTracing.Unsafe
{
    class RawReader
    {
        /// <summary>
        /// controlla se la stringa nel buffer é uguale a quella "test", attenzione che se fallisce esce e offset non é utilizzabile per stringhe successive
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="offset"></param>
        /// <param name="test"></param>
        /// <returns></returns>
        unsafe public static bool TestUnicodeString(byte[] buffer, ref int offset, string test)
        {
            fixed (byte* ptr = buffer) fixed (char* tp = test)
            {
                int bufferLength = buffer.Length;

                char* charStart = (char*)(ptr + offset);
                int maxPos = Math.Min((bufferLength - offset) / sizeof(char), test.Length);
                int curPos = 0;

                offset += sizeof(char);

                while (true)
                {
                    if (charStart[curPos] != tp[curPos])
                        return false;
                    else if (charStart[curPos] == 0)
                        return true;

                    curPos++;
                    if (curPos == maxPos) return false;
                    offset += sizeof(char);
                }
            }
        }

        unsafe public static void CopyUnicodeString(byte[] srcBuffer, ref int srcOffset, char* dstBuffer)
        {
            fixed (byte* ptr = srcBuffer)
            {
                int bufferLength = srcBuffer.Length;

                char* charStart = (char*)(ptr + srcOffset);
                int maxPos = (bufferLength - srcOffset) / sizeof(char);
                int curPos = 0;

                srcOffset += sizeof(char);

                while (charStart[curPos] != 0)
                {
                    dstBuffer[curPos] = charStart[curPos];
                    curPos++;
                    if (curPos == maxPos)
                        break;
                    srcOffset += sizeof(char);
                }

                //return new string(charStart, 0, curPos);
            }
        }

        unsafe public static byte[] UnicodeStringToUTF8Fast(byte[] buffer, ref int offset)
        {
            fixed (byte* ptr = buffer)
            {
                int bufferLength = buffer.Length;

                char* charStart = (char*)(ptr + offset);
                int maxPos = (bufferLength - offset) / sizeof(char);
                int curPos = 0;

                offset += sizeof(char);

                byte[] retvalue = new byte[maxPos];
                while (charStart[curPos] != 0)
                {
                    retvalue[curPos] = (byte)charStart[curPos];
                    curPos++;
                    if (curPos == maxPos)
                        break;
                    offset += sizeof(char);
                }


                return retvalue;
            }
        }
        unsafe public static byte[] UnicodeStringToUTF8(byte[] buffer, ref int offset)
        {
            int inOffest = offset;
            int outputLenght = UnicodeStringToUTF8Length(buffer, ref offset);

            fixed (byte* ptr = buffer)
            {
                char* charStart = (char*)(ptr + inOffest);

                byte[] retvalue = new byte[outputLenght];

                int curPos = 0;
                while (charStart[curPos] != 0)
                {
                    retvalue[curPos] = (byte)charStart[curPos];
                    curPos++;
                }

                return retvalue;
            }
        }
        unsafe public static int UnicodeStringToUTF8Length(byte[] buffer, ref int offset)
        {
            fixed (byte* ptr = buffer)
            {
                int bufferLength = buffer.Length;


                char* charStart = (char*)(ptr + offset);
                int maxPos = (bufferLength - offset) / sizeof(char);
                int curPos = 0;

                offset += sizeof(char);

                while (charStart[curPos] != 0)
                {
                    curPos++;
                    if (curPos == maxPos)
                        break;
                    offset += sizeof(char);
                }


                return curPos;
            }
        }
        unsafe public static string ReadUnicodeString(byte[] buffer, ref int offset)
        {
            fixed (byte* ptr = buffer)
            {
                int bufferLength = buffer.Length;

                char* charStart = (char*)(ptr + offset);
                int maxPos = (bufferLength - offset) / sizeof(char);
                int curPos = 0;

                offset += sizeof(char);

                while (charStart[curPos] != 0)
                {
                    curPos++;
                    if (curPos == maxPos)
                        break;
                    offset += sizeof(char);
                }

                return new string(charStart, 0, curPos);
            }
        }
        unsafe internal static string ReadUnicodeString(byte[] buffer, int offset, out int length)
        {
            fixed (byte* ptr = buffer)
            {
                length = 2;
                int bufferLength = buffer.Length;

                char* charStart = (char*)(ptr + offset);
                int maxPos = (bufferLength - offset) / sizeof(char);
                int curPos = 0;
                while (charStart[curPos] != 0)
                {
                    curPos++;
                    if (curPos == maxPos)
                        break;
                    length += sizeof(char);
                }

                return new string(charStart, 0, curPos);
            }
        }

        unsafe internal static string ReadUnicodeString(byte[] buffer, int offset, int bufferLength, out int length)
        {
            // Really we should be able to count on pointers being null terminated.  However we have had instances
            // where this is not true.   To avoid scanning the string twice we first check if the last character
            // in the buffer is a 0 if so, we KNOW we can use the 'fast path'  Otherwise we check 
            fixed (byte* ptr = buffer)
            {
                //questo blocco lo eviterei....
                //farei chiamata quando si sa giá che siamo su stringa corta e quando si sa giá che siamo 
                length = bufferLength;
                char* charEnd = (char*)(ptr + bufferLength);
                if (charEnd[-1] == 0)       // Is the last character a null?
                    return new string((char*)(ptr + offset));       // We can count on a null, so we do an optimized path 

                // (works for last string, and other times by luck). 
                // but who cares as long as it stays in the buffer.  

                // unoptimized path.  Carefully count characters and create a string up to the null.  
                char* charStart = (char*)(ptr + offset);
                int maxPos = (bufferLength - offset) / sizeof(char);
                int curPos = 0;
                while (curPos < maxPos && charStart[curPos] != 0)
                    curPos++;
                // CurPos now points at the end (either buffer end or null terminator, make just the right sized string.  
                return new string(charStart, 0, curPos);
            }
        }
        unsafe internal static short ReadInt16(byte[] buffer, int offset)
        {
            fixed (byte* ptr = buffer)
            {
                return *((short*)(ptr + offset));
            }
        }
        unsafe internal static ushort ReadUInt16(byte[] buffer, int offset)
        {
            fixed (byte* ptr = buffer)
            {
                return *((ushort*)(ptr + offset));
            }
        }
        unsafe internal static int ReadInt32(byte[] buffer, int offset)
        {
            fixed (byte* ptr = buffer)
            {
                return *((int*)(ptr + offset));
            }
        }
        unsafe internal static long ReadInt64(byte[] buffer, int offset)
        {
            fixed (byte* ptr = buffer)
            {
                return *((long*)(ptr + offset));
            }
        }
    }
}
