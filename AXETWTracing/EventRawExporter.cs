﻿using System;
using Microsoft.Diagnostics.Tracing;
using Microsoft.Diagnostics.Tracing.Session;
using System.IO;
using System.Threading.Tasks;
using AxETWTracing.Unsafe;
using System.Collections.Generic;
using AxETWTracing.Events;
using System.Threading;
using System.Linq;
using System.Text;
using AxETWTracing.Statistics.CLIENT;

namespace AxETWTracing
{
    public class EventRawExporter : IDisposable
    {
        private TraceEventSession session;
        private bool sessionDisposed {
            get {
                return session == null;
            }
        }

        private int? index;
        private ulong keywords;
        private string providerName;
        private TraceEventLevel traceEventLevel;

        private DataSend.INATSSend dataSend;
        
        public EventRawExporter(DataSend.INATSSend dataSend, string providerName, TraceEventLevel traceEventLevel, ulong keywords, int? index, string[] validInstances)
        {
            this.dataSend = dataSend;

            this.providerName = providerName;
            this.traceEventLevel = traceEventLevel;
            this.keywords = keywords;
            this.index = index;
            AosInstances.instance.Init(validInstances);
            //poi vediamo di aggiustare il time
            //resetEvent = new ManualResetEvent(false);
            //waitHandle = ThreadPool.RegisterWaitForSingleObject(resetEvent, new WaitOrTimerCallback(statisticPublisher), null, dataSend.publishInterval, false);//
            //Statistics.SessionsMatrix.instance.Init(dataSend);

            publishThread = new Thread(new ThreadStart(statisticPublisher));
            publishThread.Start();
        }

        private Thread publishThread = null;

        private void statisticPublisher()
        {
            int noEvents = 0;
            ulong checkEventCount = 0UL;
            while (!autoResetEvent.WaitOne(dueTime))
            {
                //if ((this.keywords & 0x2000000000000000UL) > 0)
                {
                    DateTime nowT = DateTime.UtcNow;
                    DateTime cycleWriteDT_EVENTS = nowT.AddMilliseconds(publishCycleTime / 2);//x evitare stranezze
                    DateTime cycleWriteDT = cycleWriteDT_EVENTS.AddMilliseconds(-publishCycleTime);

                    long cycleWriteTS_EVENTS = new EpochNanoseconds(cycleWriteDT_EVENTS).Value;
                    long cycleWriteTS = new EpochNanoseconds(cycleWriteDT).Value;//sembrerebbe che la thread calcoli piú volte questo valore 

                    Logging.Log.Instance.Debug("PUBLISHING: {0:HH:mm:ss.fffffff}||{1:HH:mm:ss.fffffff}||{2:HH:mm:ss.fffffff} - {3}||{4}", nowT, cycleWriteDT_EVENTS, cycleWriteDT, cycleWriteTS_EVENTS, cycleWriteTS);
                    Logging.Log.Instance.Debug("\n{2:HH:mm:ss.ffffff} // total: {0} losts: {1}", totalEventsCount, lostEventsCount, cycleWriteDT);

                    //--> dobbiamo inventarci le statistiche per il client, se qui le levo poi come faccio???
                    //--> se ho server e client su stessa macchina dovró fare attenzione, per ora gestiamo questo doppione

                    //NNNNB: adesso mi accontento di fotografare status

                    dataSend.PublishEvent("HEARTBEAT", cycleWriteTS,
                            string.Format("servers={0}i,RPC={4}i,SQL={5}i,MDL={6}i,FTC={7}i,clients={3}i,Ntotal={1}i,Nlost={2}i,CLI={4}i"
                            , AosInstances.instance.Count
                            , totalEventsCount, lostEventsCount, Statistics.CLIENT.SessionsMatrix.popClientEventStatistics(cycleWriteTS)
                            , Statistics.CLIENT.SessionsMatrix.CLI_events_count_cumu
                            , AosInstances.instance.RPC_stmt_count_cumu//non distiguo per istanza
                            , AosInstances.instance.SQL_stmt_count_cumu//non distiguo per istanza
                            , AosInstances.instance.MDL_stmt_count_cumu//non distiguo per istanza
                            , AosInstances.instance.FTC_stmt_count_cumu//non distiguo per istanza
                            ),
                            string.Format(",AxServer={0}", Environment.MachineName));
                    
                    //qui mettiamo un keep-alive
                    if (checkEventCount == totalEventsCount && AX2012ETWtracing.Configuration.Configuration.config.general.zeroEventsTimeout > 0)
                    {
                        noEvents++;
                        if (noEvents > AX2012ETWtracing.Configuration.Configuration.config.general.zeroEventsTimeout)
                        {
                            //error 5 mintu senza dati
                            //throw new Exception("No data from provider since 5 minutes");
                            
                            autoResetEvent.Dispose();
                            autoResetEvent = null;
                            this.autokilled = true;
                            InternalDispose();
                            break;
                            //potremmo fare dispose, giusto per provare
                        }
                    }
                    else
                    {
                        noEvents = 0;
                    }
                    checkEventCount = totalEventsCount;
                }

                AxClientStatistics.instance.checkClients();
            }
            Logging.Log.Instance.Info("EXIT THREAD");
        }
        

        private System.Threading.AutoResetEvent autoResetEvent = new AutoResetEvent(false);
        private int dueTime
        {
            get
            {
                var currentElapsed = Convert.ToInt32(DateTime.Now.TimeOfDay.TotalMilliseconds) % publishCycleTime;
                if (currentElapsed > publishCycleTime / 2)
                    return 2 * publishCycleTime - currentElapsed;
                else
                    return publishCycleTime - currentElapsed;
            }
        }
        private const int publishCycleTime = 60000;

        public void Dispose()
        {
            if (publishThread.IsAlive && (autoResetEvent != null) && autoResetEvent.Set())
            {
                try
                {
                    if (!publishThread.Join(5000))
                    {
                        Logging.Log.Instance.Error("publishThread.Join: false");
                    }
                }
                catch (Exception e)
                {
                    Logging.Log.Instance.Error(e, "publishThread.Join: Exception");
                }
            }

            InternalDispose();
        }
        private void InternalDispose()
        {
            if (sessionDisposed) return;

            session.Source.Dynamic.All -= allEvents;
            
            this.session.DisableProvider(this.providerName);

            Logging.Log.Instance.Trace("PROCESS_END this.session.Source.Dispose() {0}", EpochNanoseconds.UtcNow.Value);

            this.session.Source.Dispose();

            Logging.Log.Instance.Trace("PROCESS_END this.session.Dispose() {0}", EpochNanoseconds.UtcNow.Value);

            this.session.Dispose();
            this.session = null;

            Logging.Log.Instance.Trace("PROCESS_END this.dataSend.Dispose() {0}", EpochNanoseconds.UtcNow.Value);
            ((DataSend.InfluxDataSend)this.dataSend).Dispose();
            Logging.Log.Instance.Trace("PROCESS_END {0}", EpochNanoseconds.UtcNow.Value);
        }

        internal int Run()
        {
            session = new TraceEventSessionBuilder(providerName, index).session;
            Logging.Log.Instance.Debug("sessionName {0}", session.SessionName);
            if (session == null)
            {
                return -1;
            }
            else
            {
                session.Source.Dynamic.All += allEvents;
                
                //session.CircularBufferMB = 512;
                var restarted = session.EnableProvider(providerName, traceEventLevel, keywords);
                if (restarted) Logging.Log.Instance.Debug("The session {0} was already active, it has been restarted.", session.SessionName);
                else
                    Logging.Log.Instance.Debug("The session {0} has been started.", session.SessionName);
                
                return 0;
            }
        }

        internal bool Process()
        {
            Logging.Log.Instance.Trace("PROCESS_START {0}", EpochNanoseconds.UtcNow.Value);
            bool res = session.Source.Process();
            Logging.Log.Instance.Trace("PROCESS_ENDED ABORT {0} {1}", EpochNanoseconds.UtcNow.Value, res);

            //System.Threading.Thread.CurrentThread.Abort();
            //session.Source
            return autokilled;
        }
                
        private int lostEventsCount;
        private ulong totalEventsCount;

        private DateTime refTime = DateTime.Now;
        private string lastModelStmt = "";
        private Dictionary<string, int> modelStats = new Dictionary<string, int>();
        private bool autokilled = false;

        private void allEvents(TraceEvent obj)
        {
            //TraceInformation: inviato da ogni applicativo attivo
            totalEventsCount++;

            //poi ci pensiamo se cancellare...
            //Statistics.SessionsMatrix.instance.ClearDefuncts();
            
            bool isValidEvent = false;
            switch (obj.ushort_ID)
            {

                /*ClientAccess*/
                case 95:
                case 96:
                case 97:
                    {
                        var clientEvent = new ClientEvent(obj);
                        Statistics.CLIENT.SessionsMatrix.pushClientEvent(clientEvent, obj.nsTimeStampEpoch, obj.ProcessID);
                        isValidEvent = true;
                    }
                    break;

                /*Caching*/
                case 12:
                    {
                        var tableLoading = new TableLoading(obj);
                        Statistics.CACHING.SessionsMatrix.pushEvent(tableLoading);
                        isValidEvent = true;
                    }
                    break;
                case 13:
                    {
                        var classLoading = new ClassLoading(obj);
                        Statistics.CACHING.SessionsMatrix.pushEvent(classLoading);
                        isValidEvent = true;
                    }
                    break;
                case 33:
                    {
                        var dataAccessCCaching = new DataAccessCCaching(obj);
                        Statistics.CACHING.SessionsMatrix.pushEvent(dataAccessCCaching);
                        isValidEvent = true;
                    }
                    break;
                case 34:
                    {
                        var dataAccessSCSCCaching = new DataAccessSCSCCaching(obj);
                        Statistics.CACHING.SessionsMatrix.pushEvent(dataAccessSCSCCaching);
                        isValidEvent = true;
                    }
                    break;
                case 98:
                    {
                        var assemblyCacheElementBegin = new AssemblyCacheElementBegin(obj);
                        Statistics.CACHING.SessionsMatrix.pushEvent(assemblyCacheElementBegin);
                        isValidEvent = true;
                    }
                    break;
                case 99:
                    {
                        var assemblyCacheElement = new AssemblyCacheElement(obj);
                        Statistics.CACHING.SessionsMatrix.pushEvent(assemblyCacheElement);
                        isValidEvent = true;
                    }
                    break;
                case 100:
                    {
                        var assemblyCacheElementEnd = new AssemblyCacheElementEnd(obj);
                        Statistics.CACHING.SessionsMatrix.pushEvent(assemblyCacheElementEnd);
                        isValidEvent = true;
                    }
                    break;


                //attivi solo con XppMarker 0x0000000000000001
                case 51:
                    {
                        //"marchio" la sessione con questo evento, occhio che se non me la chiudono poi io la tento aperta fino a che la stessa id non viene riusata
                        //la marchio come tag in measuremnt ad eventi e come field nella ETW cosi filtro funziona!!!

                        Logging.Log.Instance.Debug("XPPMARKER EVENT 51 for {0}", obj.ProcessID);
                    }
                    break;
                case 52:
                    {
                        Logging.Log.Instance.Debug("XPPMARKER EVENT 52 for {0}", obj.ProcessID);
                    }
                    break;


                /*Traceinfo 0x0000000000000002*/
                case 53:
                    {
                        Logging.Log.Instance.Debug("TRACEINFO EVENT 53 for {0}", obj.ProcessID);
                    }
                    break;
                case 68:
                    {
                        Logging.Log.Instance.Debug("TRACEINFO EVENT 68 for {0}", obj.ProcessID);
                    }
                    break;
            }



            if (obj.ushort_ID == 70)
            {
                var traceInformation = new Events.TraceInformation(obj);
                Logging.Log.Instance.Info("ProcessName  {0} {1}", traceInformation.ProcessName, traceInformation.ProcessId);
                if (traceInformation.isAxServer && AosInstances.instance.isValid(traceInformation.instanceNumber))
                {
                    AosInstances.instance.setInstance(traceInformation.ProcessId, traceInformation.instance);
                }
                else /*if (traceInformation.isAxClient)*/
                {
                    Logging.Log.Instance.Info("ProcessId:{0}\nHex ProcessId:{0:x}\nApplicationName:{1}\nAxUserName:{2}", traceInformation.ProcessId, traceInformation.ProcessName, traceInformation.UserName);
                    //
                    
                }
                //else
                //{
                //    Logging.Log.Instance.Debug("Process {1} Not Valid -> {0}", traceInformation.ProcessName, traceInformation.ProcessId);
                //}
            }
            else if (AosInstances.instance.verifyAosInstance(obj.ProcessID))
            {
                try
                {
                    switch (obj.ushort_ID)
                    {
                        //RPC
                        case 54:
                            {
                                //CSRoundTripBegin
                                var csroundtripBegin = new CSRoundTripBegin(obj);
                                AosInstances.instance.getServer(csroundtripBegin.processId).addEvent(csroundtripBegin);
                                //AosInstances.instance.RPC_stmt_count_cumu++;
                                //Logging.Log.Instance.Debug("csroundtripBegin {0} {1}", csroundtripBegin.AxSessionId, csroundtripBegin.AxNestLevel);
                            }
                            break;
                        case 55:
                            {
                                //CSRoundTripEnd
                                var csroundtripEnd = new CSRoundTripEnd(obj);
                                AosInstances.instance.getServer(csroundtripEnd.processId).addEvent(csroundtripEnd);
                                AosInstances.instance.RPC_stmt_count_cumu++;
                                //Logging.Log.Instance.Debug("csroundtripEnd {0} {1} ({2}<>{3})", csroundtripEnd.AxSessionId, csroundtripEnd.AxNestLevel, csroundtripEnd.AxServerBytesRecd, csroundtripEnd.AxServerBytesSent);
                            }
                            break;

                        //XPP
                        case 56:
                            {
                                //XppMethodBegin
                                var xppmethodBegin = new XppMethodBegin(obj);
                                AosInstances.instance.getServer(xppmethodBegin.processId).addEvent(xppmethodBegin);
                            }
                            break;
                        case 57:
                            {
                                //XppMethodEnd
                                var xppmethodEnd = new XppMethodEnd(obj);
                                AosInstances.instance.getServer(xppmethodEnd.processId).addEvent(xppmethodEnd);
                            }
                            break;

                        //SQL
                        //ModelSqlInputBind  94  BindParameter --> non é gestito
                        case 65:
                            {
                                ////DEVEL#SQLINPUTBIND
                                ////per ora tolgo tutto, non mettere in keywords!!!!

                                ////SqlInputBind
                                //var sqlInputBind = new SqlInputBind(obj);
                                //Statistics.SQL.SqlParamsBinding.instance.AddBindParameter(sqlInputBind);
                            }
                            break;
                        case 94:
                            {
                                ////DEVEL#SQLINPUTBIND
                                ////per ora tolgo tutto, non mettere in keywords!!!!

                                ////SqlInputBind
                                //var sqlInputBind = new SqlInputBind(obj);
                                //Statistics.SQL.SqlParamsBinding.instance.AddBindParameter(sqlInputBind);
                            }
                            break;
                        case 59:
                            {
                                //AxSqlStmtEvent
                                var axSqlStmtEvent = new AxSqlStmtEvent(obj);
                                AosInstances.instance.getServer(axSqlStmtEvent.processId).addEvent(axSqlStmtEvent);
                                AosInstances.instance.SQL_stmt_count_cumu++;

                                //DEVEL#SQLINPUTBIND
                                //per ora tolgo tutto, non mettere in keywords!!!!

                                //////tolgo questa parte perché la logica va validata, peró é chiaro come funziona!!!
                                //if (Config.TomlConfiguration.config.tracing.hasValidSQLBIND)
                                //    Statistics.SQL.SQLsessionmatrix.ProcessStatement(axSqlStmtEvent);

                                //Statistics.SQL.SqlParamsBinding.instance.ClearBindParameters(axSqlStmtEvent.processId, axSqlStmtEvent.AxSessionId, axSqlStmtEvent.AxUserName, axSqlStmtEvent.AxConDBSpid);
                            }
                            break;
                        case 74:
                            {
                                //ModelStoredProc
                                var modelStoredProc = new ModelStoredProc(obj);
                                AosInstances.instance.getServer(modelStoredProc.processId).addEvent(modelStoredProc);
                                AosInstances.instance.MDL_stmt_count_cumu++;

                                ////DEVEL#SQLINPUTBIND
                                ////per ora tolgo tutto, non mettere in keywords!!!!

                                //Statistics.SQL.SqlParamsBinding.instance.ClearBindParameters(modelStoredProc.processId, modelStoredProc.AxSessionId, modelStoredProc.AxUserName, modelStoredProc.AxConDBSpid);
                            }
                            break;
                        case 66:
                            {
                                //SqlRowFetch
                                var sqlRowFetch = new SqlRowFetch(obj);
                                AosInstances.instance.getServer(sqlRowFetch.processId).addEvent(sqlRowFetch);
                                AosInstances.instance.FTC_stmt_count_cumu++;
                            }
                            break;
                        case 67:
                            {
                                //SqlRowFetchCumu
                                var sqlRowFetchCumu = new SqlRowFetchCumu(obj);
                                AosInstances.instance.getServer(sqlRowFetchCumu.processId).addEvent(sqlRowFetchCumu);
                                //AosInstances.instance.FTC_stmt_count_cumu++;
                            }
                            break;

                        //attivi solo con XppMarker 0x0000000000000001
                        case 51:
                            {
                                //"marchio" la sessione con questo evento, occhio che se non me la chiudono poi io la tento aperta fino a che la stessa id non viene riusata
                                //la marchio come tag in measuremnt ad eventi e come field nella ETW cosi filtro funziona!!!
                                
                                //dimentica sopra e per ora gestisco ad eventi....
                                var axTransactionBeginEvent = new AxTransactionBeginEvent(obj);
                                Logging.Log.Instance.Debug("AxTransactionBeginEvent {1} {0} --> {2}", axTransactionBeginEvent.AxSessionId.Value, axTransactionBeginEvent.AxUserName.ASCII, axTransactionBeginEvent.AxMarkerName);
                                AosInstances.instance.getServer(axTransactionBeginEvent.processId).pushEvent(axTransactionBeginEvent);
                            }
                            break;
                        case 52:
                            {
                                var axTransactionEndEvent = new AxTransactionEndEvent(obj);
                                Logging.Log.Instance.Debug("AxTransactionEndEvent {1} {0} --> {2}", axTransactionEndEvent.AxSessionId.Value, axTransactionEndEvent.AxUserName.ASCII, axTransactionEndEvent.AxMarkerName);
                                AosInstances.instance.getServer(axTransactionEndEvent.processId).pushEvent(axTransactionEndEvent);
                            }
                            break;


                        /*Traceinfo 0x0000000000000002*/
                        case 53:
                            {
                                var axTraceMessageEvent = new AxTraceMessageEvent(obj);
                                Logging.Log.Instance.Debug("AxTraceMessageEvent {1} {0} --> {2}", axTraceMessageEvent.AxSessionId.Value, axTraceMessageEvent.AxUserName.ASCII, axTraceMessageEvent.AxMsg);
                                AosInstances.instance.getServer(axTraceMessageEvent.processId).pushEvent(axTraceMessageEvent);
                            }
                            break;
                        case 68:
                            {
                                var axTraceComponentMessageEvent = new AxTraceComponentMessageEvent(obj);
                                Logging.Log.Instance.Debug("AxTraceComponentMessageEvent {1} {0} --> {2}", axTraceComponentMessageEvent.AxSessionId.Value, axTraceComponentMessageEvent.AxUserName.ASCII, axTraceComponentMessageEvent.AxMsg);
                                AosInstances.instance.getServer(axTraceComponentMessageEvent.processId).pushEvent(axTraceComponentMessageEvent);
                            }
                            break;

                        default:
                            //Logging.Log.Instance.Debug(obj.ID + " " + obj.ProcessID + " " + isValidProcess(obj.ProcessID));
                            break;
                    }
                }
                catch (Exception e)
                {
                    Logging.Log.Instance.Debug(e, "EXC SERVER EVENT {1} // {0} // {2} {3} {4}", EpochNanoseconds.UtcNow.Value, obj.ushort_ID, e.Message, e.Source, e.StackTrace);
                }
            }
            else if (!isValidEvent)
            {
                switch (obj.ushort_ID)
                {
                    /* clients & altro, poi vedremo se serve rimettere qualche verifica su WMI */

                    //attivi solo con XppMarker 0x0000000000000001
                    case 51:
                        {
                            //"marchio" la sessione con questo evento, occhio che se non me la chiudono poi io la tento aperta fino a che la stessa id non viene riusata
                            //la marchio come tag in measuremnt ad eventi e come field nella ETW cosi filtro funziona!!!

                            //dimentica sopra e per ora gestisco ad eventi....
                            var axTransactionBeginEvent = new AxTransactionBeginEvent(obj);
                            Logging.Log.Instance.Debug("AxTransactionBeginEvent {1} {0} --> {2}", axTransactionBeginEvent.AxSessionId.Value, axTransactionBeginEvent.AxUserName.ASCII, axTransactionBeginEvent.AxMarkerName);
                            Statistics.CLIENT.SessionsMatrix.pushEvent(axTransactionBeginEvent);
                        }
                        break;
                    case 52:
                        {
                            var axTransactionEndEvent = new AxTransactionEndEvent(obj);
                            Logging.Log.Instance.Debug("AxTransactionEndEvent {1} {0} --> {2}", axTransactionEndEvent.AxSessionId.Value, axTransactionEndEvent.AxUserName.ASCII, axTransactionEndEvent.AxMarkerName);
                            Statistics.CLIENT.SessionsMatrix.pushEvent(axTransactionEndEvent);
                        }
                        break;

                    /*Traceinfo 0x0000000000000002*/
                    case 53:
                        {
                            var axTraceMessageEvent = new AxTraceMessageEvent(obj);
                            Statistics.CLIENT.SessionsMatrix.pushEvent(axTraceMessageEvent);
                        }
                        break;
                    case 68:
                        {
                            var axTraceComponentMessageEvent = new AxTraceComponentMessageEvent(obj);
                            Statistics.CLIENT.SessionsMatrix.pushEvent(axTraceComponentMessageEvent);
                        }
                        break;



                    //LOST EVENT
                    case 0xFFFF:
                        lostEventsCount++;
                        Logging.Log.Instance.Error("EVENT_LOST {0}", obj.nsTimeStampEpoch);
                        break;


                    default:
                        //Logging.Log.Instance.Error("UNMANAGED EVENT ID {0}: XML\n {1}", obj.ushort_ID, obj.ToString());
                        //Console.ReadKey(true);
                        break;
                }
                
            }
        }
    }
}