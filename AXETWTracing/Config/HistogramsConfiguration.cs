﻿namespace AxETWTracing.Config
{
    public class HistogramsConfiguration
    {
        public HistogramLevel[] histogramXPP { get; set; }
        public HistogramLevel[] histogramRPC { get; set; }
        public HistogramLevel[] histogramSQLStmt { get; set; }
        public HistogramLevel[] histogramSQLModelStoredProc { get; set; }
        public HistogramLevel[] histogramFetch { get; set; }
        public HistogramLevel[] histogramFetchCumu { get; set; }
    }

    public class HistogramLevel
    {
        public string label { get; set; }
        public int threshold { get; set; }
    }
}