﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxETWTracing.Config
{
    internal class Configuration
    {
        public WindowsLogging winlog { get; set; }
        public NatsConfiguration nats { get; set; }
        public TracingConfiguration tracing { get; set; }
        public ApplicationsConfiguration apps { get; set; }
        public HistogramsConfiguration histograms { get; set; }
    }
}
