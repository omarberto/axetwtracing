﻿namespace AxETWTracing.Config
{
    public class NatsConfiguration
    {
        public string address { get; set; }
        public string subject { get; set; }
        public int msSendCycle { get; set; }

        public string measurementName { get; set; } = "ETW";//verificare che funzioni davvero
        //public string hmeasurementName { get; set; } = "ETW";
    }
}