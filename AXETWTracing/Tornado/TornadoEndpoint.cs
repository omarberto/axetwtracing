﻿using Microsoft.Win32;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace AxETWTracing.Tornado
{
    class TornadoEndpoint
    {
        private string json_keepAliveHeader = string.Empty;
        private string json_keepAliveFormatBody = string.Empty;

        private string json_perfDataHeader = string.Empty;
        private string json_perfDataFormatBody = string.Empty;


        internal TornadoEndpoint(string ServiceName)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var assemblyName = assembly.GetName().Name;
            FileVersionInfo fileVersionInfo = FileVersionInfo.GetVersionInfo(assembly.Location);

            var IPGlobalProperties = System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties();
            string hostnameformat = AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.tornado.hostnameformat == "hostonly" ?
                "{0}"
                :
                "{0}.{1}";
            string hostName = string.Format(hostnameformat, IPGlobalProperties.HostName, IPGlobalProperties.DomainName);

            string instancesString = string.Empty;

            if (AX2012ETWtracing.Configuration.Configuration.config.ax2012.axaos)
            {
                if (false)
                {
                    //complicated version
                    try
                    {
                        string tmp = "[";
                        RegistryKey keyServices = Registry.LocalMachine.OpenSubKey("SYSTEM\\CurrentControlSet\\Services");
                        foreach (var subKeyName in keyServices.GetSubKeyNames())
                        {
                            Match match = Regex.Match(subKeyName, @"AOS60\$(\d{2})+$");
                            if (match.Success)
                            {
                                string aosInstance = match.Groups[1].Value;
                                if (AX2012ETWtracing.Configuration.Configuration.config.ax2012.validInstances.Length == 0 || AX2012ETWtracing.Configuration.Configuration.config.ax2012.validInstances.Contains(aosInstance))
                                    tmp += aosInstance + ",";
                            }
                        }
                        instancesString = tmp.Trim(',') + "]@AxServer instances";
                    }
                    catch
                    {
                        instancesString = (AX2012ETWtracing.Configuration.Configuration.config.ax2012.validInstances.Length == 0) ?
                           "AxServer@All instances"
                            :
                           ("AxServer@[" + AX2012ETWtracing.Configuration.Configuration.config.ax2012.validInstances.Aggregate("", (res, el) => res += el + ",").Trim(',') + "] instances");
                    }
                }
                else
                {
                    instancesString = (AX2012ETWtracing.Configuration.Configuration.config.ax2012.validInstances.Length == 0) ?
                       "AxServer@All instances"
                        :
                       ("AxServer@[" + AX2012ETWtracing.Configuration.Configuration.config.ax2012.validInstances.Aggregate("", (res, el) => res += el + ",").Trim(',') + "] instances");
                }
            }

            if (AX2012ETWtracing.Configuration.Configuration.config.ax2012.axclient)
                instancesString = "AxClient";

            //keepalive id = 1
            using (var stream = assembly.GetManifestResourceStream(assemblyName + ".Tornado.keepAliveHeader.jfmt"))
            {
                using (var reader = new StreamReader(stream))
                {
                    var jsonFormatHeader = reader.ReadToEnd();
                    this.json_keepAliveHeader = string.Format(jsonFormatHeader
                        , hostName
                        , fileVersionInfo.FileMajorPart, fileVersionInfo.FileMinorPart, fileVersionInfo.FileBuildPart
                        , instancesString
                        , ServiceName);
                }
            }
            using (var stream = assembly.GetManifestResourceStream(assemblyName + ".Tornado.keepAliveBody.jfmt"))
            {
                using (var reader = new StreamReader(stream))
                {
                    this.json_keepAliveFormatBody = reader.ReadToEnd();
                }
            }

            //perfdata id = 2
            using (var stream = assembly.GetManifestResourceStream(assemblyName + ".Tornado.perfDataHeader.jfmt"))
            {
                using (var reader = new StreamReader(stream))
                {
                    var jsonFormatHeader = reader.ReadToEnd();
                    this.json_perfDataHeader = string.Format(jsonFormatHeader
                        , hostName
                        , instancesString
                        , ServiceName);
                }
            }
            using (var stream = assembly.GetManifestResourceStream(assemblyName + ".Tornado.perfDataBody.jfmt"))
            {
                using (var reader = new StreamReader(stream))
                {
                    this.json_perfDataFormatBody = reader.ReadToEnd();
                }
            }
        }

        public static bool active = true;
        internal void DoWork()
        {
            //TODO: only if runs as service

            System.Diagnostics.Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();


            int elapsedSeconds = 0;

            while (active)
            {
                //chiedere alessandro se questo output ha qualocosa di predefinito come subject o se devo aggiungere in config
                elapsedSeconds++;
                if (elapsedSeconds % AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.tornado.keepaliveInterval == 0)
                {
                    //keepalive
                    string json = this.json_keepAliveHeader + string.Format(this.json_keepAliveFormatBody
                        , EpochNanoseconds.UtcNow.Value / 1000000L);

                    Logging.Log.Instance.Debug(json);
                    NatsClient.Instance["TORNADO"].PublishMessage(AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.tornado.topic, json);
                }
                
                if (elapsedSeconds % AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.tornado.monitordataInterval == 0)
                {
                    //perfdata
                    TornadoEnpointData interval_values = TornadoEnpointData.interval_values;
                    
                    int exit_status = interval_values.number_of_events_lost < 1 ? 0 : 2;

                    string json = this.json_perfDataHeader + string.Format(this.json_perfDataFormatBody
                        , EpochNanoseconds.UtcNow.Value / 1000000L
                        , interval_values.number_of_events_read
                        , interval_values.number_of_events_processed
                        , interval_values.number_of_measurments_sent_to_nats
                        , interval_values.number_of_events_lost, 1, 1 //fixed limits
                        , exit_status);

                    Logging.Log.Instance.Debug(json);
                    NatsClient.Instance["TORNADO"].PublishMessage(AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.tornado.topic, json);
                }

                long deltaMilliseconds = 1000L - stopwatch.ElapsedMilliseconds;
                if (deltaMilliseconds > 0L)
                {
                    Thread.Sleep(Convert.ToInt32(deltaMilliseconds));
                }
                stopwatch.Reset();
            }
            stopwatch.Stop();
        }
        internal void Test()
        {
            {
                //keepalive
                string json = this.json_keepAliveHeader + string.Format(this.json_keepAliveFormatBody
                    , EpochNanoseconds.UtcNow.Value / 1000000L);

                Logging.Log.Instance.Debug(json);
                NatsClient.Instance["TORNADO"].PublishMessage(AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.tornado.topic, json);
            }
            
            {
                //perfdata
                TornadoEnpointData interval_values = TornadoEnpointData.interval_values;

                int exit_status = interval_values.number_of_events_lost < 1 ? 0 : 2;

                string json = this.json_perfDataHeader + string.Format(this.json_perfDataFormatBody
                    , EpochNanoseconds.UtcNow.Value / 1000000L
                    , interval_values.number_of_events_read
                    , interval_values.number_of_events_processed
                    , interval_values.number_of_measurments_sent_to_nats
                    , interval_values.number_of_events_lost, 1, 1 //fixed limits
                    , exit_status);

                Logging.Log.Instance.Debug(json);
                NatsClient.Instance["TORNADO"].PublishMessage(AX2012ETWtracing.Configuration.Configuration.config.ax2012.output.tornado.topic, json);
            }
        }
    }

    class TornadoEnpointData
    {
        internal static TornadoEnpointData values { set; get; } = new TornadoEnpointData();
        private static TornadoEnpointData old_values = new TornadoEnpointData();
        internal static TornadoEnpointData interval_values
        {
            get
            {
                int number_of_events_read = values.number_of_events_read - old_values.number_of_events_read;
                old_values.number_of_events_read = values.number_of_events_read;
                int number_of_events_processed = values.number_of_events_processed - old_values.number_of_events_processed;
                old_values.number_of_events_processed = values.number_of_events_processed;
                int number_of_measurments_sent_to_nats = values.number_of_measurments_sent_to_nats - old_values.number_of_measurments_sent_to_nats;
                old_values.number_of_measurments_sent_to_nats = values.number_of_measurments_sent_to_nats;
                int number_of_events_lost = values.number_of_events_lost - old_values.number_of_events_lost;
                old_values.number_of_events_lost = values.number_of_events_lost;
                
                return new TornadoEnpointData()
                {
                    number_of_events_read = number_of_events_read,
                    number_of_events_processed = number_of_events_processed,
                    number_of_measurments_sent_to_nats = number_of_measurments_sent_to_nats,
                    number_of_events_lost = number_of_events_lost
                };
            }
        }

        internal int number_of_events_read = 0;
        internal int number_of_events_processed = 0;
        internal int number_of_events_lost = 0;
        internal int number_of_measurments_sent_to_nats = 0;
        internal ConcurrentDictionary<string,int> number_of_running_ = new ConcurrentDictionary<string, int>();
    }
}
