﻿using System;
using System.Runtime.InteropServices;

namespace AxETWTracing.Statistics
{
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    public struct AxInstanceSessionUserDBSpidTh
    {
        public AxServerInstance Instance;
        public AxSessionId Session;
        public AxUserName User;
        public AxConDBSpid DBSpid;
        public int ThID;

        //public fixed byte Value[101];
        public override string ToString()
        {
            return string.Format("Instance {0} Session {1} User {2}", Instance.Value, Session.Value, User.Value, DBSpid.Value, ThID);
        }
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    public struct AxInstanceUserSession
    {
        public AxServerInstance Instance;
        public AxUserName User;
        public AxSessionId Session;

        //public fixed byte Value[101];
        public override string ToString()
        {
            return string.Format("Instance {0} User {1} Session {2}", Instance.Value, User.Value, Session.Value);
        }
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    public struct AxComputerSessionUser
    {
        public AxComputer Computer;
        public AxComputer Server;//Non ho id istanza
        public AxSessionId Session;
        public AxUserName User;

        //public fixed byte Value[101];
        public override string ToString()
        {
            return string.Format("Computer {0} Session {1} User {2}", Computer.Value, Session.Value, User.Value);
        }
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    public struct AxServerInstance : IEquatable<AxServerInstance>
    {
        [MarshalAs(UnmanagedType.U2)]
        public ushort InstanceNumber;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 40)]
        public string ServerName;

        public string Value
        {
            get { return string.Format("{0:D2}@{1}", InstanceNumber, ServerName); }
        }

        public bool Equals(AxServerInstance other)
        {
            //mettere via settings il test o meno su server name
            return this.InstanceNumber == other.InstanceNumber;
        }

        public override int GetHashCode()
        {
            //questo va rivisto
            return InstanceNumber.GetHashCode();
        }
    }
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    public struct AxComputer : IEquatable<AxComputer>
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 8)]
        public string Value;

        public unsafe bool Equals(AxComputer other)
        {
            int len = Value.Length;
            if (other.Value.Length != len) return false;

            fixed (char* p1 = Value) fixed (char* p2 = other.Value)
            {
                char* a = p1;
                char* b = p2;

                while (len > 0)
                {
                    if (*a != *b) return false;
                    a++;
                    b++;
                    len--;
                }
            }
            return true;
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
    }
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    public struct AxSessionId : IEquatable<AxSessionId>
    {
        [MarshalAs(UnmanagedType.I4)]
        public int Value;

        public bool Equals(AxSessionId other)
        {
            return this.Value == other.Value;
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
    }
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    public struct AxUserName : IEquatable<AxUserName>
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 8)]
        public string Value;

        public unsafe bool Equals(AxUserName other)
        {
            int len = Value.Length;
            if (other.Value.Length != len) return false;

            fixed (char* p1 = Value) fixed (char* p2 = other.Value)
            {
                char* a = p1;
                char* b = p2;

                while (len > 0)
                {
                    if (*a != *b) return false;
                    a++;
                    b++;
                    len--;
                }
            }
            return true;
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public string ASCII
        {
            get
            {
                string retvalue = string.Empty;
                for (int i = 0; i < Value.Length; i++)
                {
                    switch (Value[i])
                    {
                        case 'Ä':
                            retvalue += 'A';
                            break;
                        case 'ä':
                            retvalue += 'a';
                            break;
                        case 'Ë':
                            retvalue += 'E';
                            break;
                        case 'ë':
                            retvalue += 'e';
                            break;
                        case 'Õ':
                            retvalue += 'O';
                            break;
                        case 'õ':
                            retvalue += 'o';
                            break;
                        case 'Ö':
                            retvalue += 'O';
                            break;
                        case 'ö':
                            retvalue += 'o';
                            break;
                        case 'Ü':
                            retvalue += 'U';
                            break;
                        case 'ü':
                            retvalue += 'u';
                            break;
                        default:
                            retvalue += Value[i];
                            break;
                    }
                }
                return retvalue;
            }
        }
    }

    //public class AxInstanceSessionUserValues
    //{
    //    public string AxInstance;
    //    public int AxSession;
    //    public string AxUser;
    //}

    internal static class FixedStringManager
    {
        public unsafe static void SetString(string src, char* dst)
        {
            fixed (char* bPtr = src)
            {
                var j = 0;
                while (j < src.Length)
                {
                    *(dst++) = *(bPtr + j++);
                }
            }
        }
        public unsafe static string GetString(char* ServerName, int Length)
        {
            char[] chars = new char[Length];
            int cnt = 0;
            unsafe
            {
                char* srcPtr = ServerName;
                fixed (char* bPtr = chars)
                {
                    var j = 0;
                    while (j < Length)
                    {
                        if (*(srcPtr) == 0) break;
                        *(bPtr + j++) = *(srcPtr++);
                        cnt++;
                    }
                }
            }

            return new string(chars, 0, cnt);
        }
    }
}