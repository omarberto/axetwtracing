﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxETWTracing.Statistics.RPC
{
    internal class CircularBufferStats
    {
        private const int sendInterval = 60000;

        private static readonly Dictionary<long, Dictionary<AxServerInstance, Dictionary<AxSessionId, Dictionary<AxUserName, RPCStatistic>>>> _instances = new Dictionary<long, Dictionary<AxServerInstance, Dictionary<AxSessionId, Dictionary<AxUserName, RPCStatistic>>>>();
        
        public static Dictionary<AxInstanceSessionUser, SendDurationStatistics> RPC
        {
            get
            {
                return getValues(EpochNanoseconds.UtcNow.Value).ToDictionary(item => item.Key, item => item.Value);
            }
        }

        private static IEnumerable<KeyValuePair<AxInstanceSessionUser, SendDurationStatistics>> getValues(long nsTimestamp)
        {
            long tsKeyMax = nsTimestamp / sendInterval - 1;//cerca intervallo precendente

            var retvalue = new List<KeyValuePair<AxInstanceSessionUser, SendDurationStatistics>>();

            for (long tsKey = tsKeyMax;; tsKey--)
            {
                Dictionary<AxServerInstance, Dictionary<AxSessionId, Dictionary<AxUserName, RPCStatistic>>> intInstance;
                if (!_instances.TryGetValue(tsKey, out intInstance)) break;

                retvalue.AddRange(intInstance.SelectMany(k1 => k1.Value.SelectMany(k2 => k2.Value.Select(k3 => new KeyValuePair<AxInstanceSessionUser, SendDurationStatistics>(
                    new AxInstanceSessionUser() { Instance = k1.Key, Session = k2.Key, User = k3.Key },
                    new SendDurationStatistics(k3.Value)
                )))));

                _instances.Remove(tsKey);
            }
            return retvalue;
        }

        public static RPCStatistic GetInstance(long nsTimestamp, AxInstanceSessionUser id)
        {
            long tsKey = nsTimestamp / sendInterval;

            Dictionary<AxServerInstance, Dictionary<AxSessionId, Dictionary<AxUserName, RPCStatistic>>> intInstance;
            if (!_instances.TryGetValue(tsKey, out intInstance)) intInstance = _instances[tsKey] = new Dictionary<AxServerInstance, Dictionary<AxSessionId, Dictionary<AxUserName, RPCStatistic>>>();

            Dictionary<AxSessionId, Dictionary<AxUserName, RPCStatistic>> value1;
            if (!intInstance.TryGetValue(id.Instance, out value1)) value1 = intInstance[id.Instance] = new Dictionary<AxSessionId, Dictionary<AxUserName, RPCStatistic>>();

            Dictionary<AxUserName, RPCStatistic> value2;
            if (!value1.TryGetValue(id.Session, out value2)) value2 = value1[id.Session] = new Dictionary<AxUserName, RPCStatistic>();

            RPCStatistic value3;
            if (!value2.TryGetValue(id.User, out value3)) value3 = value2[id.User] = new RPCStatistic();

            return value3;
        }
    }
}
