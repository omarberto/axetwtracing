﻿using System;
using System.Collections.Generic;

namespace AxETWTracing.Statistics.RPC
{
    public interface IRPCStatistic
    {
        long nsTimeStampEpoch { get; } //--> per ora lo metto qui....

        long AxDuration_Total { get; }
        long AxServerDuration_Total { get; }
        long AxClientDuration_Total { get; }
        int callsCount { get; }
        int callsCountAll { get; }
        int callsDepth { get; }
        string[] labels { get; }
        int[] histogram { get; }
        long WholeDuration { get; }
        long AxServerBytesRecd { get; }
        long AxServerBytesSent { get; }

        int EventsCount_Control { get; }
    }
    public interface IParametricRPCStatistic
    {
        bool isClosed { get; }
        long AxDuration_Total { get; }

        long AxServerDuration_Total { get; }
        long AxClientDuration_Total { get; }

        int callsCount { get; }
        int callsDepth { get; }
        int callsCountAll { get; }

        IParametricHistogram histogram { get; }

        long WholeDuration { get; }

        long AxServerBytesRecd { get; }
        long AxServerBytesSent { get; }

        int EventsCount_Control { get; }
    }
}