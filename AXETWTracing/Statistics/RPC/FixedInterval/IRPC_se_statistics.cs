﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxETWTracing.Statistics.RPC.FixedInterval
{
    public interface IRPC_se_statistics
    {
        //le seguenti non sono vere se, nel senso che sono il risultato di un se, ma sono aggiornate solo con event end (quello che determina ts)
        bool isClosed { get; }//if==true indica che level=0 ha avuto end
        long AxDuration_Total { get; }
        int callsCount { get; }
        int callsCountAll { get; }
        long WholeDuration { get; }//indica durata chiamata rpc level=0 in corso
        IParametricHistogram histogram { get; }


        //le seguenti sono vere se, nel senso che sono aggiornate sia in start che end
        long AxServerDuration_Total { get; }
        long AxClientDuration_Total { get; }

    }
}
