﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxETWTracing.Statistics.RPC.FixedInterval
{
    public interface IRPC_ti_statistics
    {
        int callsDepth { get; }
        
        long AxServerBytesRecd { get; }
        long AxServerBytesSent { get; }
    }
}
