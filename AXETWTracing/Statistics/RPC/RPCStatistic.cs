﻿using Microsoft.Diagnostics.Tracing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AxETWTracing.Statistics.SQL;

namespace AxETWTracing.Statistics.RPC
{
    internal class RPCStatistic
    {
        //questo é l'inizio di ogni interval
        internal readonly long intervalStartTimeStamp;
        internal readonly long intervalEndTimeStamp;

        internal RPCStatistic(long tracingMessageTimeStamp)
        {
            this.intervalStartTimeStamp = (tracingMessageTimeStamp / 60000000000L) * 60000000000L;//poi trasformiamo
            this.intervalEndTimeStamp = this.intervalStartTimeStamp + 60000000000L;

            //serve per avere valore wholeduration abbastanza plausibile
            this.lastMessageTimeStamp = this.firstMessageTimeStamp = tracingMessageTimeStamp;
        }

        internal RPCStatistic Next()
        {
            var next = new RPCStatistic(intervalEndTimeStamp);

            next.lastAxNestLevel = lastAxNestLevel;
            next.nestLevelClosed = nestLevelClosed;

            //questi per forza, servono per i calcoli
            next.firstMessageTimeStamp = firstMessageTimeStamp;
            next.lastMessageTimeStamp = lastMessageTimeStamp;
            //maxNestLevel0Duration la voglio relativa all'intervallo quindi va a zero se non ci sono eventi

            return next;
        }

        internal RPCStatistic Next(long deltaTimeEpoch)
        {
            var next = new RPCStatistic((((intervalEndTimeStamp + deltaTimeEpoch) / 60000000000L) * 60000000000L) + 60000000000L);

            next.lastAxNestLevel = lastAxNestLevel;
            next.nestLevelClosed = nestLevelClosed;

            //questi per forza, servono per i calcoli
            next.firstMessageTimeStamp = firstMessageTimeStamp + deltaTimeEpoch;
            next.lastMessageTimeStamp = lastMessageTimeStamp + deltaTimeEpoch;
            //maxNestLevel0Duration la voglio relativa all'intervallo quindi va a zero se non ci sono eventi

            return next;
        }

        internal void Correct(long deltaTimeEpoch)
        {
            this.firstMessageTimeStamp += deltaTimeEpoch;
            this.lastMessageTimeStamp += deltaTimeEpoch;
        }

        /************************************************/

        //nestlevel0 in realtá é 1..., combaciano lato server e client
        private long lastAxNestLevel = 0;
        //segnala quando rpc level 1 é terminato, forse inutile
        public bool nestLevelClosed { private set; get; } = true;

        /************************************************/

        //begin messaggio di livello zero
        private long firstMessageTimeStamp;
        //ultimo messaggio
        private long lastMessageTimeStamp;
        private long maxNestLevel0Duration = 0;
        public long WholeDuration
        {
            get
            {
                if (nestLevelClosed) return maxNestLevel0Duration;
                
                //cosi ho maggiore dell'intervallo, che non é per forza ultimo
                var finalNestLevel0Duration = intervalEndTimeStamp - firstMessageTimeStamp;
                return (finalNestLevel0Duration > maxNestLevel0Duration) ? finalNestLevel0Duration : maxNestLevel0Duration;
            }
        }

        /************************************************/

        private int _EventsCount_Control;
        private int _callsCount;
        public int EventsCount_Control { get { return _EventsCount_Control; } }//equivale a callsDepth 100%
        public int callsCount { get { return _callsCount; } }

        /************************************************/

        public IParametricHistogram histogram { get; } = new ParametricHistogram(AX2012ETWtracing.Configuration.Configuration.config.ax2012.statistic.histograms.histogramRPC);

        /************************************************/

        private long _AxServerBytesRecd;
        private long _AxServerBytesSent;
        public long AxServerBytesRecd { get { return _AxServerBytesRecd; } }
        public long AxServerBytesSent { get { return _AxServerBytesSent; } }

        /************************************************/

        private long _AxDuration_Total;
        private long _AxClientDuration_Total;
        private long _AxServerDuration_Total;
        public long AxDuration_Total { get { return _AxDuration_Total; } }
        public long AxClientDuration_Total { get { return _AxClientDuration_Total; } }
        public long AxServerDuration_Total { get { return _AxServerDuration_Total; } }

        /************************************************/


        internal void addBeginEvent(Events.CSRoundTripBegin @event)
        {
            //primo evento rilevato trova lastAxNestLevel = 0
            //oppure l'evento precendente era un end livello server
            if (@event.AxNestLevel == 1)
            {
                nestLevelClosed = false;
                firstMessageTimeStamp = @event.nsTimeStampEpoch;
            }
            //altrimenti e se l'evento precendete era un end non dello stesso livello
            else if (lastAxNestLevel != @event.AxNestLevel)
            {
                //prima esaminiamo il caso di uno start tipo client
                if (@event.AxNestLevel % 2 == 0)
                    _AxServerDuration_Total += @event.nsTimeStampEpoch - lastMessageTimeStamp;
                //siamo im uno start di tipo server, ma nested dentro un evento client
                else
                    _AxClientDuration_Total += @event.nsTimeStampEpoch - lastMessageTimeStamp;
            }

            lastMessageTimeStamp = @event.nsTimeStampEpoch;
            lastAxNestLevel = @event.AxNestLevel;
        }
        internal void addEndEvent(Events.CSRoundTripEnd @event)
        {
            _EventsCount_Control++;

            if (@event.AxNestLevel == 1)
            {
                nestLevelClosed = true;
                _callsCount++;
            }

            //primo evento rilevato trova lastAxNestLevel = 0, quindi abbiamo perso lo start corrispondente
            //sotto non perdo nulla, il primo messagggio se é end darebbe duration zero in ogni caso, perdo qualcosa ma non é drammatico
            if (lastAxNestLevel == 0)
            {
                //firstMessageTimeStamp adesso é inizializzato con il primo messaggio della registrazione, che é piú corretto!!!
                //firstMessageTimeStamp = @event.nsTimeStampEpoch;
            }
            else
            {
                //end tipo client
                if (@event.AxNestLevel % 2 == 0)
                    _AxClientDuration_Total += @event.nsTimeStampEpoch - lastMessageTimeStamp;
                //end di tipo server
                else
                    _AxServerDuration_Total += @event.nsTimeStampEpoch - lastMessageTimeStamp;

                if (@event.AxNestLevel == 1)
                {
                    var duration0 = @event.nsTimeStampEpoch - firstMessageTimeStamp;
                    if (maxNestLevel0Duration < duration0) maxNestLevel0Duration = duration0;
                    _AxDuration_Total += duration0;
                    histogram.AddDuration(duration0);
                }
            }

            lastMessageTimeStamp = @event.nsTimeStampEpoch;
            lastAxNestLevel = @event.AxNestLevel;

            _AxServerBytesRecd += @event.AxServerBytesRecd;
            _AxServerBytesSent += @event.AxServerBytesSent;
        }

        /************************************************/

    }
}
