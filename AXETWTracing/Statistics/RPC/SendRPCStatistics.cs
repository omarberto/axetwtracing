﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AxETWTracing.Statistics.SQL;

namespace AxETWTracing.Statistics.RPC
{
    class SendRPCStatistics : IRPCStatistic
    {
        public long nsTimeStampEpoch { get; private set; }

        public SendRPCStatistics(long nsTimeStampEpoch)
        {
            this.nsTimeStampEpoch = nsTimeStampEpoch;
            this.lastWrite = DateTime.UtcNow;
        }

        public SendRPCStatistics(long nsTimeStampEpoch, IParametricRPCStatistic fromETW)
        {
            this.nsTimeStampEpoch = nsTimeStampEpoch;

            this.EventsCount_Control = fromETW.EventsCount_Control;

            this.WholeDuration = fromETW.WholeDuration;

            this.AxDuration_Total = fromETW.AxDuration_Total;
            this.AxServerDuration_Total = fromETW.AxServerDuration_Total;
            this.AxClientDuration_Total = fromETW.AxClientDuration_Total;
            this.AxServerBytesRecd = fromETW.AxServerBytesRecd;
            this.AxServerBytesSent = fromETW.AxServerBytesSent;

            this.callsCount = fromETW.callsCount;
            this.callsCountAll = fromETW.callsCountAll;
            this.callsDepth = fromETW.callsDepth;

            this.histogram = fromETW.histogram.ToArray();
            this.labels = fromETW.histogram.labels;

            this.lastWrite = DateTime.UtcNow;
        }
        public SendRPCStatistics(long nsTimeStampEpoch, IParametricRPCStatistic fromETW1, IParametricRPCStatistic fromETW2)
        {
            this.nsTimeStampEpoch = nsTimeStampEpoch;

            this.EventsCount_Control = fromETW2.EventsCount_Control - fromETW1.EventsCount_Control;

            this.WholeDuration = (fromETW2.WholeDuration==fromETW1.WholeDuration) ? 0 : fromETW2.WholeDuration;//é un cumulativo che poi si resetta da solo, se sono uguali non ci sono piú eventi e quindi evito di scrivere...

            this.AxDuration_Total = fromETW2.AxDuration_Total - fromETW1.AxDuration_Total;
            this.AxServerDuration_Total = fromETW2.AxServerDuration_Total - fromETW1.AxServerDuration_Total;
            this.AxClientDuration_Total = fromETW2.AxClientDuration_Total - fromETW1.AxClientDuration_Total;
            this.AxServerBytesRecd = fromETW2.AxServerBytesRecd - fromETW1.AxServerBytesRecd;
            this.AxServerBytesSent = fromETW2.AxServerBytesSent - fromETW1.AxServerBytesSent;

            this.callsCount = fromETW2.callsCount - fromETW1.callsCount;
            this.callsCountAll = fromETW2.callsCountAll - fromETW1.callsCountAll;
            this.callsDepth = fromETW2.callsDepth - fromETW1.callsDepth;

            this.labels = fromETW2.histogram.labels;
            this.histogram = fromETW2.histogram.ToArray();
            for (int i = 0; i < this.histogram.Length; i++) this.histogram[i] -= fromETW1.histogram.ToArray()[i];

            this.lastWrite = DateTime.UtcNow;
        }

        public long AxDuration_Total { get; private set; }

        public int callsCount { get; private set; }

        public int callsCountAll { get; private set; }

        public int callsDepth { get; private set; }

        public string[] labels { get; private set; }

        public int[] histogram { get; private set; }

        public long AxServerDuration_Total { get; private set; }

        public long AxClientDuration_Total { get; private set; }

        public long WholeDuration { get; private set; }

        public long AxServerBytesRecd { get; private set; }
        public long AxServerBytesSent { get; private set; }

        private DateTime lastWrite;

        private const long publishCycleTime = 60000000000L;

        internal SendRPCStatistics Increment(IParametricRPCStatistic value, int timeoutMinutes, out bool clear, out bool noChange)
        {
            this.WholeDuration = value.WholeDuration;

            clear = false;
            //forse basta uno
            noChange = this.callsCount == value.callsCount && this.callsDepth == value.callsDepth;
            if (noChange)
            {
                //value.isClosed
                clear = ((DateTime.UtcNow - lastWrite).TotalMinutes > timeoutMinutes) && value.isClosed;

                //if (clear)
                //{
                //    return null;
                //}
                this.nsTimeStampEpoch += publishCycleTime;
                return this;
            }
            var retValue = new SendRPCStatistics(nsTimeStampEpoch + publishCycleTime)
            {
                WholeDuration = this.WholeDuration,

                AxDuration_Total = value.AxDuration_Total - this.AxDuration_Total,
                AxServerDuration_Total = value.AxServerDuration_Total - this.AxServerDuration_Total,
                AxClientDuration_Total = value.AxClientDuration_Total - this.AxClientDuration_Total,
                AxServerBytesRecd = value.AxServerBytesRecd - this.AxServerBytesRecd,
                AxServerBytesSent = value.AxServerBytesSent - this.AxServerBytesSent,
                callsCount = value.callsCount - this.callsCount,
                callsCountAll = value.callsCountAll - this.callsCountAll,
                callsDepth = value.callsDepth - this.callsDepth,
                histogram = value.histogram.ToArray(),
                labels = value.histogram.labels
            };
            for (int i = 0; i < retValue.histogram.Length; i++) retValue.histogram[i] -= this.histogram[i];

            this.AxDuration_Total = value.AxDuration_Total;
            this.AxServerDuration_Total = value.AxServerDuration_Total;
            this.AxClientDuration_Total = value.AxClientDuration_Total;
            this.AxServerBytesRecd = value.AxServerBytesRecd;
            this.AxServerBytesSent = value.AxServerBytesSent;
            this.callsCount = value.callsCount;
            this.callsCountAll = value.callsCountAll;
            this.callsDepth = value.callsDepth;
            this.histogram = value.histogram.ToArray();

            this.lastWrite = DateTime.UtcNow;

            return retValue;
        }


        public int EventsCount_Control { get; private set; }
    }
}
