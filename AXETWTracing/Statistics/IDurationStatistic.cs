﻿using System.Collections.Generic;

namespace AxETWTracing.Statistics
{
    public interface IDurationStatistic
    {
        long AxDuration_Total { get; }
        long AxServerDuration_Total { get; }
        long AxClientDuration_Total { get; }
        int callsCount { get; }
        int callsCountAll { get; }
        int callsDepth { get; }
        string[] labels { get; }
        int[] histogram { get; }
        int WholeDurationSeconds { get; }
        long AxServerBytesRecd { get; }
        long AxServerBytesSent { get; }
    }
    public interface IParametricDurationStatistic
    {
        //long AxDuration_Total { get; }
        long AxServerDuration_Total { get; }
        long AxClientDuration_Total { get; }
        int callsCount { get; }
        int callsDepth { get; }
        int callsCountAll { get; }
        IParametricHistogram histogram { get; }

        int WholeDurationSeconds { get; }

        long AxServerBytesRecd { get; }
        long AxServerBytesSent { get; }
    }
}