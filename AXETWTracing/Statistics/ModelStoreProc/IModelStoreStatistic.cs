﻿using System;

namespace AxETWTracing.Statistics.ModelStoreProc
{
    public interface IModelStoreStatistic
    {
        long nsTimeStampEpoch { get; } //--> per ora lo metto qui....

        long Duration_Total { get; }
        long PrepDuration_Total { get; }
        int Count_Total { get; }
        string[] labels { get; }
        int[] histogram { get; }

        int EventsCount_Control { get; }
    }

    public interface IParametricModelStoreStatistic
    {
        long Duration_Total { get; }
        long PrepDuration_Total { get; }
        IParametricHistogram histogram { get; }
        int Count_Total { get; }

        int EventsCount_Control { get; }
    }
}