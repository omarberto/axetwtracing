﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Diagnostics.Tracing;
using AxETWTracing.Events;

namespace AxETWTracing.Statistics.ModelStoreProc
{
    internal class ModelStoreStatistic
    {
        //questo é l'inizio di ogni interval
        internal readonly long intervalStartTimeStamp;
        internal readonly long intervalEndTimeStamp;

        internal ModelStoreStatistic(long tracingMessageTimeStamp)
        {
            this.intervalStartTimeStamp = (tracingMessageTimeStamp / 60000000000L) * 60000000000L;//poi trasformiamo
            this.intervalEndTimeStamp = this.intervalStartTimeStamp + 60000000000L;
        }

        internal ModelStoreStatistic Next()
        {
            return new ModelStoreStatistic(intervalEndTimeStamp);
        }

        internal ModelStoreStatistic Next(long deltaTimeEpoch)
        {
            return new ModelStoreStatistic((((intervalEndTimeStamp + deltaTimeEpoch) / 60000000000L) * 60000000000L) + 60000000000L);
        }

        internal void Correct(long deltaTimeEpoch)
        {
        }

        /************************************************/

        public int EventsCount_Control { get { return _Count_Total; } }

        /************************************************/

        public IParametricHistogram histogram { get; } = new ParametricHistogram(AX2012ETWtracing.Configuration.Configuration.config.ax2012.statistic.histograms.histogramSQLModelStoredProc);

        /************************************************/

        private long _Duration_Total;
        private long _PrepDuration_Total;
        private int _Count_Total;
        public long Duration_Total { get { return _Duration_Total; } }
        public long PrepDuration_Total { get { return _PrepDuration_Total; } }
        public int Count_Total { get { return _Count_Total; } }

        /************************************************/

        //devo gestire svuotamento
        internal void addEvent(AxSqlStmtEvent obj)
        {
            this._Count_Total++;
            this._PrepDuration_Total += obj.AxPrepDuration;
            this._Duration_Total += obj.AxDuration;
            histogram.AddDuration(obj.AxPrepDuration + obj.AxDuration);
        }
    }
}
