﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxETWTracing.Statistics.ModelStoreProc
{
    internal class SendModelStoreStatistics : IModelStoreStatistic
    {
        public long nsTimeStampEpoch { get; private set; }

        private SendModelStoreStatistics(long nsTimeStampEpoch)
        {
            this.nsTimeStampEpoch = nsTimeStampEpoch;
            this.lastWrite = DateTime.UtcNow;
        }
        public SendModelStoreStatistics(long nsTimeStampEpoch, IParametricModelStoreStatistic fromETW)
        {
            this.nsTimeStampEpoch = nsTimeStampEpoch;

            this.EventsCount_Control = fromETW.EventsCount_Control;

            this.Duration_Total = fromETW.Duration_Total;
            this.PrepDuration_Total = fromETW.PrepDuration_Total;
            this.Count_Total = fromETW.Count_Total;
            this.histogram = fromETW.histogram.ToArray();
            this.labels = fromETW.histogram.labels;

            this.lastWrite = DateTime.UtcNow;
        }

        public SendModelStoreStatistics(long nsTimeStampEpoch, IParametricModelStoreStatistic fromETW1, IParametricModelStoreStatistic fromETW2)
        {
            this.nsTimeStampEpoch = nsTimeStampEpoch;

            this.EventsCount_Control = fromETW2.EventsCount_Control - fromETW1.EventsCount_Control;

            this.Duration_Total = fromETW2.Duration_Total - fromETW1.Duration_Total;
            this.PrepDuration_Total = fromETW2.PrepDuration_Total - fromETW1.PrepDuration_Total;
            this.Count_Total = fromETW2.Count_Total - fromETW1.Count_Total;
            this.labels = fromETW2.histogram.labels;
            this.histogram = fromETW2.histogram.ToArray();
            for (int i = 0; i < this.histogram.Length; i++) this.histogram[i] -= fromETW1.histogram.ToArray()[i];

            this.lastWrite = DateTime.UtcNow;
        }

        public long Duration_Total { get; private set; }
        public long PrepDuration_Total { get; private set; }
        public int Count_Total { get; private set; }
        public string[] labels { get; private set; }
        public int[] histogram { get; private set; }

        private DateTime lastWrite;

        private const long publishCycleTime = 60000000000L;

        internal SendModelStoreStatistics Increment(IParametricModelStoreStatistic value, int timeoutMinutes, out bool clear, out bool noChange)
        {
            clear = false;
            //forse basta uno
            noChange = this.Count_Total == value.Count_Total;
            if (noChange)
            {
                clear = (DateTime.UtcNow - lastWrite).TotalMinutes > timeoutMinutes;

                //if (clear)
                //{
                //    return null;
                //}
                this.nsTimeStampEpoch += publishCycleTime;
                return this;
            }

            var retValue = new SendModelStoreStatistics(nsTimeStampEpoch + publishCycleTime)
            {
                Duration_Total = value.Duration_Total - this.Duration_Total,
                PrepDuration_Total = value.PrepDuration_Total - this.PrepDuration_Total,
                Count_Total = value.Count_Total - this.Count_Total,
                histogram = value.histogram.ToArray(),
                labels = value.histogram.labels
            };
            for (int i = 0; i < retValue.histogram.Length; i++) retValue.histogram[i] -= this.histogram[i];
            
            this.Duration_Total = value.Duration_Total;
            this.PrepDuration_Total = value.PrepDuration_Total;
            this.Count_Total = value.Count_Total;
            this.histogram = value.histogram.ToArray();
            this.labels = value.histogram.labels;

            this.lastWrite = DateTime.UtcNow;

            return retValue;
        }


        public int EventsCount_Control { get; private set; }
    }
}
