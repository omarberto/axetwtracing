﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxETWTracing.Statistics
{
    internal class Histogram : IHistogram
    {
        private int[] values;

        public IEnumerator<int> GetEnumerator()
        {
            return values.AsEnumerable<int>().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return values.GetEnumerator();
        }

        private static readonly long[] DurationsLimits = new long[] { 50000000, 100000000, 200000000, 500000000, 1000000000, 2000000000 };
        private static readonly string[] DurationsTags = new string[] { "50ms", "100ms", "200ms", "500ms", "1sec", "2sec", "long" };
        public string getKey(int i)
        {
            return DurationsTags[i];
        }
        internal Histogram()
        {
            values = new int[DurationsLimits.Length + 1];
        }

        public void AddDuration(long Duration)
        {
            for (int i = 0; i < 1 + DurationsLimits.Length; i++)
            {
                if (i == DurationsLimits.Length || Duration < DurationsLimits[i])
                {
                    values[i]++;
                    break;
                }
            }
        }

        private void RemoveDuration(long Duration)
        {
            for (int i = 0; i < 1 + DurationsLimits.Length; i++)
            {
                if (i == DurationsLimits.Length || Duration < DurationsLimits[i])
                {
                    values[i]--;
                    break;
                }
            }
        }
        public void OverwriteDuration(long Duration, long addDuration)
        {
            RemoveDuration(Duration);
            AddDuration(addDuration);
        }

        public override string ToString()
        {
            return string.Join("|", values);
        }
    }
}
