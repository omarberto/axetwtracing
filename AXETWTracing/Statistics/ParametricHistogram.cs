﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace AxETWTracing.Statistics
{
    internal class ParametricHistogram : IParametricHistogram
    {
        private int[] values;
        public string[] labels { get; private set; }
        private AX2012ETWtracing.Configuration.HistogramLevel[] levels;

        public IEnumerator<int> GetEnumerator()
        {
            return values.AsEnumerable<int>().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return values.GetEnumerator();
        }

        //private static readonly long[] DurationsLimits = new long[] { 50000000, 100000000, 200000000, 500000000, 1000000000, 2000000000 };
        //private static readonly string[] DurationsTags = new string[] { "50ms", "100ms", "200ms", "500ms", "1sec", "2sec", "long" };
        public string getKey(int i)
        {
            return levels[i].label;
        }


        public ParametricHistogram(AX2012ETWtracing.Configuration.HistogramLevel[] levels)
        {
            this.levels = levels;
            labels = new string[levels.Length];
            for (int i = 0; i < levels.Length; i++)
                labels[i] = levels[i].label;

            values = new int[levels.Length];
        }

        public void AddDuration(long Duration)
        {
            for (int i = 0; i < levels.Length; i++)
            {
                if (i == levels.Length - 1 || Duration < levels[i].threshold)
                {
                    values[i]++;
                    break;
                }
            }
        }

        private void RemoveDuration(long Duration)
        {
            for (int i = 0; i < levels.Length; i++)
            {
                if (i == levels.Length - 1 || Duration < levels[i].threshold)
                {
                    values[i]--;
                    break;
                }
            }
        }
        public void OverwriteDuration(long Duration, long addDuration)
        {
            RemoveDuration(Duration);
            AddDuration(addDuration);
        }

        public override string ToString()
        {
            return string.Join("|", values);
        }

        public void ResetDuration(long v)
        {
            for (int i = 0; i < levels.Length; i++)
                values[i] = 0;
            AddDuration(v);
        }
    }

    //internal class SimpleParametricHistogram : IParametricHistogram
    //{
    //    private int[] values;
    //    private AX2012ETWtracing.Configuration.HistogramLevel[] levels;

    //    public IEnumerator<int> GetEnumerator()
    //    {
    //        return values.AsEnumerable<int>().GetEnumerator();
    //    }

    //    IEnumerator IEnumerable.GetEnumerator()
    //    {
    //        return values.GetEnumerator();
    //    }
        
    //    public string getKey(int i)
    //    {
    //        return levels[i].label;
    //    }

    //    public string[] labels { get; private set; }

    //    public SimpleParametricHistogram(IParametricHistogram hist)
    //    {
    //        values = hist.ToArray();
    //    }

    //    public void AddDuration(long Duration)
    //    {
    //    }
        
    //    public void OverwriteDuration(long Duration, long addDuration)
    //    {
    //    }

    //    public override string ToString()
    //    {
    //        return string.Join("|", values);
    //    }

    //    public void ResetDuration(long v)
    //    {
    //        for (int i = 0; i < levels.Length; i++)
    //            values[i] = 0;
    //        AddDuration(v);
    //    }
    //}
}
