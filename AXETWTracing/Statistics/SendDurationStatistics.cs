﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxETWTracing.Statistics
{
    class SendDurationStatistics : IDurationStatistic
    {
        private SendDurationStatistics()
        {

        }
        public SendDurationStatistics(IParametricDurationStatistic fromETW)
        {
            this.WholeDurationSeconds = fromETW.WholeDurationSeconds;

            this.AxDuration_Total = fromETW.AxClientDuration_Total + fromETW.AxServerDuration_Total;
            this.AxServerDuration_Total = fromETW.AxServerDuration_Total;
            this.AxClientDuration_Total = fromETW.AxClientDuration_Total;
            this.AxServerBytesRecd = fromETW.AxServerBytesRecd;
            this.AxServerBytesSent = fromETW.AxServerBytesSent;

            this.callsCount = fromETW.callsCount;
            this.callsCountAll = fromETW.callsCountAll;
            this.callsDepth = fromETW.callsDepth;

            this.histogram = fromETW.histogram.ToArray();
            this.labels = fromETW.histogram.labels;
            this.lastWrite = DateTime.UtcNow;
        }

        public long AxDuration_Total { get; private set; }

        public int callsCount { get; private set; }

        public int callsCountAll { get; private set; }

        public int callsDepth { get; private set; }

        public string[] labels { get; private set; }

        public int[] histogram { get; private set; }

        public long AxServerDuration_Total { get; private set; }

        public long AxClientDuration_Total { get; private set; }

        public int WholeDurationSeconds { get; private set; }
        public long AxServerBytesRecd { get; private set; }
        public long AxServerBytesSent { get; private set; }

        private DateTime lastWrite;

        internal SendDurationStatistics Increment(IParametricDurationStatistic value, out bool clear)
        {
            clear = false;
            //forse basta uno
            bool retvalue = this.callsCount == value.callsCount && this.callsDepth == value.callsDepth;
            if (retvalue)
            {
                clear = (DateTime.UtcNow - lastWrite).TotalMinutes > 60;
                if (clear) Logging.Log.Instance.Debug("CLEAR RPC");
                //clear = true;
                return null;
            }
            var valueAxDuration_Total = value.AxServerDuration_Total + value.AxClientDuration_Total;
            var retValue = new SendDurationStatistics()
            {
                AxDuration_Total = valueAxDuration_Total - this.AxDuration_Total,
                AxServerDuration_Total = value.AxServerDuration_Total - this.AxServerDuration_Total,
                AxClientDuration_Total = value.AxClientDuration_Total - this.AxClientDuration_Total,
                AxServerBytesRecd = value.AxServerBytesRecd - this.AxServerBytesRecd,
                AxServerBytesSent = value.AxServerBytesSent - this.AxServerBytesSent,
                callsCount = value.callsCount - this.callsCount,
                callsCountAll = value.callsCountAll - this.callsCountAll,
                callsDepth = value.callsDepth - this.callsDepth,
                histogram = value.histogram.ToArray(),
                labels = value.histogram.labels
            };
            for (int i = 0; i < retValue.histogram.Length; i++) retValue.histogram[i] -= this.histogram[i];

            this.WholeDurationSeconds = value.WholeDurationSeconds;

            this.AxDuration_Total = valueAxDuration_Total;
            this.AxServerDuration_Total = value.AxServerDuration_Total;
            this.AxClientDuration_Total = value.AxClientDuration_Total;
            this.AxServerBytesRecd = value.AxServerBytesRecd;
            this.AxServerBytesSent = value.AxServerBytesSent;
            this.callsCount = value.callsCount;
            this.callsCountAll = value.callsCountAll;
            this.callsDepth = value.callsDepth;
            this.histogram = value.histogram.ToArray();

            this.lastWrite = DateTime.UtcNow;

            return retValue;
        }
    }
}
