﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxETWTracing.Statistics
{
    public class SendStatistics
    {
        public AxServerInstance aosInstance { get; private set; }
        public AxSessionId axSession { get; private set; }
        public AxUserName axUserName { get; private set; }
        public long nsTimeStampEpoch { get; private set; }

        public SendStatistics(AxServerInstance aosInstance, AxSessionId axSession, AxUserName axUserName, long nsTimeStampEpoch)
        {
            this.aosInstance = aosInstance;
            this.axSession = axSession;
            this.axUserName = axUserName;
            this.nsTimeStampEpoch = nsTimeStampEpoch;
        }

        public int XPP_all_count { get; internal set; }
        public int XPP_nestlevel0_count { get; internal set; }
        public long XPP_nestlevel0_duration { get; internal set; }
        public long XPP_whole_duration { get; internal set; }
        public IParametricHistogram XPP_histogram { get; internal set; }

        public int RPC_all_count { get; internal set; }
        public int RPC_nestlevel0_count { get; internal set; }
        public long RPC_nestlevel0_duration { get; internal set; }
        public long RPC_server_duration { get; internal set; }
        public long RPC_client_duration { get; internal set; }
        public long RPC_whole_duration { get; internal set; }
        public long RPC_bytes_recd { get; internal set; }
        public long RPC_bytes_sent { get; internal set; }
        public IParametricHistogram RPC_histogram { get; internal set; }

        public int SQL_stmt_count { get; internal set; }
        public int stmtType_call { get; internal set; }
        public int stmtType_delete { get; internal set; }
        public int stmtType_insert { get; internal set; }
        public int stmtType_none { get; internal set; }
        public int stmtType_other { get; internal set; }
        public int stmtType_select { get; internal set; }
        public int stmtType_update { get; internal set; }
        public long SQL_stmt_duration { get; internal set; }
        public long SQL_stmt_preparation_duration { get; internal set; }
        public IParametricHistogram SQL_histogram { get; internal set; }

        public int SQL_modelstoredproc_count { get; internal set; }
        public long SQL_modelstoredproc_duration { get; internal set; }
        public long SQL_modelstoredproc_preparation_duration { get; internal set; }
        public IParametricHistogram SQL_modelstoredproc_histogram { get; internal set; }

        public int FETCH_fetches_count { get; internal set; }
        public long FETCH_duration { get; internal set; }
        public long FETCH_rows_count { get; internal set; }
        public long FETCH_fetchcumu_count { get; internal set; }
        public long FETCH_cumu_duration { get; internal set; }
        public IParametricHistogram FETCH_histogram { get; internal set; }
        public string AXTransaction { get; internal set; }
        public string clientType { get; internal set; }
        public object computerName { get; internal set; }
        public long nsClientTypeDuration { get; internal set; }
        public string batchRecId { get; internal set; }
        public string batchStatus { get; internal set; }
        public long nsBatchDuration { get; internal set; }
    }
}
