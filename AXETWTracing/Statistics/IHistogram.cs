﻿using System.Collections.Generic;

namespace AxETWTracing.Statistics
{
    public interface IHistogram : IEnumerable<int>
    {
        void AddDuration(long Duration);
        void OverwriteDuration(long axDuration, long addAxDuration);
        string getKey(int i);
    }
}