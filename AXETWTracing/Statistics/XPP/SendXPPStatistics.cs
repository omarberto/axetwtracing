﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxETWTracing.Statistics.XPP
{
    class SendXPPStatistics : IXPPStatistic
    {
        private SendXPPStatistics()
        {

        }
        public SendXPPStatistics(IParametricXPPStatistic fromETW)
        {
            this.WholeDuration = fromETW.WholeDuration;

            this.AxDuration_Total = fromETW.AxDuration_Total;

            this.callsCount = fromETW.callsCount;
            this.callsCountAll = fromETW.callsCountAll;
            this.callsDepth = fromETW.callsDepth;

            this.histogram = fromETW.histogram.ToArray();
            this.labels = fromETW.histogram.labels;
            this.lastWrite = DateTime.UtcNow;
        }

        public long AxDuration_Total { get; private set; }

        public int callsCount { get; private set; }

        public int callsCountAll { get; private set; }

        public int callsDepth { get; private set; }

        public string[] labels { get; private set; }

        public int[] histogram { get; private set; }

        public long WholeDuration { get; private set; }

        private DateTime lastWrite;

        internal SendXPPStatistics Increment(IParametricXPPStatistic value, out bool clear)
        {
            clear = false;
            //forse basta uno
            bool retvalue = this.callsCount == value.callsCount && this.callsDepth == value.callsDepth;
            if (retvalue)
            {
                clear = ((DateTime.UtcNow - lastWrite).TotalMinutes > 60) && value.isClosed;
                if (clear) Logging.Log.Instance.Debug("CLEAR RPC");
                //clear = true;
                return null;
            }
            var retValue = new SendXPPStatistics()
            {
                AxDuration_Total = value.AxDuration_Total - this.AxDuration_Total,
                callsCount = value.callsCount - this.callsCount,
                callsCountAll = value.callsCountAll - this.callsCountAll,
                callsDepth = value.callsDepth - this.callsDepth,
                histogram = value.histogram.ToArray(),
                labels = value.histogram.labels
            };
            for (int i = 0; i < retValue.histogram.Length; i++) retValue.histogram[i] -= this.histogram[i];

            this.WholeDuration = value.WholeDuration;

            this.AxDuration_Total = value.AxDuration_Total;
            this.callsCount = value.callsCount;
            this.callsCountAll = value.callsCountAll;
            this.callsDepth = value.callsDepth;
            this.histogram = value.histogram.ToArray();

            this.lastWrite = DateTime.UtcNow;

            return retValue;
        }
    }
}
