﻿using Microsoft.Diagnostics.Tracing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxETWTracing.Statistics.XPP
{
    internal class XPPStatistic
    {
        //questo é l'inizio di ogni interval
        internal readonly long intervalStartTimeStamp;
        internal readonly long intervalEndTimeStamp;

        internal XPPStatistic(long tracingMessageTimeStamp)
        {
            this.intervalStartTimeStamp = (tracingMessageTimeStamp / 60000000000L) * 60000000000L;//poi trasformiamo
            this.intervalEndTimeStamp = this.intervalStartTimeStamp + 60000000000L;

            //serve per avere valore wholeduration abbastanza plausibile
            this.lastMessageTimeStamp = this.firstMessageTimeStamp = tracingMessageTimeStamp;
        }

        internal XPPStatistic Next()
        {
            var next = new XPPStatistic(intervalEndTimeStamp);

            next.lastAxNestLevel = lastAxNestLevel;
            next.nestLevelClosed = nestLevelClosed;

            //questi per forza, servono per i calcoli
            //TODO: verificare, deltaTimeEpoch inserito seza checks
            next.firstMessageTimeStamp = firstMessageTimeStamp;
            next.lastMessageTimeStamp = lastMessageTimeStamp;
            //maxNestLevel0Duration la voglio relativa all'intervallo quindi va a zero se non ci sono eventi

            return next;
        }

        internal XPPStatistic Next(long deltaTimeEpoch)
        {
            var next = new XPPStatistic((((intervalEndTimeStamp + deltaTimeEpoch) / 60000000000L) * 60000000000L) + 60000000000L);

            next.lastAxNestLevel = lastAxNestLevel;
            next.nestLevelClosed = nestLevelClosed;

            //questi per forza, servono per i calcoli
            //TODO: verificare, deltaTimeEpoch inserito seza checks
            next.firstMessageTimeStamp = firstMessageTimeStamp + deltaTimeEpoch;
            next.lastMessageTimeStamp = lastMessageTimeStamp + deltaTimeEpoch;
            //maxNestLevel0Duration la voglio relativa all'intervallo quindi va a zero se non ci sono eventi

            return next;
        }

        internal void Correct(long deltaTimeEpoch)
        {
            this.firstMessageTimeStamp += deltaTimeEpoch;
            this.lastMessageTimeStamp += deltaTimeEpoch;
        }

        /************************************************/

        //nest level nell'XPP é calcolato, non é noto!!!!
        private long lastAxNestLevel = 0;
        //segnala quando xpp level 1 é terminato, forse inutile
        public bool nestLevelClosed { private set; get; } = true;

        /************************************************/

        //begin messaggio di livello zero
        private long firstMessageTimeStamp;
        //ultimo messaggio
        private long lastMessageTimeStamp;
        private long maxNestLevel0Duration = 0;
        public long WholeDuration
        {
            get
            {
                if (nestLevelClosed) return maxNestLevel0Duration;

                //cosi ho maggiore dell'intervallo, che non é per forza ultimo
                var finalNestLevel0Duration = intervalEndTimeStamp - firstMessageTimeStamp;
                return (finalNestLevel0Duration > maxNestLevel0Duration) ? finalNestLevel0Duration : maxNestLevel0Duration;
            }
        }

        /************************************************/

        private int _EventsCount_Control;
        private int _callsCount;
        public int EventsCount_Control { get { return _EventsCount_Control; } }//equivale a callsDepth 100%
        public int callsCount { get { return _callsCount; } }

        /************************************************/

        public IParametricHistogram histogram { get; } = new ParametricHistogram(AX2012ETWtracing.Configuration.Configuration.config.ax2012.statistic.histograms.histogramXPP);

        /************************************************/

        private long _AxDuration_Total;
        public long AxDuration_Total { get { return _AxDuration_Total; } }

        /************************************************/


        //segnala quando rpc level 1 é terminato
        public bool isClosed { private set; get; } = false;
        internal void addBeginEvent(Events.XppMethodBegin @event)
        {
            //primo evento rilevato trova lastAxNestLevel = 0
            //oppure l'evento precendente era un end livello server
            if (lastAxNestLevel == 0)
            {
                nestLevelClosed = false;//si potrebbe mettere sempre a false quando c'é un begin!!!
                firstMessageTimeStamp = @event.nsTimeStampEpoch;
            }

            lastMessageTimeStamp = @event.nsTimeStampEpoch;
            lastAxNestLevel++;
        }

        internal void addEndEvent(Events.XppMethodEnd @event)
        {
            _EventsCount_Control++;

            //primo evento rilevato trova lastAxNestLevel = 0, quindi abbiamo perso lo start corrispondente
            //io per XPP eviterei in toto istogramma, cosi non mi uccido di calcoli, e poi sinceramente ha poco significato
            if (lastAxNestLevel == 0)
            {
                lastAxNestLevel++;//avevo perso un begin

                var durationPrev = lastMessageTimeStamp - firstMessageTimeStamp;
                var durationAdd = @event.nsTimeStampEpoch - lastMessageTimeStamp;
                _AxDuration_Total += durationAdd;
                if (maxNestLevel0Duration < _AxDuration_Total) maxNestLevel0Duration = _AxDuration_Total; //whole duration fino a quando non siamo usciti dal primo loop di nesting coincide con nestlevel0
                histogram.ResetDuration(durationPrev + durationAdd);
            }
            else if (lastAxNestLevel == 1)
            {
                nestLevelClosed = true;
                _callsCount++;

                var duration0 = @event.nsTimeStampEpoch - firstMessageTimeStamp;
                if (maxNestLevel0Duration < duration0) maxNestLevel0Duration = duration0;
                _AxDuration_Total += duration0;
                histogram.AddDuration(duration0);
            }


            lastMessageTimeStamp = @event.nsTimeStampEpoch;
            lastAxNestLevel--;
        }
    }
}
