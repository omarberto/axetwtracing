﻿using System.Collections.Generic;

namespace AxETWTracing.Statistics.XPP
{
    public interface IXPPStatistic
    {
        long AxDuration_Total { get; }
        int callsCount { get; }
        int callsCountAll { get; }
        int callsDepth { get; }
        string[] labels { get; }
        int[] histogram { get; }
        long WholeDuration { get; }
    }
    public interface IParametricXPPStatistic
    {
        bool isClosed { get; }
        long AxDuration_Total { get; }
        int callsCount { get; }
        int callsDepth { get; }
        int callsCountAll { get; }
        IParametricHistogram histogram { get; }

        long WholeDuration { get; }

        long AxServerBytesRecd { get; }
        long AxServerBytesSent { get; }
    }
}