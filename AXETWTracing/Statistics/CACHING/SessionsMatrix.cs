﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AxETWTracing.Events;

namespace AxETWTracing.Statistics.CACHING
{
    internal class SessionsMatrix
    {
        internal static void pushEvent(TableLoading tableLoading)
        {
            /*
                <template tid="AOTTaskArgs">
                <data name="seqNum" inType="win:Int32"/>
                <data name="AxServerName" inType="win:UnicodeString"/>
                <data name="AxUserName" inType="win:UnicodeString"/>
                <data name="AxSessionId" inType="win:Int32"/>
                <data name="AxTableId" inType="win:Int64"/>
                <data name="AxTableName" inType="win:UnicodeString"/>
                <data name="AxLoadingStrategy" inType="win:UnicodeString"/>
                <data name="AxRequestType" inType="win:UnicodeString"/>
                <data name="AxUtilMode" inType="win:Int32"/>
                <data name="AxMethodList" inType="win:UnicodeString"/>
                <data name="AxUCBufferSize" inType="win:Int64"/>
                <data name="AxCBufferSize" inType="win:Int64"/>
                </template>
            */

            Logging.Log.Instance.Trace("Caching_TableLoading,SourceType={11},AxServerName={0},AxTableId={3},AxTableName={4} AxUserName=\"{1}\",AxSessionId=\"{2}\",AxLoadingStrategy=\"{5}\",AxRequestType=\"{6}\",AxUtilMode={7}i,AxMethodList=\"{8}\",AxUCBufferSize={9}i,AxCBufferSize={10}i",
                   tableLoading.AxServerName, tableLoading.AxUserName.ASCII, tableLoading.AxSessionId.Value,
                   tableLoading.AxTableId,
                   tableLoading.AxTableName,
                   tableLoading.AxLoadingStrategy,
                   tableLoading.AxRequestType,
                   tableLoading.AxUtilMode,
                   tableLoading.AxMethodList,
                   tableLoading.AxUCBufferSize,
                   tableLoading.AxCBufferSize,
                   (AosInstances.instance.verifyAosInstance(tableLoading.processId) ? "Ax32Serv" : "Ax32")
                   );

            new DataSend.DataSendInstance().sender.PublishEvent("Caching_TableLoading",
                   tableLoading.nsTimeStampEpoch,
                string.Format("AxUserName=\"{0}\",AxSessionId=\"{1}\",AxLoadingStrategy=\"{2}\",AxRequestType=\"{3}\",AxUtilMode={4}i,AxMethodList=\"{5}\",AxUCBufferSize={6}i,AxCBufferSize={7}i,ProcessId=\"{8}\"",
                   tableLoading.AxUserName.ASCII, tableLoading.AxSessionId.Value,
                   tableLoading.AxLoadingStrategy,
                   tableLoading.AxRequestType,
                   tableLoading.AxUtilMode,
                   tableLoading.AxMethodList,
                   tableLoading.AxUCBufferSize,
                   tableLoading.AxCBufferSize,
                   tableLoading.processId),
                string.Format(",SourceType={3},AxServerName={0},AxTableName={2}",
                   tableLoading.AxServerName,
                   tableLoading.AxTableId,
                   tableLoading.AxTableName,
                   (AosInstances.instance.verifyAosInstance(tableLoading.processId) ? "Ax32Serv" : "Ax32"))
                   );
        }

        internal static void pushEvent(ClassLoading classLoading)
        {
            /*
                 <template tid="AOTTask13Args">
                  <data name="seqNum" inType="win:Int32"/>
                  <data name="AxServerName" inType="win:UnicodeString"/>
                  <data name="AxUserName" inType="win:UnicodeString"/>
                  <data name="AxSessionId" inType="win:Int32"/>
                  <data name="AxClassId" inType="win:Int64"/>
                  <data name="AxClassName" inType="win:UnicodeString"/>
                  <data name="AxLoadingStrategy" inType="win:UnicodeString"/>
                  <data name="AxRequestType" inType="win:UnicodeString"/>
                  <data name="AxComponentName" inType="win:UnicodeString"/>
                  <data name="AxBufferSize" inType="win:Int64"/>
                 </template>
            */

            Logging.Log.Instance.Trace("Caching_ClassLoading,SourceType={9},AxServerName={0},AxClassId={3},AxClassName={4} AxUserName=\"{1}\",AxSessionId=\"{2}\",AxLoadingStrategy=\"{5}\",AxRequestType=\"{6}\",AxComponentName=\"{7}\",AxBufferSize={8}i",
                   classLoading.AxServerName, classLoading.AxUserName.ASCII, classLoading.AxSessionId.Value,
                   classLoading.AxClassId,
                   classLoading.AxClassName,
                   classLoading.AxLoadingStrategy,
                   classLoading.AxRequestType,
                   classLoading.AxComponentName,
                   classLoading.AxBufferSize,
                   (AosInstances.instance.verifyAosInstance(classLoading.processId) ? "Ax32Serv" : "Ax32")
                   );

            new DataSend.DataSendInstance().sender.PublishEvent("Caching_ClassLoading",
                   classLoading.nsTimeStampEpoch,
                string.Format("AxUserName=\"{0}\",AxSessionId=\"{1}\",AxLoadingStrategy=\"{2}\",AxRequestType=\"{3}\",AxComponentName=\"{4}\",AxBufferSize={5}i,ProcessId=\"{6}\"",
                   classLoading.AxUserName.ASCII, classLoading.AxSessionId.Value,
                   classLoading.AxLoadingStrategy,
                   classLoading.AxRequestType,
                   classLoading.AxComponentName,
                   classLoading.AxBufferSize,
                   classLoading.processId),
                string.Format(",SourceType={3},AxServerName={0},AxClassName={2}",
                   classLoading.AxServerName,
                   classLoading.AxClassId,
                   classLoading.AxClassName,
                   (AosInstances.instance.verifyAosInstance(classLoading.processId) ? "Ax32Serv" : "Ax32"))
                   );
        }

        internal static void pushEvent(DataAccessCCaching dataAccessCCaching)
        {
            /*
             <template tid="DataAccessTaskArgs">
              <data name="seqNum" inType="win:Int32"/>
              <data name="AxServerName" inType="win:UnicodeString"/>
              <data name="AxUserName" inType="win:UnicodeString"/>
              <data name="AxSessionId" inType="win:Int32"/>
              <data name="EventName" inType="win:UnicodeString"/>
              <data name="RootTableId" inType="win:Int64"/>
              <data name="RootTableName" inType="win:UnicodeString"/>
              <data name="RecId" inType="win:Int64"/>
             </template>
            */

            Logging.Log.Instance.Trace("Caching_DataAccessCCaching,SourceType={7},AxServerName={0},RootTableId={3},RootTableName={4},EventName={5} AxUserName=\"{1}\",AxSessionId=\"{2}\",RecId=\"{6}\"",
                   dataAccessCCaching.AxServerName, dataAccessCCaching.AxUserName.ASCII, dataAccessCCaching.AxSessionId.Value,
                   dataAccessCCaching.RootTableId,
                   dataAccessCCaching.RootTableName,
                   dataAccessCCaching.EventName,
                   dataAccessCCaching.RecId,
                   (AosInstances.instance.verifyAosInstance(dataAccessCCaching.processId) ? "Ax32Serv" : "Ax32")
                   );

            new DataSend.DataSendInstance().sender.PublishEvent("Caching_DataAccessCCaching",
                   dataAccessCCaching.nsTimeStampEpoch,
                string.Format("AxUserName=\"{0}\",AxSessionId=\"{1}\",RecId=\"{2}\",ProcessId=\"{3}\"",
                   dataAccessCCaching.AxUserName.ASCII, dataAccessCCaching.AxSessionId.Value,
                   dataAccessCCaching.RecId,
                   dataAccessCCaching.processId),
                string.Format(",SourceType={4},AxServerName={0},RootTableName={2},EventName={3}",
                   dataAccessCCaching.AxServerName,
                   dataAccessCCaching.RootTableId,
                   dataAccessCCaching.RootTableName,
                   dataAccessCCaching.EventName.Replace(" ", "\\ "),
                   (AosInstances.instance.verifyAosInstance(dataAccessCCaching.processId) ? "Ax32Serv" : "Ax32"))
                   );
        }

        internal static void pushEvent(DataAccessSCSCCaching dataAccessSCSCCaching)
        {
            /*
             <template tid="DataAccessTaskArgs">
              <data name="seqNum" inType="win:Int32"/>
              <data name="AxServerName" inType="win:UnicodeString"/>
              <data name="AxUserName" inType="win:UnicodeString"/>
              <data name="AxSessionId" inType="win:Int32"/>
              <data name="EventName" inType="win:UnicodeString"/>
              <data name="RootTableId" inType="win:Int64"/>
              <data name="RootTableName" inType="win:UnicodeString"/>
              <data name="RecId" inType="win:Int64"/>
             </template>
            */

            Logging.Log.Instance.Trace("Caching_DataAccessCCaching,SourceType={7},AxServerName={0},RootTableId={3},RootTableName={4},EventName={5} AxUserName=\"{1}\",AxSessionId=\"{2}\",RecId=\"{6}\"",
                   dataAccessSCSCCaching.AxServerName, dataAccessSCSCCaching.AxUserName.ASCII, dataAccessSCSCCaching.AxSessionId.Value,
                   dataAccessSCSCCaching.RootTableId,
                   dataAccessSCSCCaching.RootTableName,
                   dataAccessSCSCCaching.EventName,
                   dataAccessSCSCCaching.RecId,
                   (AosInstances.instance.verifyAosInstance(dataAccessSCSCCaching.processId) ? "Ax32Serv" : "Ax32")
                   );

            new DataSend.DataSendInstance().sender.PublishEvent("Caching_DataAccessCCaching",
                   dataAccessSCSCCaching.nsTimeStampEpoch,
                string.Format("AxUserName=\"{0}\",AxSessionId=\"{1}\",RecId=\"{2}\",ProcessId=\"{3}\"",
                   dataAccessSCSCCaching.AxUserName.ASCII, dataAccessSCSCCaching.AxSessionId.Value,
                   dataAccessSCSCCaching.RecId,
                   dataAccessSCSCCaching.processId),
                string.Format(",SourceType={4},AxServerName={0},RootTableName={2},EventName={3}",
                   dataAccessSCSCCaching.AxServerName,
                   dataAccessSCSCCaching.RootTableId,
                   dataAccessSCSCCaching.RootTableName,
                   dataAccessSCSCCaching.EventName.Replace(" ", "\\ "),
                   (AosInstances.instance.verifyAosInstance(dataAccessSCSCCaching.processId) ? "Ax32Serv" : "Ax32"))
                   );
        }

        internal static void pushEvent(AssemblyCacheElementBegin assemblyCacheElementBegin)
        {
            Logging.Log.Instance.Trace("Caching_AssemblyCacheElement,SourceType={0},EventType=Begin",
                   (AosInstances.instance.verifyAosInstance(assemblyCacheElementBegin.processId) ? "Ax32Serv" : "Ax32")
                   );

            new DataSend.DataSendInstance().sender.PublishEvent("Caching_AssemblyCacheElement",
                   assemblyCacheElementBegin.nsTimeStampEpoch, "Element =\"\"",
                string.Format(",SourceType={0},EventType=Begin",
                   (AosInstances.instance.verifyAosInstance(assemblyCacheElementBegin.processId) ? "Ax32Serv" : "Ax32"))
                   );
        }

        internal static void pushEvent(AssemblyCacheElement assemblyCacheElement)
        {
            Logging.Log.Instance.Trace("Caching_AssemblyCacheElement,SourceType={0},EventType=Event Element=\"{1}\"",
                   (AosInstances.instance.verifyAosInstance(assemblyCacheElement.processId) ? "Ax32Serv" : "Ax32"),
                   assemblyCacheElement.Element
                   );

            new DataSend.DataSendInstance().sender.PublishEvent("Caching_AssemblyCacheElement,EventType=Event",
                   assemblyCacheElement.nsTimeStampEpoch, 
                   string.Format("Element =\"{0}\"", 
                   assemblyCacheElement.Element),
                string.Format(",SourceType={0},EventType=Begin",
                   (AosInstances.instance.verifyAosInstance(assemblyCacheElement.processId) ? "Ax32Serv" : "Ax32"))
                   );
        }

        internal static void pushEvent(AssemblyCacheElementEnd assemblyCacheElementEnd)
        {
            Logging.Log.Instance.Trace("Caching_AssemblyCacheElement,SourceType={0},EventType=End",
                   (AosInstances.instance.verifyAosInstance(assemblyCacheElementEnd.processId) ? "Ax32Serv" : "Ax32")
                   );

            new DataSend.DataSendInstance().sender.PublishEvent("Caching_AssemblyCacheElement",
                   assemblyCacheElementEnd.nsTimeStampEpoch, "Element =\"\"",
                string.Format(",SourceType={0},EventType=End",
                   (AosInstances.instance.verifyAosInstance(assemblyCacheElementEnd.processId) ? "Ax32Serv" : "Ax32"))
                   );
        }
    }
}
