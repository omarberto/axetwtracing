﻿using System.Collections.Generic;

namespace AxETWTracing.Statistics
{
    public interface IParametricHistogram : IEnumerable<int>
    {
        void AddDuration(long Duration);
        void OverwriteDuration(long axDuration, long addAxDuration);
        string getKey(int i);
        string[] labels { get; }

        void ResetDuration(long v);
    }
}