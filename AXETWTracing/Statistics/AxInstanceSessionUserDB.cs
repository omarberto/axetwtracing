﻿using System;
using System.Runtime.InteropServices;

namespace AxETWTracing.Statistics
{
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    public struct AxInstanceSessionUserDB
    {
        public AxServerInstance Instance;
        public AxSessionId Session;
        public AxUserName User;
        public AxConDBSpid ConDBSpid;

        //public fixed byte Value[101];
        public override string ToString()
        {
            return string.Format("Instance {0} Session {1} User {2} ConnDBSpid {3}", Instance.Value, Session.Value, User.Value, ConDBSpid.Value);
        }
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    public struct AxConDBSpid : IEquatable<AxConDBSpid>
    {
        [MarshalAs(UnmanagedType.I8)]
        public long Value;

        public bool Equals(AxConDBSpid other)
        {
            return this.Value == other.Value;
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
    }

}