﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxETWTracing.Statistics
{
    internal class AxTransaction
    {
        public AxTransaction(string AxMarkerName, string AxMarkerGuid, long BeginTimestamp)
        {
            this.AxMarkerName = AxMarkerName;
            this.AxMarkerGuid = AxMarkerGuid;
            this.BeginTimestamp = BeginTimestamp;
        }

        public string AxMarkerName { get; private set; }
        public string AxMarkerGuid { get; private set; }


        public long BeginTimestamp { get; private set; }
        public long? EndTimestamp { get; private set; }
        public void Close(long EndTimestamp)
        {
            this.EndTimestamp = EndTimestamp;
        }
        public bool isClosed
        {
            get
            {
                return EndTimestamp.HasValue;
            }
        }
        //gestire open//close durante statistica --> il problema é se transaction é diversa...
    }
}
