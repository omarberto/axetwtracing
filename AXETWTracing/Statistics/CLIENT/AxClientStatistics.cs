﻿using AxETWTracing.WMI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AxETWTracing.Events;
using System.Collections.Concurrent;

namespace AxETWTracing.Statistics.CLIENT
{
    internal class AxClientStatistics
    {

        private static readonly Lazy<AxClientStatistics> lazy = new Lazy<AxClientStatistics>(() => new AxClientStatistics());
        public static AxClientStatistics instance { get { return lazy.Value; } }

        private AxClientStatistics()
        {
        }
        //processo,dati
        private ConcurrentDictionary<int, AxClientData> _axProcesses = new ConcurrentDictionary<int, AxClientData>();

        internal void addEvent(AxTransactionBeginEvent axTransactionBeginEvent)
        {
            if (!_axProcesses.ContainsKey(axTransactionBeginEvent.processId))
            {
                if (_axProcesses.TryAdd(axTransactionBeginEvent.processId, new AxClientData(axTransactionBeginEvent.processId, axTransactionBeginEvent.nsTimeStampEpoch, axTransactionBeginEvent.AxSessionId)))
                {
                    _axProcesses[axTransactionBeginEvent.processId].BeginTransaction(axTransactionBeginEvent.AxMarkerName, axTransactionBeginEvent.nsTimeStampEpoch);
                }
                //Logging.Log.Instance.Debug("AxTransactionEndEvent {1} {0} --> {2}", axTransactionBeginEvent.AxSessionId.Value, axTransactionBeginEvent.AxUserName.ASCII, axTransactionBeginEvent.AxMarkerName);

            }
            else
                _axProcesses[axTransactionBeginEvent.processId].BeginTransaction(axTransactionBeginEvent.AxMarkerName, axTransactionBeginEvent.nsTimeStampEpoch);
        }

        internal void addEvent(AxTransactionEndEvent axTransactionEndEvent)
        {
            if (_axProcesses.ContainsKey(axTransactionEndEvent.processId))
            {
                long duration;
                if (_axProcesses[axTransactionEndEvent.processId].EndTransaction(axTransactionEndEvent.AxMarkerName, axTransactionEndEvent.nsTimeStampEpoch, out duration))
                {
                    //NB: verificare a tutti i costi se questa cosa funziona!!!
                    new DataSend.DataSendInstance().sender.PublishEvent("CLIENT_AxTransactions", axTransactionEndEvent.nsTimeStampEpoch,
                        string.Format("AxComputer=\"{0}\",duration={1}i",
                            Environment.MachineName, duration
                            ),//se l'utente apre due clients in contemporanea potrei avere delle stranezze
                        string.Format(",AxTransactionName={0},AxUser={1},AxServer={2},AxSession={3}",
                            axTransactionEndEvent.AxMarkerName,
                            axTransactionEndEvent.AxUserName.ASCII, axTransactionEndEvent.AxServerName, axTransactionEndEvent.AxSessionId.Value));

                    //Logging.Log.Instance.Debug("AxTransactionEndEvent {1} {0} --> {2}", axTransactionEndEvent.AxSessionId.Value, axTransactionEndEvent.AxUserName.ASCII, axTransactionEndEvent.AxMarkerName);
                }
            }
        }

        internal void checkClients()
        {
            var toBeDeleted = _axProcesses.Keys.Except(AxClient.CheckProcesses()).ToList();
            foreach (int processId in toBeDeleted)
            {
                AxClientData tmp;
                _axProcesses.TryRemove(processId, out tmp);
                Logging.Log.Instance.Debug("checkClients _axProcesses.TryRemove {0}", processId);

            }
        }
    }
}
