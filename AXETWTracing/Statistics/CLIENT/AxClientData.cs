﻿using AxETWTracing.WMI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxETWTracing.Statistics.CLIENT
{
    class AxClientData
    {
        public AxClient process { get; private set; }
        public AxSessionId AxSession { get; private set; }

        private Dictionary<string, long> axtransactions = new Dictionary<string, long>();

        private int processId;
        private long nsTimeStampEpoch;

        internal AxClientData(int processId, long nsTimeStampEpoch, AxSessionId axSession)
        {
            //WMI lo interrogo solo per vedere se esite ancora il processo, non voglio aggiungere inutili lentezze
            this.processId = processId;
            this.nsTimeStampEpoch = nsTimeStampEpoch;

            this.process = new AxClient(processId, new EpochNanoseconds(nsTimeStampEpoch).dateTime);
        }

        internal void BeginTransaction(string axMarkerName, long nsTimeStampEpoch)
        {
            if (!axtransactions.ContainsKey(axMarkerName))
                this.axtransactions.Add(axMarkerName, nsTimeStampEpoch);
            else
                axtransactions[axMarkerName] = nsTimeStampEpoch;
        }

        internal bool EndTransaction(string axMarkerName, long nsTimeStampEpoch, out long duration)
        {
            if (axtransactions.ContainsKey(axMarkerName))
            {
                duration = nsTimeStampEpoch - axtransactions[axMarkerName];
                axtransactions.Remove(axMarkerName);
                return true;
            }
            duration = 0;
            return false;
        }
    }
}
