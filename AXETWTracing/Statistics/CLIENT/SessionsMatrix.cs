﻿using AxETWTracing.Monitor.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AxETWTracing.Events;

namespace AxETWTracing.Statistics.CLIENT
{
    internal class SessionsMatrix
    {
        public static int CLI_events_count_cumu;
        
        public static void pushClientEvent(Events.ClientEvent clientEvent, long nsTimeStampEpoch, int processId)
        {
            CLI_events_count_cumu++;

            new DataSend.DataSendInstance().sender.PublishEvent("CLIENT_EVENTS2", nsTimeStampEpoch,
                string.Format("FormOpen={0}i,FormClose={1}i,ControlClicked={2}i,AxComputer=\"{3}\",AxServer=\"{4}\",AxSession={5}i",
                    (clientEvent.eventType == Events.ClientEventType.FormOpen) ? 1 : 0,
                    (clientEvent.eventType == Events.ClientEventType.FormClose) ? 1 : 0,
                    (clientEvent.eventType == Events.ClientEventType.ControlClicked) ? 1 : 0,
                    clientEvent.AxComputerName, clientEvent.AxServerName, clientEvent.AxSessionId),//se l'utente apre due clients in contemporanea potrei avere delle stranezze
                string.Format(",AxUser={0},Control={1}", clientEvent.AxUserName, clientEvent.ControlName));


            Logging.Log.Instance.Debug("published CLIENT_EVENTS,AxUser={6},Control={7} FormOpen={0}i,FormClose={1}i,ControlClicked={2}i,AxComputer=\"{3}\",AxServer=\"{4}\",AxSession={5}i",
                (clientEvent.eventType == Events.ClientEventType.FormOpen) ? 1 : 0,
                    (clientEvent.eventType == Events.ClientEventType.FormClose) ? 1 : 0,
                    (clientEvent.eventType == Events.ClientEventType.ControlClicked) ? 1 : 0,
                    clientEvent.AxComputerName, clientEvent.AxServerName, clientEvent.AxSessionId
                    , clientEvent.AxUserName, clientEvent.ControlName
                );
        }

        public static int popClientEventStatistics(long nsTimeStampEpoch)
        {
            try
            {
                return System.Diagnostics.Process.GetProcessesByName("Ax32").Length;
            }
            catch
            {
                return -1;
            }
        }

        internal static void pushEvent(AxTraceMessageEvent axTraceMessageEvent)
        {
            new DataSend.DataSendInstance().sender.PublishEvent("CLIENT_EVENTS2", axTraceMessageEvent.nsTimeStampEpoch,
                string.Format("Message=\"{0}\",AxComputer=\"{1}\",AxServer=\"{2}\",AxSession={3}i,AxSessionS=\"{3}\"",
                    axTraceMessageEvent.AxMsg,
                    Environment.MachineName, axTraceMessageEvent.AxServerName, axTraceMessageEvent.AxSessionId.Value),//se l'utente apre due clients in contemporanea potrei avere delle stranezze
                string.Format(",AxUser={0}", axTraceMessageEvent.AxUserName.ASCII));
        }

        internal static void pushEvent(AxTraceComponentMessageEvent axTraceComponentMessageEvent)
        {
            new DataSend.DataSendInstance().sender.PublishEvent("CLIENT_EVENTS2", axTraceComponentMessageEvent.nsTimeStampEpoch,
                string.Format("Message=\"{0}::{1}\",AxComputer=\"{2}\",AxServer=\"{3}\",AxSession={4}i,AxSessionS=\"{4}\"",
                    axTraceComponentMessageEvent.AxComponent, axTraceComponentMessageEvent.AxMsg, 
                    Environment.MachineName, axTraceComponentMessageEvent.AxServerName, axTraceComponentMessageEvent.AxSessionId.Value),//se l'utente apre due clients in contemporanea potrei avere delle stranezze
                string.Format(",AxUser={0}", axTraceComponentMessageEvent.AxUserName.ASCII));
        }
        
        internal static void pushEvent(AxTransactionBeginEvent axTransactionBeginEvent)
        {
            new DataSend.DataSendInstance().sender.PublishEvent("CLIENT_EVENTS2", axTransactionBeginEvent.nsTimeStampEpoch,
                string.Format("AxTransactionName=\"{0}\",AxTransactionType=\"Begin\",AxComputer=\"{1}\",AxServer=\"{2}\",AxSession={3}i,AxSessionS=\"{3}\"",
                    axTransactionBeginEvent.AxMarkerName,
                    Environment.MachineName, axTransactionBeginEvent.AxServerName, axTransactionBeginEvent.AxSessionId.Value),//se l'utente apre due clients in contemporanea potrei avere delle stranezze
                string.Format(",AxUser={0}", axTransactionBeginEvent.AxUserName.ASCII));

            if (AX2012ETWtracing.Configuration.Configuration.config.ax2012.traceAxTransitions)
            {
                AxClientStatistics.instance.addEvent(axTransactionBeginEvent);
            }
        }
        internal static void pushEvent(AxTransactionEndEvent axTransactionEndEvent)
        {
            new DataSend.DataSendInstance().sender.PublishEvent("CLIENT_EVENTS2", axTransactionEndEvent.nsTimeStampEpoch,
                string.Format("AxTransactionName=\"{0}\",AxTransactionType=\"End\",AxComputer=\"{1}\",AxServer=\"{2}\",AxSession={3}i,AxSessionS=\"{3}\"",
                    axTransactionEndEvent.AxMarkerName,
                    Environment.MachineName, axTransactionEndEvent.AxServerName, axTransactionEndEvent.AxSessionId.Value),//se l'utente apre due clients in contemporanea potrei avere delle stranezze
                string.Format(",AxUser={0}", axTransactionEndEvent.AxUserName.ASCII));
            
            if (AX2012ETWtracing.Configuration.Configuration.config.ax2012.traceAxTransitions)
            {
                AxClientStatistics.instance.addEvent(axTransactionEndEvent);
            }
        }
    }
}
