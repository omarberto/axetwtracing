﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Diagnostics.Tracing;

namespace AxETWTracing.Statistics
{

    internal class DBConnectionStatistics
    {
        internal long firstMessageTimeStamp;

        internal long lastMessageTimeStamp;

        internal BindParameterStatistic bindParameter;
        internal SqlStmtStatistic sqlStmt;
        internal SqlRowFetchStatistic sqlRowFetch;
        internal SqlRowFetchCumuStatistic sqlRowFetchCumu;
    }
    internal class DBStatistics
    {
        internal int messagesCnt;
    }
    internal class BindParameterStatistic : DBStatistics
    {
    }

    internal class SqlDBtatistic : DBStatistics
    {
        internal int AxDuration_Total;
    }
    internal class SqlStmtStatistic : SqlDBtatistic
    {
        //potrei mettere tipo (select, insert usw)

        internal int AxPrepDuration_Total;
    }
    internal class SqlRowFetchStatistic : SqlDBtatistic
    {
    }
    internal class SqlRowFetchCumuStatistic : SqlRowFetchStatistic
    {
        internal int AxFetchCount_Total;
    }

    internal class MatrixRow
    {
        internal DurationStatistic RPCstatistics = new RPCStatistic();
        internal DurationStatistic XPPstatistics = new XPPStatistic();
        internal Dictionary<AxConDBSpid, DBConnectionStatistics> connectionStatistics = new Dictionary<AxConDBSpid, DBConnectionStatistics>();
    }
}
