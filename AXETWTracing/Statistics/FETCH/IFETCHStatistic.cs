﻿using System;

namespace AxETWTracing.Statistics.FETCH
{
    public interface IFETCHStatistic
    {
        long nsTimeStampEpoch { get; } //--> per ora lo metto qui....

        long AxFetchCountCumu_Total { get; }
        long AxFetchCumu_N_Total { get; }
        int AxFetchCount_Total { get; }
        long AxFetchDurationCumu_Total { get; }
        long AxFetchDuration_Total { get; }

        string[] labels { get; }
        int[] histogram { get; }

        //string[] labelsCumu { get; }
        //int[] histogramCumu { get; }

        int EventsCount_Control { get; }
    }
    public interface IParametricFETCHStatistic
    {
        long AxFetchCountCumu_Total { get; }
        long AxFetchCumu_N_Total { get; }
        int AxFetchCount_Total { get; }
        long AxFetchDurationCumu_Total { get; }
        long AxFetchDuration_Total { get; }

        IParametricHistogram histogram { get; }
        //IParametricHistogram histogramCumu { get; }

        int EventsCount_Control { get; }
    }
}