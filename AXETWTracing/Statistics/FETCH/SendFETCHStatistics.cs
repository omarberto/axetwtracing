﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxETWTracing.Statistics.FETCH
{
    class SendFETCHStatistics : IFETCHStatistic
    {
        public long nsTimeStampEpoch { get; private set; }

        private SendFETCHStatistics(long nsTimeStampEpoch)
        {
            this.nsTimeStampEpoch = nsTimeStampEpoch;
            this.lastWrite = DateTime.UtcNow;
        }
        public SendFETCHStatistics(long nsTimeStampEpoch, IParametricFETCHStatistic fromETW)
        {
            this.nsTimeStampEpoch = nsTimeStampEpoch;

            this.EventsCount_Control = fromETW.EventsCount_Control;

            this.AxFetchCount_Total = fromETW.AxFetchCount_Total;
            this.AxFetchDuration_Total = fromETW.AxFetchDuration_Total;
            this.labels = fromETW.histogram.labels;
            this.histogram = fromETW.histogram.ToArray();

            this.AxFetchCountCumu_Total = fromETW.AxFetchCountCumu_Total;
            this.AxFetchCumu_N_Total = fromETW.AxFetchCumu_N_Total;
            this.AxFetchDurationCumu_Total = fromETW.AxFetchDurationCumu_Total;
            //this.labelsCumu = fromETW.histogramCumu.labels;
            //this.histogramCumu = fromETW.histogramCumu.ToArray();

            this.lastWrite = DateTime.UtcNow;
        }
        public SendFETCHStatistics(long nsTimeStampEpoch, IParametricFETCHStatistic fromETW1, IParametricFETCHStatistic fromETW2)
        {
            this.nsTimeStampEpoch = nsTimeStampEpoch;

            this.EventsCount_Control = fromETW2.EventsCount_Control - fromETW1.EventsCount_Control;

            this.AxFetchCount_Total = fromETW2.AxFetchCount_Total - fromETW1.AxFetchCount_Total;
            this.AxFetchDuration_Total = fromETW2.AxFetchDuration_Total - fromETW1.AxFetchDuration_Total;
            this.labels = fromETW2.histogram.labels;
            this.histogram = fromETW2.histogram.ToArray();
            for (int i = 0; i < this.histogram.Length; i++) this.histogram[i] -= fromETW1.histogram.ToArray()[i];

            this.AxFetchCountCumu_Total = fromETW2.AxFetchCountCumu_Total - fromETW1.AxFetchCountCumu_Total;
            this.AxFetchCumu_N_Total = fromETW2.AxFetchCumu_N_Total - fromETW1.AxFetchCumu_N_Total;
            this.AxFetchDurationCumu_Total = fromETW2.AxFetchDurationCumu_Total - fromETW1.AxFetchDurationCumu_Total;

            this.lastWrite = DateTime.UtcNow;
        }

        public int AxFetchCount_Total { get; private set; }
        public long AxFetchDuration_Total { get; private set; }
        public string[] labels { get; private set; }
        public int[] histogram { get; private set; }

        public long AxFetchCountCumu_Total { get; private set; }
        public long AxFetchCumu_N_Total { get; private set; }
        public long AxFetchDurationCumu_Total { get; private set; }

        //public string[] labelsCumu { get; private set; }
        //public int[] histogramCumu { get; private set; }

        private DateTime lastWrite;

        private const long publishCycleTime = 60000000000L;
        internal SendFETCHStatistics Increment(IParametricFETCHStatistic value, int timeoutMinutes, out bool clear, out bool noChange)
        {
            clear = false;
            //forse basta uno
            noChange = this.AxFetchCount_Total == value.AxFetchCount_Total && this.AxFetchCountCumu_Total == value.AxFetchCountCumu_Total;
            if (noChange)
            {
                clear = (DateTime.UtcNow - lastWrite).TotalMinutes > timeoutMinutes;

                //if (clear)
                //{
                //    Logging.Log.Instance.Debug("SendFETCHStatistics Increment return null, clear is {0}", clear);
                //    return null;
                //}
                this.nsTimeStampEpoch += publishCycleTime;
                return this;
            }

            var retValue = new SendFETCHStatistics(nsTimeStampEpoch + publishCycleTime)
            {
                AxFetchCount_Total = value.AxFetchCount_Total - this.AxFetchCount_Total,
                AxFetchDuration_Total = value.AxFetchDuration_Total - this.AxFetchDuration_Total,
                histogram = value.histogram.ToArray(),
                labels = value.histogram.labels,

                AxFetchCountCumu_Total = value.AxFetchCountCumu_Total - this.AxFetchCountCumu_Total,
                AxFetchCumu_N_Total = value.AxFetchCumu_N_Total - this.AxFetchCumu_N_Total,
                AxFetchDurationCumu_Total = value.AxFetchDurationCumu_Total - this.AxFetchDurationCumu_Total,
                //histogramCumu = value.histogramCumu.ToArray(),
                //labelsCumu = value.histogramCumu.labels
            };
            for (int i = 0; i < retValue.histogram.Length; i++) retValue.histogram[i] -= this.histogram[i];
            //for (int i = 0; i < retValue.histogramCumu.Length; i++) retValue.histogramCumu[i] -= this.histogramCumu[i];

            this.AxFetchCount_Total = value.AxFetchCount_Total;
            this.AxFetchDuration_Total = value.AxFetchDuration_Total;
            this.histogram = value.histogram.ToArray();

            this.AxFetchCountCumu_Total = value.AxFetchCountCumu_Total;
            this.AxFetchCumu_N_Total = value.AxFetchCumu_N_Total;
            this.AxFetchDurationCumu_Total = value.AxFetchDurationCumu_Total;
            //this.histogramCumu = value.histogramCumu.ToArray();

            this.lastWrite = DateTime.UtcNow;

            return retValue;
        }


        public int EventsCount_Control { get; private set; }
    }
}
