﻿using System;
using Microsoft.Diagnostics.Tracing;
using AxETWTracing.Events;
using AxETWTracing.Statistics.SQL;

namespace AxETWTracing.Statistics.FETCH
{
    internal class FETCHStatistic
    {
        //questo é l'inizio di ogni interval
        internal readonly long intervalStartTimeStamp;
        internal readonly long intervalEndTimeStamp;

        internal FETCHStatistic(long tracingMessageTimeStamp)
        {
            this.intervalStartTimeStamp = (tracingMessageTimeStamp / 60000000000L) * 60000000000L;//poi trasformiamo
            this.intervalEndTimeStamp = this.intervalStartTimeStamp + 60000000000L;
        }

        internal FETCHStatistic Next()
        {
            return new FETCHStatistic(intervalEndTimeStamp);
        }

        internal FETCHStatistic Next(long deltaTimeEpoch)
        {
            return new FETCHStatistic((((intervalEndTimeStamp + deltaTimeEpoch) / 60000000000L) * 60000000000L) + 60000000000L);
        }

        internal void Correct(long deltaTimeEpoch)
        {
        }


        /************************************************/

        private int _EventsCount_Control;
        public int EventsCount_Control { get { return _EventsCount_Control; } }

        /************************************************/

        private long _AxFetchCountCumu_Total;
        private int _AxFetchCount_Total;
        private long _AxFetchDurationCumu_Total;
        private long _AxFetchCumu_N_Total;
        private long _AxFetchDuration_Total;

        public long AxFetchCountCumu_Total { get { return _AxFetchCountCumu_Total; } }
        public int AxFetchCount_Total { get { return _AxFetchCount_Total; } }
        public long AxFetchDurationCumu_Total { get { return _AxFetchDurationCumu_Total; } }
        public long AxFetchCumu_N_Total { get { return _AxFetchCumu_N_Total; } }
        public long AxFetchDuration_Total { get { return _AxFetchDuration_Total; } }

        /************************************************/

        internal void addEvent(SqlRowFetch sqlRowFetch)
        {
            this._EventsCount_Control++;

            this._AxFetchDuration_Total += sqlRowFetch.AxDuration;
            this._AxFetchCount_Total++;
            histogram.AddDuration(sqlRowFetch.AxDuration);
        }

        internal void addEvent(SqlRowFetchCumu sqlRowFetchCumu)
        {
            this._EventsCount_Control++;

            this._AxFetchDurationCumu_Total += sqlRowFetchCumu.AxDuration;
            this._AxFetchCountCumu_Total += sqlRowFetchCumu.AxFetchCount;
            this._AxFetchCumu_N_Total++;
            histogramCumu.AddDuration(sqlRowFetchCumu.AxDuration);
        }

        public IParametricHistogram histogram { get; } = new ParametricHistogram(AX2012ETWtracing.Configuration.Configuration.config.ax2012.statistic.histograms.histogramFetch);
        public IParametricHistogram histogramCumu { get; } = new ParametricHistogram(AX2012ETWtracing.Configuration.Configuration.config.ax2012.statistic.histograms.histogramFetchCumu);

    }
}