﻿using System;
using AxETWTracing.Statistics.RPC;
using AxETWTracing.Statistics.SQL;
using System.Collections.Generic;
using AxETWTracing.Statistics.FETCH;
using AxETWTracing.Statistics.ModelStoreProc;
using AxETWTracing.Statistics.XPP;

namespace AxETWTracing.Statistics
{

    internal class AxSessionData
    {
        private AxUserName AxUserName;
        public AxUserName User
        {
            get
            {
                return AxUserName;
            }
        }
        //il close a questo punto si riapre per le rpc ed eventuali xpp, la wholeduration in teoria risolve!!!

        public AxSessionData(AxUserName AxUserName, long tracingMessageTimeStamp)
        {
            this.AxUserName = AxUserName;

            this.XPPstatistics = new XPPStatistic(tracingMessageTimeStamp);
            this.RPCstatistics = new RPCStatistic(tracingMessageTimeStamp);
            this.SQLstatistics = new SQLStatistic(tracingMessageTimeStamp);
            this.MODELSTOREstatistics = new ModelStoreStatistic(tracingMessageTimeStamp);
            this.FETCHstatistics = new FETCHStatistic(tracingMessageTimeStamp);
        }

        internal bool checkUser(AxUserName axUserName)
        {
            return this.AxUserName.Equals(axUserName);
        }

        internal bool Next(AxServerInstance aosInstance, AxSessionId axSession, long intervalStartTimeStamp)
        {
            Close(aosInstance, axSession, intervalStartTimeStamp);

            //andrebbe gestito anche un timeout, cioé la lascio aperta n intervalli
            bool sessionClosed =
                this.XPPstatistics.nestLevelClosed
                && this.RPCstatistics.nestLevelClosed
                && this.SQLstatistics.SqlStmtCount_Total == 0
                && this.MODELSTOREstatistics.Count_Total == 0
                && this.FETCHstatistics.AxFetchCount_Total == 0
                && this.FETCHstatistics.AxFetchCumu_N_Total == 0;

            //if (!sessionClosed)
            {
                this.XPPstatistics = this.XPPstatistics.Next();
                this.RPCstatistics = this.RPCstatistics.Next();
                this.SQLstatistics = this.SQLstatistics.Next();
                this.MODELSTOREstatistics = this.MODELSTOREstatistics.Next();
                this.FETCHstatistics = this.FETCHstatistics.Next();
            }

            this.nsTimeStampEpochLast = this.nsTimeStampEpochLast + 60000000000L;

            //TODO: ripensare questa parte, che con l'if cosi non ha mai funzionato, vedere se timecorrection può portare problemi....
            if (sessionClosed)
            {
                //dopo 15 minuti la chiude in ogni caso
                sessionClosed = this.nsTimeStampEpochLast - intervalStartTimeStamp > 900000000000L;
            }

            return sessionClosed;
        }
        internal bool Next(AxServerInstance aosInstance, AxSessionId axSession, long intervalStartTimeStamp, long deltaTimeEpoch)
        {
            Close(aosInstance, axSession, intervalStartTimeStamp);

            //andrebbe gestito anche un timeout, cioé la lascio aperta n intervalli
            bool sessionClosed = 
                this.XPPstatistics.nestLevelClosed
                && this.RPCstatistics.nestLevelClosed
                && this.SQLstatistics.SqlStmtCount_Total == 0 
                && this.MODELSTOREstatistics.Count_Total == 0
                && this.FETCHstatistics.AxFetchCount_Total == 0
                && this.FETCHstatistics.AxFetchCumu_N_Total == 0;
            
            //if (!sessionClosed)
            {
                this.XPPstatistics = this.XPPstatistics.Next(deltaTimeEpoch);
                this.RPCstatistics = this.RPCstatistics.Next(deltaTimeEpoch);
                this.SQLstatistics = this.SQLstatistics.Next(deltaTimeEpoch);
                this.MODELSTOREstatistics = this.MODELSTOREstatistics.Next(deltaTimeEpoch);
                this.FETCHstatistics = this.FETCHstatistics.Next(deltaTimeEpoch);
            }


            this.nsTimeStampEpochStart += deltaTimeEpoch;
            this.nsTimeStampEpochLast += deltaTimeEpoch;

            return sessionClosed;
        }
        internal void Correct(AxServerInstance aosInstance, AxSessionId axSession, long deltaTimeEpoch)
        {
            this.XPPstatistics.Correct(deltaTimeEpoch);
            this.RPCstatistics.Correct(deltaTimeEpoch);
            this.SQLstatistics.Correct(deltaTimeEpoch);
            this.MODELSTOREstatistics.Correct(deltaTimeEpoch);
            this.FETCHstatistics.Correct(deltaTimeEpoch);

            this.nsTimeStampEpochStart += deltaTimeEpoch;
            this.nsTimeStampEpochLast += deltaTimeEpoch;
        }

        internal void Close(AxServerInstance aosInstance, AxSessionId axSession, long nsTimeStampEpoch)
        {
            
            var tmp = new SendStatistics(aosInstance, axSession, AxUserName, nsTimeStampEpoch);

            tmp.XPP_all_count = this.XPPstatistics.EventsCount_Control;
            tmp.XPP_nestlevel0_count = this.XPPstatistics.callsCount;
            tmp.XPP_nestlevel0_duration = this.XPPstatistics.AxDuration_Total;
            tmp.XPP_whole_duration = this.XPPstatistics.WholeDuration;
            tmp.XPP_histogram = this.XPPstatistics.histogram;//da verificare!!!

            tmp.RPC_all_count = this.RPCstatistics.EventsCount_Control;
            tmp.RPC_nestlevel0_count = this.RPCstatistics.callsCount;
            tmp.RPC_nestlevel0_duration = this.RPCstatistics.AxDuration_Total;
            tmp.RPC_server_duration = this.RPCstatistics.AxServerDuration_Total;//manca client count
            tmp.RPC_client_duration = this.RPCstatistics.AxClientDuration_Total;//manca server count
            tmp.RPC_whole_duration = this.RPCstatistics.WholeDuration;
            tmp.RPC_bytes_recd = this.RPCstatistics.AxServerBytesRecd;
            tmp.RPC_bytes_sent = this.RPCstatistics.AxServerBytesSent;
            tmp.RPC_histogram = this.RPCstatistics.histogram;//da verificare!!!

            tmp.SQL_stmt_count = this.SQLstatistics.SqlStmtCount_Total;
            tmp.SQL_stmt_duration = this.SQLstatistics.AxDuration_Total;
            tmp.SQL_stmt_preparation_duration = this.SQLstatistics.AxPrepDuration_Total;
            tmp.stmtType_call = this.SQLstatistics.stmtType_call;
            tmp.stmtType_delete = this.SQLstatistics.stmtType_delete;
            tmp.stmtType_insert = this.SQLstatistics.stmtType_insert;
            tmp.stmtType_none = this.SQLstatistics.stmtType_none;
            tmp.stmtType_other = this.SQLstatistics.stmtType_other;
            tmp.stmtType_select = this.SQLstatistics.stmtType_select;
            tmp.stmtType_update = this.SQLstatistics.stmtType_update;
            tmp.SQL_histogram = this.SQLstatistics.histogram;

            tmp.SQL_modelstoredproc_count = this.MODELSTOREstatistics.Count_Total;
            tmp.SQL_modelstoredproc_duration = this.MODELSTOREstatistics.Duration_Total;
            tmp.SQL_modelstoredproc_preparation_duration = this.MODELSTOREstatistics.PrepDuration_Total;
            tmp.SQL_modelstoredproc_histogram = this.MODELSTOREstatistics.histogram;

            tmp.FETCH_fetches_count = this.FETCHstatistics.AxFetchCount_Total;
            tmp.FETCH_duration = this.FETCHstatistics.AxFetchDuration_Total;
            tmp.FETCH_rows_count = this.FETCHstatistics.AxFetchCountCumu_Total;
            tmp.FETCH_fetchcumu_count = this.FETCHstatistics.AxFetchCumu_N_Total;
            tmp.FETCH_cumu_duration = this.FETCHstatistics.AxFetchDurationCumu_Total;
            tmp.FETCH_histogram = this.FETCHstatistics.histogram;

            if (!string.IsNullOrEmpty(this.clientType))
            {
                tmp.clientType = this.clientType;
                tmp.computerName = this.computerName;
                tmp.nsClientTypeDuration = this.nsClientTypeDuration;
            }

            if (this.nsBatchDuration.HasValue)
            {
                tmp.batchStatus = this.batchStatus;
                tmp.batchRecId = this.batchRecId;
                tmp.nsBatchDuration = this.nsBatchDuration.Value;
            }

            //qui tmp va inviato
            new DataSend.DataSendInstance().sender.Publish(tmp);
        }

        public XPPStatistic XPPstatistics { get; private set; }
        public RPCStatistic RPCstatistics { get; private set; }
        public SQLStatistic SQLstatistics { get; private set; }
        public ModelStoreStatistic MODELSTOREstatistics { get; private set; }
        public FETCHStatistic FETCHstatistics { get; private set; }
        public string clientType { get; internal set; }
        public string computerName { get; internal set; }
        public long nsTimeStampEpochStart { get; internal set; }
        public long nsTimeStampEpochLast { get; internal set; }
        public long nsClientTypeDuration
        {
            get
            {
                return nsTimeStampEpochLast - nsTimeStampEpochStart;
            }
        }
        public string batchRecId { get; internal set; }
        public long? nsBatchDuration
        {
            get
            {
                if (nsTimeStampBatchStart.HasValue)
                    return (nsTimeStampBatchEnd.HasValue ? nsTimeStampBatchEnd.Value : nsTimeStampEpochLast) - nsTimeStampBatchStart.Value;
                else
                    return new long?();
            }
        }
        //public long nsClientTypeDuration(long nsTimeStampEpoch)
        //{
        //    return nsTimeStampEpoch - nsTimeStampEpochStart;
        //}

        public long? nsTimeStampBatchStart { get; internal set; }
        public long? nsTimeStampBatchEnd { get; internal set; }
        public string batchStatus { get; internal set; }
    }
}