﻿using System;

namespace AxETWTracing.Statistics.SQL
{
    public interface ISQLStatistic
    {
        long nsTimeStampEpoch { get; } //--> per ora lo metto qui....

        long AxDuration_Total { get; }
        long AxPrepDuration_Total { get; }
        int SqlStmtCount_Total { get; }
        string[] labels { get; }
        int[] histogram { get; }

        int EventsCount_Control { get; }
    }

    public interface IParametricSQLStatistic
    {
        long AxDuration_Total { get; }
        long AxPrepDuration_Total { get; }
        IParametricHistogram histogram { get; }
        int SqlStmtCount_Total { get; }

        int EventsCount_Control { get; }
    }
}