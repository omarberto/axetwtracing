﻿using System;
using System.Collections.Concurrent;
using System.Linq;

namespace AxETWTracing.Statistics.SQL
{
    internal sealed class SqlParamsBinding
    {
        private static readonly Lazy<SqlParamsBinding> lazy = new Lazy<SqlParamsBinding>(() => new SqlParamsBinding());
        internal static SqlParamsBinding instance { get { return lazy.Value; } }

        public bool isActive { get; internal set; }

        private SqlParamsBinding()
        {
        }

        private ConcurrentDictionary<int, ConcurrentDictionary<AxSessionId, ConcurrentDictionary<AxUserName, ConcurrentDictionary<AxConDBSpid, SqlParameters>>>> _instances = new ConcurrentDictionary<int, ConcurrentDictionary<AxSessionId, ConcurrentDictionary<AxUserName, ConcurrentDictionary<AxConDBSpid, SqlParameters>>>>();
        internal void AddBindParameter(Events.SqlInputBind sqlInputBind)
        {
            GetBindParameters(sqlInputBind.processId, sqlInputBind.AxSessionId, sqlInputBind.AxUserName, sqlInputBind.AxConDBSpid).Add(sqlInputBind);
        }
        internal void ClearBindParameters(int aosinstanceProcessId, AxSessionId sessionId, AxUserName userName, AxConDBSpid conDBspid)
        {
            GetBindParameters(aosinstanceProcessId, sessionId, userName, conDBspid).Clear();
        }

        internal SqlParameters GetBindParameters(int aosinstanceProcessId, AxSessionId sessionId, AxUserName userName, AxConDBSpid conDBspid)
        {
            ConcurrentDictionary<AxSessionId, ConcurrentDictionary<AxUserName, ConcurrentDictionary<AxConDBSpid, SqlParameters>>> value1;
            if (!_instances.TryGetValue(aosinstanceProcessId, out value1))
                value1 = _instances[aosinstanceProcessId] = new ConcurrentDictionary<AxSessionId, ConcurrentDictionary<AxUserName, ConcurrentDictionary<AxConDBSpid, SqlParameters>>>();

            ConcurrentDictionary<AxUserName, ConcurrentDictionary<AxConDBSpid, SqlParameters>> value2;
            if (!value1.TryGetValue(sessionId, out value2))
                value2 = value1[sessionId] = new ConcurrentDictionary<AxUserName, ConcurrentDictionary<AxConDBSpid, SqlParameters>>();

            ConcurrentDictionary<AxConDBSpid, SqlParameters> value3;
            if (!value2.TryGetValue(userName, out value3))
                value3 = value2[userName] = new ConcurrentDictionary<AxConDBSpid, SqlParameters>();

            SqlParameters value4;
            if (!value3.TryGetValue(conDBspid, out value4))
                value4 = value3[conDBspid] = new SqlParameters();

            //SqlParameters value5; --> va usata anche thread????
            //if (!value4.TryGetValue(id.ThID, out value5))
            //    value5 = value4[id.ThID] = new SqlParameters();

            return value4;
        }
        internal void RemoveBindParameters(int aosinstanceProcessId, AxSessionId sessionId, AxUserName userName, AxConDBSpid conDBspid)
        {
            SqlParameters sqlParameter;
            _instances[aosinstanceProcessId][sessionId][userName].TryRemove(conDBspid, out sqlParameter);
            ConcurrentDictionary<AxConDBSpid, SqlParameters> spidDict;
            if (_instances[aosinstanceProcessId][sessionId][userName].Count == 0) _instances[aosinstanceProcessId][sessionId].TryRemove(userName, out spidDict);
            ConcurrentDictionary<AxUserName, ConcurrentDictionary<AxConDBSpid, SqlParameters>> sessionDict;
            if (_instances[aosinstanceProcessId][sessionId].Count == 0) _instances[aosinstanceProcessId].TryRemove(sessionId, out sessionDict);
            ConcurrentDictionary<AxSessionId, ConcurrentDictionary<AxUserName, ConcurrentDictionary<AxConDBSpid, SqlParameters>>> instanceDict;
            if (_instances[aosinstanceProcessId].Count == 0) _instances.TryRemove(aosinstanceProcessId, out instanceDict);
        }
    }

    internal sealed class CreateUserSessionBinding
    {
        private static readonly Lazy<CreateUserSessionBinding> lazy = new Lazy<CreateUserSessionBinding>(() => new CreateUserSessionBinding());
        internal static CreateUserSessionBinding instance { get { return lazy.Value; } }

        private CreateUserSessionBinding()
        {
        }

        private ConcurrentDictionary<int, Events.CreateUserSessionEvent> _instances = new ConcurrentDictionary<int, Events.CreateUserSessionEvent>();
        internal void SetCreateUserSessionEvent(Events.CreateUserSessionEvent createUserSessionEvent, int aosinstanceProcessId)
        {
            var value = GetCreateUserSessionEvent(aosinstanceProcessId);
            value.clientType = createUserSessionEvent.clientType;
            value.axUserName = createUserSessionEvent.axUserName;
            value.computerName = createUserSessionEvent.computerName;
            value.nsTimeStampEpoch = createUserSessionEvent.nsTimeStampEpoch;
        }

        internal Events.CreateUserSessionEvent GetCreateUserSessionEvent(int aosinstanceProcessId)
        {
            Events.CreateUserSessionEvent value1;
            if (!_instances.TryGetValue(aosinstanceProcessId, out value1))
                value1 = _instances[aosinstanceProcessId] = new Events.CreateUserSessionEvent();

            return value1;
        }
        internal void RemoveCreateUserSessionEvent(int aosinstanceProcessId)
        {
            Events.CreateUserSessionEvent createUserSessionEvent;
            _instances.TryRemove(aosinstanceProcessId, out createUserSessionEvent);
        }
    }
}
