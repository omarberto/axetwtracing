﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Diagnostics.Tracing;
using AxETWTracing.Events;

namespace AxETWTracing.Statistics.SQL
{
    internal class SQLStatistic
    {
        //questo é l'inizio di ogni interval
        internal readonly long intervalStartTimeStamp;
        internal readonly long intervalEndTimeStamp;

        internal SQLStatistic(long tracingMessageTimeStamp)
        {
            this.intervalStartTimeStamp = (tracingMessageTimeStamp / 60000000000L) * 60000000000L;//poi trasformiamo
            this.intervalEndTimeStamp = this.intervalStartTimeStamp + 60000000000L;
        }

        internal SQLStatistic Next()
        {
            return new SQLStatistic(intervalEndTimeStamp);
        }

        internal SQLStatistic Next(long deltaTimeEpoch)
        {
            return new SQLStatistic((((intervalEndTimeStamp + deltaTimeEpoch) / 60000000000L) * 60000000000L) + 60000000000L);
        }

        internal void Correct(long deltaTimeEpoch)
        {
        }

        /************************************************/

        public int EventsCount_Control { get { return _SqlStmtCount_Total; } }

        /************************************************/

        public IParametricHistogram histogram { get; } = new ParametricHistogram(AX2012ETWtracing.Configuration.Configuration.config.ax2012.statistic.histograms.histogramSQLStmt);

        /************************************************/

        private long _AxDuration_Total;
        private long _AxPrepDuration_Total;
        private int _SqlStmtCount_Total;
        public long AxDuration_Total { get { return _AxDuration_Total; } }
        public long AxPrepDuration_Total { get { return _AxPrepDuration_Total; } }
        public int SqlStmtCount_Total { get { return _SqlStmtCount_Total; } }

        /************************************************/

        //devo gestire svuotamento
        internal void addEvent(AxSqlStmtEvent obj)
        {
            this._SqlStmtCount_Total++;

            this._AxPrepDuration_Total += obj.AxPrepDuration;
            this._AxDuration_Total += obj.AxDuration;
            histogram.AddDuration(obj.AxPrepDuration + obj.AxDuration);
        }

        private int _stmtType_call;
        public int stmtType_call { get { return _stmtType_call; } }
        private int _stmtType_delete;
        public int stmtType_delete { get { return _stmtType_delete; } }
        private int _stmtType_insert;
        public int stmtType_insert { get { return _stmtType_insert; } }
        private int _stmtType_none;
        public int stmtType_none { get { return _stmtType_none; } }
        private int _stmtType_other;
        public int stmtType_other { get { return _stmtType_other; } }
        private int _stmtType_select;
        public int stmtType_select { get { return _stmtType_select; } }
        private int _stmtType_update;
        public int stmtType_update { get { return _stmtType_update; } }
        
        internal void addEventType(SqlStmtParser.StmtType stmtType)
        {
            switch (stmtType)
            {
                case SqlStmtParser.StmtType.call:
                    this._stmtType_call++;
                    break;
                case SqlStmtParser.StmtType.delete:
                    this._stmtType_delete++;
                    break;
                case SqlStmtParser.StmtType.insert:
                    this._stmtType_insert++;
                    break;
                case SqlStmtParser.StmtType.none:
                    this._stmtType_none++;
                    break;
                case SqlStmtParser.StmtType.other:
                    this._stmtType_other++;
                    break;
                case SqlStmtParser.StmtType.select:
                    this._stmtType_select++;
                    break;
                case SqlStmtParser.StmtType.update:
                    this._stmtType_update++;
                    break;
            }
        }
    }
}
