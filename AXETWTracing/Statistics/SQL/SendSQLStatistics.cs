﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxETWTracing.Statistics.SQL
{
    internal class SendSQLStatistics : ISQLStatistic
    {
        public long nsTimeStampEpoch { get; private set; }

        private SendSQLStatistics(long nsTimeStampEpoch)
        {
            this.nsTimeStampEpoch = nsTimeStampEpoch;

            this.lastWrite = DateTime.UtcNow;
        }
        public SendSQLStatistics(long nsTimeStampEpoch, IParametricSQLStatistic fromETW)
        {
            this.nsTimeStampEpoch = nsTimeStampEpoch;

            this.EventsCount_Control = fromETW.EventsCount_Control;

            this.SqlStmtCount_Control = fromETW.SqlStmtCount_Total;

            this.AxDuration_Total = fromETW.AxDuration_Total;
            this.AxPrepDuration_Total = fromETW.AxPrepDuration_Total;
            this.SqlStmtCount_Total = fromETW.SqlStmtCount_Total;
            this.histogram = fromETW.histogram.ToArray();
            this.labels = fromETW.histogram.labels;

            this.lastWrite = DateTime.UtcNow;
        }
        public SendSQLStatistics(long nsTimeStampEpoch, IParametricSQLStatistic fromETW1 , IParametricSQLStatistic fromETW2)
        {
            this.nsTimeStampEpoch = nsTimeStampEpoch;

            this.EventsCount_Control = fromETW2.EventsCount_Control - fromETW1.EventsCount_Control;
            //this.EventsCount_Control = fromETW2.EventsCount_Control;

            this.SqlStmtCount_Control = fromETW2.SqlStmtCount_Total;

            this.AxDuration_Total = fromETW2.AxDuration_Total - fromETW1.AxDuration_Total;
            this.AxPrepDuration_Total = fromETW2.AxPrepDuration_Total - fromETW1.AxPrepDuration_Total;
            this.SqlStmtCount_Total = fromETW2.SqlStmtCount_Total - fromETW1.SqlStmtCount_Total;
            this.labels = fromETW2.histogram.labels;
            this.histogram = fromETW2.histogram.ToArray();
            for (int i = 0; i < this.histogram.Length; i++) this.histogram[i] -= fromETW1.histogram.ToArray()[i];

            this.lastWrite = DateTime.UtcNow;
        }

        public long AxDuration_Total { get; private set; }
        public long AxPrepDuration_Total { get; private set; }
        public int SqlStmtCount_Total { get; private set; }
        public string[] labels { get; private set; }
        public int[] histogram { get; private set; }

        public int SqlStmtCount_Control { get; private set; }

        private DateTime lastWrite;

        private const long publishCycleTime = 60000000000L;
        internal SendSQLStatistics Increment(IParametricSQLStatistic value, int timeoutMinutes, out bool clear, out bool noChange)
        {
            clear = false;
            //forse basta uno
            noChange = this.SqlStmtCount_Total == value.SqlStmtCount_Total;
            if (noChange)
            {
                clear = (DateTime.UtcNow - lastWrite).TotalMinutes > timeoutMinutes;
                //clear = true;
                //if (clear)
                //{
                //    return null;
                //}
                this.nsTimeStampEpoch += publishCycleTime;
                return this;
            }

            var retValue = new SendSQLStatistics(nsTimeStampEpoch + publishCycleTime)
            {
                SqlStmtCount_Control = value.SqlStmtCount_Total,//so is writable

                AxDuration_Total = value.AxDuration_Total - this.AxDuration_Total,
                AxPrepDuration_Total = value.AxPrepDuration_Total - this.AxPrepDuration_Total,
                SqlStmtCount_Total = value.SqlStmtCount_Total - this.SqlStmtCount_Total,
                histogram = value.histogram.ToArray(),
                labels = value.histogram.labels
            };
            for (int i = 0; i < retValue.histogram.Length; i++) retValue.histogram[i] -= this.histogram[i];

            this.AxDuration_Total = value.AxDuration_Total;
            this.AxPrepDuration_Total = value.AxPrepDuration_Total;
            this.SqlStmtCount_Total = value.SqlStmtCount_Total;
            this.histogram = value.histogram.ToArray();
            this.labels = value.histogram.labels;

            this.lastWrite = DateTime.UtcNow;

            return retValue;
        }


        public int EventsCount_Control { get; private set; }
    }
}
